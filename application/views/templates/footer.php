<div class="loader" id="loader" style="display: none;"></div>

<!-- FOOTER -->
<div id="footer">





    <p>&copy;  mHealth Kenya &nbsp;2016 &nbsp; &nbsp;         <b> Powered by : mHealth  Kenya </b> </p>
      <!-- <img src="template/assets/img/mhealth_4.png" alt="" />   -->
</div>
<!--END FOOTER -->
<!-- GLOBAL SCRIPTS -->
<script src="<?php echo base_url(); ?>assets/template/assets/plugins/jquery-2.0.3.min.js"></script>
<script src="<?php echo base_url(); ?>assets/template/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/template/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
<!-- END GLOBAL SCRIPTS -->
<!--start crud styles 
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<!-- START CLOSE CHART STYLES    -->  
<!-- indesx chart      -->
<script src="<?php echo base_url(); ?>assets/charts/lib/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/charts/lib/js/chartphp.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/charts/lib/js/chartphp.css">



<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" />

<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {

<?php
$function_name = $this->uri->segment(2);

if ($function_name == "appointment_diary") {
    ?>
            $(".download_excel").click(function () {
                var href = "<?php echo base_url(); ?>home/download_app_diary";
                window.location.href = href; //causes the browser to refresh and load the requested url
            });
            $(".download_register").click(function () {
                var href = "<?php echo base_url(); ?>home/download_defaulter_register";
                window.location.href = href; //causes the browser to refresh and load the requested url
            });
    <?php
} else {
    ?>


    <?php
}
?>

        $('.filter_county').on('change', function () {
            // Does some stuff and logs the event to the console
            $(".filter_sub_county").hide();
            $(".filter_sub_county_wait").show();
            var county_id = this.value;
            $.ajax({
                url: "<?php echo base_url(); ?>Reports/filter_sub_county/" + county_id + "/",
                type: 'GET',
                dataType: 'JSON',
                success: function (data) {
                    $(".filter_sub_county").empty();
                    var select = "<option value=''> Please Select : </option>";
                    $(".filter_sub_county").append(select);
                    $.each(data, function (i, value) {
                        $(".filter_sub_county_wait").hide();
                        $(".filter_sub_county").show();
                        var sub_county_options = "<option value=" + value.sub_county_id + ">" + value.sub_county_name + "</option>";
                        $(".filter_sub_county").append(sub_county_options);
                    });
                }, error: function (jqXHR) {

                }
            })
        });
        $('.filter_sub_county').on('change', function () {
            // Does some stuff and logs the event to the console
            $(".filter_facility").hide();
            $(".filter_facility_wait").show();
            var sub_county_id = this.value;
            $.ajax({
                url: "<?php echo base_url(); ?>Reports/filter_facilities/" + sub_county_id + "/",
                type: 'GET',
                dataType: 'JSON',
                success: function (data) {
                    $(".filter_facility").empty();
                    var select = "<option value=''>Please Select : </option>";
                    $(".filter_facility").append(select);
                    $.each(data, function (i, value) {
                        $(".filter_facility_wait").hide();
                        $(".filter_facility").show();
                        var facility_options = "<option value=" + value.mfl_code + ">" + value.facility_name + "</option>";
                        $(".filter_facility").append(facility_options);
                    });
                }, error: function (jqXHR) {

                }
            })
        });
        window.submit_data = function (controller, submit_function, form_class, success_alert, error_alert) {

            $(".loader").show();
            $("#" + form_class + "").submit(function (event) {
                event.preventDefault();
//                $('.loader').show();
                dataString = $("#" + form_class + "").serialize();
                $(".btn").prop('disabled', true);
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>" + controller + "/" + submit_function + "",
                    data: dataString,
                    success: function (data) {
                        $(".loader").hide();
                        data = JSON.parse(data);
                        var response = data[0].response;
                        if (response === true) {




                            swal({
                                title: "Success!",
                                text: "" + success_alert + "",
                                type: "success",
                                confirmButtonText: "Okay!",
                                closeOnConfirm: true
                            }, function () {
                                window.location.reload(1);
                            });
                        } else if (response === 'Taken') {
                            $(".btn").prop('disabled', false);
                            sweetAlert("Info", "Clinic No already taken ", 'info');
                        } else if (response === 'Phone Taken') {
                            $(".btn").prop('disabled', false);
                            sweetAlert("Info", "Phone No already used in the System ", 'info');
                        } else if (response === 'Email Taken') {
                            $(".btn").prop('disabled', false);
                            sweetAlert("Info", "Email Address already used in the System ", 'info');
                        } else if (response === 'Phone Email Taken') {
                            $(".btn").prop('disabled', false);
                            sweetAlert("Info", "Phone No and Email Address already used in the System ", 'info');
                        } else if (response === 'Under Age') {
                            $(".btn").prop('disabled', false);
                            sweetAlert("Info", "Under Age are not allowed in the System ", 'info');
                        } else if (response === 'Enrollment greater than DoB') {
                            $(".btn").prop('disabled', false);
                            sweetAlert("Info", "Enrollment Date cannot be before than Date of BIrth", 'info');
                        } else if (response === 'ART greater than DoB') {
                            $(".btn").prop('disabled', false);
                            sweetAlert("Info", "ART Date cannot be before  than Date of Birth ", 'info');
                        } else if (response === 'ART less than Enrollment') {
                            $(".btn").prop('disabled', false);
                            sweetAlert("Info", "ART Date cannot be after than Enrollment Date ", 'info');
                        } else {
                            $(".btn").prop('disabled', false);
                            sweetAlert("Oops...", "" + error_alert + "", "error");
                        }




                    }, error: function (data) {
                        $('.loader').hide();
                        $(".btn").prop('disabled', false);
                        // sweetAlert("Oops...", "" + error_alert + "", "error");
                    }

                });
                event.preventDefault();
                return false;
            });
        }








    });</script>

<?php
$function_name = $this->uri->segment(2);

if ($function_name == "facilities") {
    ?>

    <?php
} else {
    ?>


    <?php
}
?>




<script src="https://code.jquery.com/jquery-1.12.4.js"></script>

<!--<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs-3.3.7/jqc-1.12.4/jszip-3.1.3/pdfmake-0.1.27/dt-1.10.15/af-2.2.0/b-1.3.1/b-colvis-1.3.1/b-flash-1.3.1/b-html5-1.3.1/b-print-1.3.1/cr-1.3.3/fc-3.2.2/fh-3.1.2/kt-2.2.1/r-2.1.1/rg-1.0.0/rr-1.2.0/sc-1.4.2/se-1.2.2/datatables.min.css"/>

<script type="text/javascript" src="https://cdn.datatables.net/v/bs-3.3.7/jqc-1.12.4/jszip-3.1.3/pdfmake-0.1.27/dt-1.10.15/af-2.2.0/b-1.3.1/b-colvis-1.3.1/b-flash-1.3.1/b-html5-1.3.1/b-print-1.3.1/cr-1.3.3/fc-3.2.2/fh-3.1.2/kt-2.2.1/r-2.1.1/rg-1.0.0/rr-1.2.0/sc-1.4.2/se-1.2.2/datatables.min.js"></script>
-->




<!--<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jqc-1.12.3/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.0/b-colvis-1.5.0/b-flash-1.5.0/b-html5-1.5.0/b-print-1.5.0/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.3/sl-1.2.4/datatables.min.css"/>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/jqc-1.12.3/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.0/b-colvis-1.5.0/b-flash-1.5.0/b-html5-1.5.0/b-print-1.5.0/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.3/sl-1.2.4/datatables.min.js"></script>-->






<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs-3.3.7/jq-2.2.4/jszip-2.5.0/pdfmake-0.1.18/dt-1.10.13/af-2.1.3/b-1.2.4/b-colvis-1.2.4/b-flash-1.2.4/b-html5-1.2.4/b-print-1.2.4/cr-1.3.2/fc-3.2.2/fh-3.1.2/kt-2.2.0/r-2.1.1/rg-1.0.0/rr-1.2.0/sc-1.4.2/se-1.2.0/datatables.min.css"/>

<script type="text/javascript" src="https://cdn.datatables.net/v/bs-3.3.7/jq-2.2.4/jszip-2.5.0/pdfmake-0.1.18/dt-1.10.13/af-2.1.3/b-1.2.4/b-colvis-1.2.4/b-flash-1.2.4/b-html5-1.2.4/b-print-1.2.4/cr-1.3.2/fc-3.2.2/fh-3.1.2/kt-2.2.0/r-2.1.1/rg-1.0.0/rr-1.2.0/sc-1.4.2/se-1.2.0/datatables.min.js"></script>



<script type="text/javascript">
    var dTbles = jQuery.noConflict();
    dTbles(document).ready(function () {


//
//        var table = dTbles('#table').DataTable({
//            buttons: ['copy', 'excel', 'pdf', 'colvis'],
//            responsive: true,
//            "lengthMenu": [[5, 10, 25, 50, -1], [10, 25, 50, "All"]]
//        });
//        table.buttons().container()
//                .appendTo('#table_wrapper .col-sm-6:eq(0)');

<?php
if ($function_name == "facility_home") {
    ?>

            dTbles('.today_app_table').DataTable({
                dom: 'Bfrtip',
                responsive: true,
                "lengthMenu": [[5, 10, 25, 50, -1], [10, 25, 50, "All"]],
                buttons: [
                    dTbles.extend(true, {}, {
                        extend: 'copyHtml5'
                    }),
                    dTbles.extend(true, {}, {
                        extend: 'excelHtml5'
                    }),
                    dTbles.extend(true, {}, {
                        extend: 'pdfHtml5'
                    })
                ]
            });



            dTbles('.missed_table').DataTable({
                dom: 'Bfrtip',
                responsive: true,
                "lengthMenu": [[5, 10, 25, 50, -1], [10, 25, 50, "All"]],
                buttons: [
                    dTbles.extend(true, {}, {
                        extend: 'copyHtml5'
                    }),
                    dTbles.extend(true, {}, {
                        extend: 'excelHtml5'
                    }),
                    dTbles.extend(true, {}, {
                        extend: 'pdfHtml5'
                    })
                ]
            });


            dTbles('.defaulted_table').DataTable({
                dom: 'Bfrtip',
                responsive: true,
                "lengthMenu": [[5, 10, 25, 50, -1], [10, 25, 50, "All"]],
                buttons: [
                    dTbles.extend(true, {}, {
                        extend: 'copyHtml5'
                    }),
                    dTbles.extend(true, {}, {
                        extend: 'excelHtml5'
                    }),
                    dTbles.extend(true, {}, {
                        extend: 'pdfHtml5'
                    })
                ]
            });


            dTbles('.ltfu_table').DataTable({
                dom: 'Bfrtip',
                responsive: true,
                "lengthMenu": [[5, 10, 25, 50, -1], [10, 25, 50, "All"]],
                buttons: [
                    dTbles.extend(true, {}, {
                        extend: 'copyHtml5'
                    }),
                    dTbles.extend(true, {}, {
                        extend: 'excelHtml5'
                    }),
                    dTbles.extend(true, {}, {
                        extend: 'pdfHtml5'
                    })
                ]
            });


    <?php
}
?>




        dTbles('.table').DataTable({
            dom: 'Bfrtip',
            responsive: true,
            "lengthMenu": [[5, 10, 25, 50, -1], [10, 25, 50, "All"]],
            buttons: [
                dTbles.extend(true, {}, {
                    extend: 'copyHtml5'
                }),
                dTbles.extend(true, {}, {
                    extend: 'excelHtml5'
                }),
                dTbles.extend(true, {}, {
                    extend: 'pdfHtml5'
                })
            ]
        });
//        var table = dTbles('.table').DataTable({
//            buttons: ['copy', 'excel', 'pdf', 'colvis'],
//            responsive: true,
//            "lengthMenu": [[5, 10, 25, 50, -1], [10, 25, 50, "All"]]
//        });
//        table.buttons().container()
//                .appendTo('#table_wrapper .col-sm-6:eq(0)');
//
//




        function create_audit_trail_results_layout() {

            var value = dTbles(".audit_trail_search_value").val();
            dTbles.ajax({
                type: 'GET',
                url: "<?php echo base_url(); ?>support/search_audit_trail/" + value,
                dataType: 'JSON',
                "dataSrc": "results",
                success: function (results) {
                    var check_data = jQuery.isEmptyObject(results);
                    if (check_data === true) {
                        dTbles(".audit_trail_search_results_div").hide();
                    } else {

                        dTbles(".audit_trail_search_results_div").empty();
                        var table = '<table class=" dataTables_processing  table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">\n\
             <thead> <tr> <th>Date Added : </th> <th> Username  </th> <th>Description  :</th>  <th>Phone Number   : </th>   </tr> </thead>  \n\
               <tbody id="audit_trail_results_tbody" class="audit_trail_results_tbody"> </tbody> </table>';
                        dTbles('.audit_trail_search_results_div').append(table);
                        dTbles.each(results, function (i, messsages) {


                            var tr_results = "<tr>\n\
                    <td>" + messsages.created_at + "</td>\n\
                    <td>" + messsages.user_name + "</td>\n\
                    <td>" + messsages.description + "</td>\n\
                    <td>" + messsages.phone_no + "</td>\n\
                    </tr>";
                            dTbles(".audit_trail_results_tbody").append(tr_results);
                            dTbles(".audit_trail_search_results_div").show();
                        });
                        //dTbles('.dataTables_processing').DataTable({});






                        var table = dTbles('.dataTables_processing').DataTable({
                            dom: 'Bfrtip',
                            responsive: true,
                            "lengthMenu": [[5, 10, 25, 50, -1], [10, 25, 50, "All"]],
                            buttons: [
                                dTbles.extend(true, {}, {
                                    extend: 'copyHtml5'
                                }),
                                dTbles.extend(true, {}, {
                                    extend: 'excelHtml5'
                                }),
                                dTbles.extend(true, {}, {
                                    extend: 'pdfHtml5'
                                })
                            ]
                        });
//                        var table = dTbles('.dataTables_processing').DataTable({
//                            buttons: ['copy', 'excel', 'pdf', 'colvis'],
//                            responsive: true,
//                            "lengthMenu": [[5, 10, 25, 50, -1], [10, 25, 50, "All"]]
//                        });
//                        table.buttons().container()
//                                .appendTo('#table_wrapper .col-sm-6:eq(0)');


                    }
                }
            });
        }

        dTbles(".audit_trail_serach_btn").click(function () {


            if (!dTbles("input[name='audit_trail_search_option']:checked").val()) {

                sweetAlert("Info", "Nothing is checked!, Please select one of the Search Parameters... ", 'info');
            } else {
                create_audit_trail_results_layout();
            }
        });
        function create_incoming_results_layout() {

            var value = dTbles(".incoming_search_value").val();
            dTbles.ajax({
                type: 'GET',
                url: "<?php echo base_url(); ?>support/search_incoming/" + value,
                dataType: 'JSON',
                "dataSrc": "results",
                success: function (results) {
                    var check_data = jQuery.isEmptyObject(results);
                    if (check_data === true) {
                        dTbles(".incoming_search_results_div").hide();
                    } else {

                        dTbles(".incoming_search_results_div").empty();
                        var table = '<table class=" dataTables_processing  table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">\n\
             <thead> <tr> <th>From  No</th> <th>Destination No :</th> <th>Message : </th><th>Date Added : </th> <th>Time Stamp   : </th><th>Processed : </th>   </tr> </thead>  \n\
               <tbody id="incoming_results_tbody" class="incoming_results_tbody"> </tbody> </table>';
                        dTbles('.incoming_search_results_div').append(table);
                        dTbles.each(results, function (i, messsages) {


                            var tr_results = "<tr>\n\
                    <td>" + messsages.source + "</td>\n\
                    <td>" + messsages.destination + "</td>\n\
                    <td>" + messsages.msg + "</td>\n\
                    <td>" + messsages.created_at + "</td>\n\
                    <td>" + messsages.updated_at + "</td>\n\
                    <td>" + messsages.processed + "</td>\n\
                    </tr>";
                            console.log(tr_results);
                            dTbles(".incoming_results_tbody").append(tr_results);
                            dTbles(".incoming_search_results_div").show();
                        });
                        var table = dTbles('.dataTables_processing').DataTable({
                            dom: 'Bfrtip',
                            responsive: true,
                            "lengthMenu": [[5, 10, 25, 50, -1], [10, 25, 50, "All"]],
                            buttons: [
                                dTbles.extend(true, {}, {
                                    extend: 'copyHtml5'
                                }),
                                dTbles.extend(true, {}, {
                                    extend: 'excelHtml5'
                                }),
                                dTbles.extend(true, {}, {
                                    extend: 'pdfHtml5'
                                })
                            ]
                        });
                    }
                }
            });
        }

        dTbles(".incoming_serach_btn").click(function () {


            if (!dTbles("input[name='incoming_search_option']:checked").val()) {

                sweetAlert("Info", "Nothing is checked!, Please select one of the Search Parameters... ", 'info');
            } else {
                create_incoming_results_layout();
            }
        });
        function create_outgoing_results_layout() {

            var value = dTbles(".outgoing_search_value").val();
            dTbles.ajax({
                type: 'GET',
                url: "<?php echo base_url(); ?>support/search_outgoing/" + value,
                dataType: 'JSON',
                "dataSrc": "results",
                success: function (results) {
                    var check_data = jQuery.isEmptyObject(results);
                    if (check_data === true) {
                        dTbles(".outgoing_search_results_div").hide();
                    } else {

                        dTbles(".outgoing_search_results_div").empty();
                        var table = '<table class=" dataTables_processing  table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">\n\
             <thead> <tr> <th>From  No</th> <th>Destination No :</th> <th>Message : </th><th>Date Added : </th> <th>Time Stamp   : </th><th>Processed : </th>   </tr> </thead>  \n\
               <tbody id="outgoing_results_tbody" class="outgoing_results_tbody"> </tbody> </table>';
                        dTbles('.outgoing_search_results_div').append(table);
                        dTbles.each(results, function (i, messsages) {


                            var tr_results = "<tr>\n\
                    <td>" + messsages.source + "</td>\n\
                    <td>" + messsages.destination + "</td>\n\
                    <td>" + messsages.msg + "</td>\n\
                    <td>" + messsages.created_at + "</td>\n\
                    <td>" + messsages.updated_at + "</td>\n\
                    <td>" + messsages.processed + "</td>\n\
                    </tr>";
                            console.log(tr_results);
                            dTbles(".outgoing_results_tbody").append(tr_results);
                            dTbles(".outgoing_search_results_div").show();
                        });
                        var table = dTbles('.dataTables_processing').DataTable({
                            dom: 'Bfrtip',
                            responsive: true,
                            "lengthMenu": [[5, 10, 25, 50, -1], [10, 25, 50, "All"]],
                            buttons: [
                                dTbles.extend(true, {}, {
                                    extend: 'copyHtml5'
                                }),
                                dTbles.extend(true, {}, {
                                    extend: 'excelHtml5'
                                }),
                                dTbles.extend(true, {}, {
                                    extend: 'pdfHtml5'
                                })
                            ]
                        });
                    }
                }
            });
        }

        dTbles(".outgoing_serach_btn").click(function () {


            if (!dTbles("input[name='outgoing_search_option']:checked").val()) {

                sweetAlert("Info", "Nothing is checked!, Please select one of the Search Parameters... ", 'info');
            } else {
                create_outgoing_results_layout();
            }
        });
        function create_results_layout() {

            var value = dTbles(".search_value").val();
            dTbles.ajax({
                type: 'GET',
                url: "<?php echo base_url(); ?>admin/search_facility/" + value,
                dataType: 'JSON',
                "dataSrc": "results",
                success: function (results) {
                    var check_data = jQuery.isEmptyObject(results);
                    if (check_data === true) {
                        dTbles(".search_results_div").hide();
                    } else {

                        dTbles(".search_results_div").empty();
                        var table = '<table class=" dataTables_processing  table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">\n\
             <thead> <tr> <th>Name</th> <th>MFL No :</th> <th>County : </th><th>Sub County : </th> <th>Consituency : </th><th>Owner: </th> <th>Add Facility </th>  </tr> </thead>  \n\
               <tbody id="results_tbody" class="results_tbody"> </tbody> </table>';
                        dTbles('.search_results_div').append(table);
                        dTbles.each(results, function (i, facilities) {


                            var tr_results = "<tr>\n\
                    <td>" + facilities.facility_name + "</td>\n\
                    <td>" + facilities.mfl_code + "</td>\n\
                    <td>" + facilities.owner + "</td>\n\
                    <td>" + facilities.county_name + "</td>\n\
                    <td>" + facilities.sub_county_name + "</td>\n\
                    <td>" + facilities.consituency_name + "</td>\n\
                    <td><input type='hidden' id='hidden_mfl_code' class=' hidden_mfl_code form-control' name='hidden_mfl_code' value='" + facilities.mfl_code + "'/>\n\
                    <button class='btn btn-xs btn-default select_mfl_btn' id='select_donor_btn' data-original-title='Select Facility'>\n\
                    <span class='glyphicon glyphicon-plus'></span> </button></td>  \n\
                    </tr>";
                            dTbles(".results_tbody").append(tr_results);
                            dTbles(".search_results_div").show();
                        });
                        var table = dTbles('.dataTables_processing').DataTable({
                            dom: 'Bfrtip',
                            responsive: true,
                            "lengthMenu": [[5, 10, 25, 50, -1], [10, 25, 50, "All"]],
                            buttons: [
                                dTbles.extend(true, {}, {
                                    extend: 'copyHtml5'
                                }),
                                dTbles.extend(true, {}, {
                                    extend: 'excelHtml5'
                                }),
                                dTbles.extend(true, {}, {
                                    extend: 'pdfHtml5'
                                })
                            ]
                        });
                    }
                }
            });
        }

        dTbles(".search_value").keyup(function () {

            if (!dTbles("input[name='search_option']:checked").val()) {

                sweetAlert("Info", "Nothing is checked!, Please select one of the Search Parameters... ", 'info');
            } else {
                create_results_layout();
            }
        });
        window.create_percentage_counties_report = function create_percentage_counties_report(data) {




            dTbles(".percentage_counties_div").empty();
            var table = '<table id="percentage_counties_table" class=" percentage_counties_table dashboard_report dataTables_processing  table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">\n\
             <thead> <tr> <th>Name</th> <th> No :</th>  </tr> </thead>  \n\
               <tbody id="percentage_counties_tbody" class="percentage_counties_tbody"> </tbody> </table>';
            dTbles('.percentage_counties_div').append(table);
            dTbles.each(data, function (i, val) {
                var target_counties = val.target_counties;
                var actual_counties = val.actual_counties;
                var tr_results = "<tr>\n\
                    <td>Target Counties</td>\n\
                    <td>" + target_counties + "</td>\n\
                    </tr><tr>\n\
                     <td>Actual Counties</td>\n\
                    <td>" + actual_counties + "</td>\n\</tr>";
                dTbles(".percentage_counties_tbody").append(tr_results);
            });
            dTbles(".percentage_counties_div").show();
            dTbles('.percentage_counties_table').DataTable({dom: 'Bfrtip',
                "searching": false,
                "bInfo": false,
                "bPaginate": false,
                buttons: [
                    {
                        extend: 'copyHtml5',
                        exportOptions: {
                            columns: ':contains("Office")'
                        }
                    },
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5'
                ]});
        }





        window.create_client_reg_report = function create_client_reg_report(data) {




            dTbles(".client_reg_report_div").empty();
            var table = '<table id="client_reg_dashboard_table" class=" client_reg_dashboard_table dashboard_report dataTables_processing  table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">\n\
             <thead> <tr> <th>Name</th> <th> No :</th>  </tr> </thead>  \n\
               <tbody id="client_reg_results_tbody" class="client_reg_results_tbody"> </tbody> </table>';
            dTbles('.client_reg_report_div').append(table);
            dTbles.each(data, function (i, val) {
                var name = val.k;
                var value = val.v;
                var tr_results = "<tr>\n\
                    <td>" + name + "</td>\n\
                    <td>" + value + "</td>\n\</td>\n\
                    </tr>";
                dTbles(".client_reg_results_tbody").append(tr_results);
            });
            dTbles(".client_reg_report_div").show();
            dTbles('.client_reg_dashboard_table').DataTable({dom: 'Bfrtip',
                "searching": false,
                "bInfo": false,
                "bPaginate": false,
                buttons: [
                    {
                        extend: 'copyHtml5',
                        exportOptions: {
                            columns: ':contains("Office")'
                        }
                    },
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5'
                ]});
        }










        window.create_client_appointment_report = function create_client_appointment_report(data) {




            dTbles(".client_appointment_report_div").empty();
            var table = '<table id="client_appointment_dashboard_table" class=" client_appointment_dashboard_table dashboard_report dataTables_processing  table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">\n\
             <thead> <tr> <th>Name</th> <th> No :</th>  </tr> </thead>  \n\
               <tbody id="client_appointment_results_tbody" class="client_appointment_results_tbody"> </tbody> </table>';
            dTbles('.client_appointment_report_div').append(table);
            dTbles.each(data, function (i, val) {
                var name = val.k;
                var value = val.v;
                var tr_results = "<tr>\n\
                    <td>" + name + "</td>\n\
                    <td>" + value + "</td>\n\</td>\n\
                    </tr>";
                dTbles(".client_appointment_results_tbody").append(tr_results);
            });
            dTbles(".client_appointment_report_div").show();
            dTbles('.client_appointment_dashboard_table').DataTable({dom: 'Bfrtip',
                "searching": false,
                "bInfo": false,
                "bPaginate": false,
                buttons: [
                    {
                        extend: 'copyHtml5',
                        exportOptions: {
                            columns: ':contains("Office")'
                        }
                    },
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5'
                ]});
        }

        window.create_gender_report = function create_gender_report(data) {


            var table = dTbles('#gender_dashboard_table').DataTable({
                lengthChange: false,
                buttons: ['copy', 'excel', 'pdf', 'colvis']
            });
            table.buttons().container()
                    .appendTo('#table_wrapper .col-sm-6:eq(0)');
            dTbles(".gender_report_div").empty();
            var table = '<table id="gender_dashboard_table" class=" gender_dashboard_table dashboard_report dataTables_processing  table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">\n\
             <thead> <tr> <th>Name</th> <th> No :</th>  </tr> </thead>  \n\
               <tbody id="gender_results_tbody" class="gender_results_tbody"> </tbody> </table>';
            dTbles('.gender_report_div').append(table);
            dTbles.each(data, function (i, val) {
                var name = val.name;
                var value = val.value;
                var tr_results = "<tr>\n\
                    <td>" + name + "</td>\n\
                    <td>" + value + "</td>\n\</td>\n\
                    </tr>";
                dTbles(".gender_results_tbody").append(tr_results);
            });
            dTbles(".gender_report_div").show();
            dTbles('.gender_dashboard_table').DataTable({dom: 'Bfrtip',
                "searching": false,
                "bInfo": false,
                "bPaginate": false,
                buttons: [
                    {
                        extend: 'copyHtml5',
                        exportOptions: {
                            columns: ':contains("Office")'
                        }
                    },
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5'
                ]});
        }


        window.create_message_distribution_report = function create_message_distribution_report(data) {

            dTbles(".message_distribution_report_div").empty();
            var table = dTbles('#message_distributon_dashboard_table').DataTable({
                lengthChange: false,
                buttons: ['copy', 'excel', 'pdf', 'colvis']
            });
            table.buttons().container()
                    .appendTo('#table_wrapper .col-sm-6:eq(0)');
            var table = '<table id="message_distributon_dashboard_table" class=" message_distributon_dashboard_table dashboard_report dataTables_processing  table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">\n\
             <thead> <tr> <th>Name</th> <th> No :</th>  </tr> </thead>  \n\
               <tbody id="message_distribution_results_tbody" class="message_distribution_results_tbody"> </tbody> </table>';
            dTbles('.message_distribution_report_div').append(table);
            dTbles.each(data, function (i, val) {
                var name = val.message_type;
                var value = val.no_messages;
                //console.log('Message Type => ' + name + ' No of Messages => ' + value + ' end ');
                var tr_results = "<tr>\n\
                    <td>" + name + "</td>\n\
                    <td>" + value + "</td>\n\</td>\n\
                    </tr>";
                dTbles(".message_distribution_results_tbody").append(tr_results);
            });
            dTbles(".message_distribution_report_div").show();
            dTbles('.message_distributon_dashboard_table').DataTable({
                dom: 'Bfrtip',
                "searching": false,
                "bInfo": false,
                "bPaginate": false,
                buttons: [
                    {
                        extend: 'copyHtml5',
                        exportOptions: {
                            columns: ':contains("Office")'
                        }
                    },
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5'
                ]});
        }


        window.create_client_language_report = function create_client_language_report(data) {





            dTbles(".language_report_div").empty();
            var table = '<table id="language_dashboard_table" class=" language_dashboard_table dashboard_report dataTables_processing  table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">\n\
             <thead> <tr> <th>Name</th> <th> No :</th>  </tr> </thead>  \n\
               <tbody id="language_results_tbody" class="language_results_tbody"> </tbody> </table>';
            dTbles('.language_report_div').append(table);
            dTbles.each(data, function (i, val) {
                var name = val.NAME;
                var value = val.VALUE;
                var tr_results = "<tr>\n\
                    <td>" + name + "</td>\n\
                    <td>" + value + "</td>\n\</td>\n\
                    </tr>";
                dTbles(".language_results_tbody").append(tr_results);
            });
            dTbles(".language_report_div").show();
            dTbles('.language_dashboard_table').DataTable({dom: 'Bfrtip',
                "searching": false,
                "bInfo": false,
                "bPaginate": false,
                buttons: [
                    {
                        extend: 'copyHtml5',
                        exportOptions: {
                            columns: ':contains("Office")'
                        }
                    },
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5'
                ]});
        }
        window.create_client_marital_report = function create_client_marital_report(data) {





            dTbles(".marital_report_div").empty();
            var table = '<table id="marital_dashboard_table" class=" marital_dashboard_table dashboard_report dataTables_processing  table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">\n\
             <thead> <tr> <th>Name</th> <th> No :</th>  </tr> </thead>  \n\
               <tbody id="marital_results_tbody" class="marital_results_tbody"> </tbody> </table>';
            dTbles('.marital_report_div').append(table);
            dTbles.each(data, function (i, val) {
                var name = val.NAME;
                var value = val.VALUE;
                var tr_results = "<tr>\n\
                    <td>" + name + "</td>\n\
                    <td>" + value + "</td>\n\</td>\n\
                    </tr>";
                dTbles(".marital_results_tbody").append(tr_results);
            });
            dTbles(".marital_report_div").show();
            dTbles('.marital_dashboard_table').DataTable({dom: 'Bfrtip',
                "searching": false,
                "bInfo": false,
                "bPaginate": false,
                buttons: [
                    {
                        extend: 'copyHtml5',
                        exportOptions: {
                            columns: ':contains("Office")'
                        }
                    },
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5'
                ]});
        }
        window.create_client_type_report = function create_client_type_report(data) {





            dTbles(".type_report_div").empty();
            var table = '<table id="type_dashboard_table" class=" type_dashboard_table dashboard_report dataTables_processing  table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">\n\
             <thead> <tr> <th>Name</th> <th> No :</th>  </tr> </thead>  \n\
               <tbody id="type_results_tbody" class="type_results_tbody"> </tbody> </table>';
            dTbles('.type_report_div').append(table);
            dTbles.each(data, function (i, val) {
                var name = val.k;
                var value = val.v;
                var tr_results = "<tr>\n\
                    <td>" + name + "</td>\n\
                    <td>" + value + "</td>\n\</td>\n\
                    </tr>";
                dTbles(".type_results_tbody").append(tr_results);
            });
            dTbles(".type_report_div").show();
            dTbles('.type_dashboard_table').DataTable({dom: 'Bfrtip',
                "searching": false,
                "bInfo": false,
                "bPaginate": false,
                buttons: [
                    {
                        extend: 'copyHtml5',
                        exportOptions: {
                            columns: ':contains("Office")'
                        }
                    },
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5'
                ]});
        }

        window.create_client_category_report = function create_client_category_report(data) {





            dTbles(".client_category_report_div").empty();
            var table = '<table id="client_category_dashboard_table" class=" client_category_dashboard_table dashboard_report dataTables_processing  table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">\n\
             <thead> <tr> <th>Name</th> <th> No :</th>  </tr> </thead>  \n\
               <tbody id="client_category_results_tbody" class="client_category_results_tbody"> </tbody> </table>';
            dTbles('.client_category_report_div').append(table);
            dTbles.each(data, function (i, val) {
                var name = val.k;
                var value = val.v;
                var tr_results = "<tr>\n\
                    <td>" + name + "</td>\n\
                    <td>" + value + "</td>\n\</td>\n\
                    </tr>";
                dTbles(".client_category_results_tbody").append(tr_results);
            });
            dTbles(".client_category_report_div").show();
            dTbles('.client_category_dashboard_table').DataTable({dom: 'Bfrtip',
                "searching": false,
                "bInfo": false,
                "bPaginate": false,
                buttons: [
                    {
                        extend: 'copyHtml5',
                        exportOptions: {
                            columns: ':contains("Office")'
                        }
                    },
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5'
                ]});
        }

        window.create_client_age_group_report = function create_client_age_group_report(data) {





            dTbles(".client_age_group_report_div").empty();
            var table = '<table id="client_age_group_dashboard_table" class=" client_age_group_dashboard_table dashboard_report dataTables_processing  table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">\n\
             <thead> <tr> <th>Name</th> <th> No :</th>  </tr> </thead>  \n\
               <tbody id="client_age_group_results_tbody" class="client_age_group_results_tbody"> </tbody> </table>';
            dTbles('.client_age_group_report_div').append(table);
            dTbles.each(data, function (i, val) {
                var name = val.k;
                var value = val.v;
                var tr_results = "<tr>\n\
                    <td>" + name + "</td>\n\
                    <td>" + value + "</td>\n\</td>\n\
                    </tr>";
                dTbles(".client_age_group_results_tbody").append(tr_results);
            });
            dTbles(".client_age_group_report_div").show();
            dTbles('.client_age_group_dashboard_table').DataTable({dom: 'Bfrtip',
                "searching": false,
                "bInfo": false,
                "bPaginate": false,
                buttons: [
                    {
                        extend: 'copyHtml5',
                        exportOptions: {
                            columns: ':contains("Office")'
                        }
                    },
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5'
                ]});
        }

        window.create_client_condition_report = function create_client_condition_report(data) {





            dTbles(".client_condition_report_div").empty();
            var table = '<table id="client_condition_dashboard_table" class=" client_condition_dashboard_table dashboard_report dataTables_processing  table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">\n\
             <thead> <tr> <th>Name</th> <th> No :</th>  </tr> </thead>  \n\
               <tbody id="client_condition_results_tbody" class="client_condition_results_tbody"> </tbody> </table>';
            dTbles('.client_condition_report_div').append(table);
            dTbles.each(data, function (i, val) {
                var name = val.k;
                var value = val.v;
                var tr_results = "<tr>\n\
                    <td>" + name + "</td>\n\
                    <td>" + value + "</td>\n\</td>\n\
                    </tr>";
                dTbles(".client_condition_results_tbody").append(tr_results);
            });
            dTbles(".client_condition_report_div").show();
            dTbles('.client_condition_dashboard_table').DataTable({dom: 'Bfrtip',
                "searching": false,
                "bInfo": false,
                "bPaginate": false,
                buttons: [
                    {
                        extend: 'copyHtml5',
                        exportOptions: {
                            columns: ':contains("Office")'
                        }
                    },
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5'
                ]});
        }

        window.create_client_status_report = function create_client_status_report(data) {





            dTbles(".client_status_report_div").empty();
            var table = '<table id="client_status_dashboard_table" class=" client_status_dashboard_table dashboard_report dataTables_processing  table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">\n\
             <thead> <tr> <th>Name</th> <th> No :</th>  </tr> </thead>  \n\
               <tbody id="client_status_results_tbody" class="client_status_results_tbody"> </tbody> </table>';
            dTbles('.client_status_report_div').append(table);
            dTbles.each(data, function (i, val) {
                var name = val.k;
                var value = val.v;
                var tr_results = "<tr>\n\
                    <td>" + name + "</td>\n\
                    <td>" + value + "</td>\n\</td>\n\
                    </tr>";
                dTbles(".client_status_results_tbody").append(tr_results);
            });
            dTbles(".client_status_report_div").show();
            dTbles('.client_status_dashboard_table').DataTable({dom: 'Bfrtip',
                "searching": false,
                "bInfo": false,
                "bPaginate": false,
                buttons: [
                    {
                        extend: 'copyHtml5',
                        exportOptions: {
                            columns: ':contains("Office")'
                        }
                    },
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5'
                ]});
        }

        window.create_client_consented_report = function create_client_consented_report(data) {





            dTbles(".client_consented_report_div").empty();
            var table = '<table id="client_consented_dashboard_table" class=" client_consented_dashboard_table dashboard_report dataTables_processing  table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">\n\
             <thead> <tr> <th>Name</th> <th> No :</th>  </tr> </thead>  \n\
               <tbody id="client_consented_results_tbody" class="client_consented_results_tbody"> </tbody> </table>';
            dTbles('.client_consented_report_div').append(table);
            dTbles.each(data, function (i, val) {
                var name = val.k;
                var value = val.v;
                var tr_results = "<tr>\n\
                    <td>" + name + "</td>\n\
                    <td>" + value + "</td>\n\</td>\n\
                    </tr>";
                dTbles(".client_consented_results_tbody").append(tr_results);
            });
            dTbles(".client_consented_report_div").show();
            dTbles('.client_consented_dashboard_table').DataTable({dom: 'Bfrtip',
                "searching": false,
                "bInfo": false,
                "bPaginate": false,
                buttons: [
                    {
                        extend: 'copyHtml5',
                        exportOptions: {
                            columns: ':contains("Office")'
                        }
                    },
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5'
                ]});
        }

        window.create_client_consented_gender_report = function create_client_consented_gender_report(data) {





            dTbles(".client_consented_gender_report_div").empty();
            var table = '<table id="client_consented_gender_dashboard_table" class=" client_consented_gender_dashboard_table dashboard_report dataTables_processing  table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">\n\
             <thead> <tr> <th>Name</th> <th> No :</th>  </tr> </thead>  \n\
               <tbody id="client_consented_gender_results_tbody" class="client_consented_gender_results_tbody"> </tbody> </table>';
            dTbles('.client_consented_gender_report_div').append(table);
            dTbles.each(data, function (i, val) {
                var name = val.gender;
                var value = val.total_client;
                var tr_results = "<tr>\n\
                    <td>" + name + "</td>\n\
                    <td>" + value + "</td>\n\</td>\n\
                    </tr>";
                dTbles(".client_consented_gender_results_tbody").append(tr_results);
            });
            dTbles(".client_consented_gender_report_div").show();
            dTbles('.client_consented_gender_dashboard_table').DataTable({dom: 'Bfrtip',
                "searching": false,
                "bInfo": false,
                "bPaginate": false,
                buttons: [
                    {
                        extend: 'copyHtml5',
                        exportOptions: {
                            columns: ':contains("Office")'
                        }
                    },
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5'
                ]});
        }

        window.create_client_consented_marital_report = function create_client_consented_marital_report(data) {





            dTbles(".client_consented_marital_report_div").empty();
            var table = '<table id="client_consented_marital_dashboard_table" class=" client_consented_marital_dashboard_table dashboard_report dataTables_processing  table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">\n\
             <thead> <tr> <th>Name</th> <th> No :</th>  </tr> </thead>  \n\
               <tbody id="client_consented_marital_results_tbody" class="client_consented_marital_results_tbody"> </tbody> </table>';
            dTbles('.client_consented_marital_report_div').append(table);
            dTbles.each(data, function (i, val) {
                var name = val.marital;
                var value = val.total_client;
                var tr_results = "<tr>\n\
                    <td>" + name + "</td>\n\
                    <td>" + value + "</td>\n\</td>\n\
                    </tr>";
                dTbles(".client_consented_marital_results_tbody").append(tr_results);
            });
            dTbles(".client_consented_marital_report_div").show();
            dTbles('.client_consented_marital_dashboard_table').DataTable({dom: 'Bfrtip',
                "searching": false,
                "bInfo": false,
                "bPaginate": false,
                buttons: [
                    {
                        extend: 'copyHtml5',
                        exportOptions: {
                            columns: ':contains("Office")'
                        }
                    },
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5'
                ]});
        }


        window.create_client_consented_category_report = function create_client_consented_category_report(data) {





            dTbles(".client_consented_category_report_div").empty();
            var table = '<table id="client_consented_category_dashboard_table" class=" client_consented_category_dashboard_table dashboard_report dataTables_processing  table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">\n\
             <thead> <tr> <th>Name</th> <th> No :</th>  </tr> </thead>  \n\
               <tbody id="client_consented_category_results_tbody" class="client_consented_category_results_tbody"> </tbody> </table>';
            dTbles('.client_consented_category_report_div').append(table);
            dTbles.each(data, function (i, val) {
                var name = val.group_name;
                var value = val.total_client;
                var tr_results = "<tr>\n\
                    <td>" + name + "</td>\n\
                    <td>" + value + "</td>\n\</td>\n\
                    </tr>";
                dTbles(".client_consented_category_results_tbody").append(tr_results);
            });
            dTbles(".client_consented_category_report_div").show();
            dTbles('.client_consented_category_dashboard_table').DataTable({dom: 'Bfrtip',
                "searching": false,
                "bInfo": false,
                "bPaginate": false,
                buttons: [
                    {
                        extend: 'copyHtml5',
                        exportOptions: {
                            columns: ':contains("Office")'
                        }
                    },
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5'
                ]});
        }


        window.create_client_appointment_status_report = function create_client_appointment_status_report(data) {





            dTbles(".client_appointment_status_report_div").empty();
            var table = '<table id="client_appointment_status_dashboard_table" class=" client_appointment_status_dashboard_table dashboard_report dataTables_processing  table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">\n\
             <thead> <tr> <th>Name</th> <th> No :</th>  </tr> </thead>  \n\
               <tbody id="client_appointment_status_results_tbody" class="client_appointment_status_results_tbody"> </tbody> </table>';
            dTbles('.client_appointment_status_report_div').append(table);
            dTbles.each(data, function (i, val) {
                var name = val.Missed;
                var value = val.total;
                var tr_results = "<tr>\n\
                    <td>" + name + "</td>\n\
                    <td>" + value + "</td>\n\</td>\n\
                    </tr>";
                dTbles(".client_appointment_status_results_tbody").append(tr_results);
            });
            dTbles(".client_appointment_status_report_div").show();
            dTbles('.client_appointment_status_dashboard_table').DataTable({dom: 'Bfrtip',
                "searching": false,
                "bInfo": false,
                "bPaginate": false,
                buttons: [
                    {
                        extend: 'copyHtml5',
                        exportOptions: {
                            columns: ':contains("Office")'
                        }
                    },
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5'
                ]});
        }


        window.create_client_booked_appointment_status_report = function create_client_booked_appointment_status_report(data) {





            dTbles(".client_booked_appointment_status_report_div").empty();
            var table = '<table id="client_booked_appointment_status_dashboard_table" class=" client_booked_appointment_status_dashboard_table dashboard_report dataTables_processing  table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">\n\
             <thead> <tr> <th>Name</th> <th> No :</th>  </tr> </thead>  \n\
               <tbody id="client_booked_appointment_status_results_tbody" class="client_booked_appointment_status_results_tbody"> </tbody> </table>';
            dTbles('.client_booked_appointment_status_report_div').append(table);
            dTbles.each(data, function (i, val) {
                var name = val.group_name;
                var value = val.total;
                var tr_results = "<tr>\n\
                    <td>" + name + "</td>\n\
                    <td>" + value + "</td>\n\</td>\n\
                    </tr>";
                dTbles(".client_booked_appointment_status_results_tbody").append(tr_results);
            });
            dTbles(".client_booked_appointment_status_report_div").show();
            dTbles('.client_booked_appointment_status_dashboard_table').DataTable({dom: 'Bfrtip',
                "searching": false,
                "bInfo": false,
                "bPaginate": false,
                buttons: [
                    {
                        extend: 'copyHtml5',
                        exportOptions: {
                            columns: ':contains("Office")'
                        }
                    },
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5'
                ]});
        }



        window.create_client_notified_appointment_status_report = function create_client_notified_appointment_status_report(data) {





            dTbles(".client_notified_appointment_status_report_div").empty();
            var table = '<table id="client_notified_appointment_status_dashboard_table" class=" client_notified_appointment_status_dashboard_table dashboard_report dataTables_processing  table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">\n\
             <thead> <tr> <th>Name</th> <th> No :</th>  </tr> </thead>  \n\
               <tbody id="client_notified_appointment_status_results_tbody" class="client_notified_appointment_status_results_tbody"> </tbody> </table>';
            dTbles('.client_notified_appointment_status_report_div').append(table);
            dTbles.each(data, function (i, val) {
                var name = val.group_name;
                var value = val.total;
                var tr_results = "<tr>\n\
                    <td>" + name + "</td>\n\
                    <td>" + value + "</td>\n\</td>\n\
                    </tr>";
                dTbles(".client_notified_appointment_status_results_tbody").append(tr_results);
            });
            dTbles(".client_notified_appointment_status_report_div").show();
            dTbles('.client_notified_appointment_status_dashboard_table').DataTable({dom: 'Bfrtip',
                "searching": false,
                "bInfo": false,
                "bPaginate": false,
                buttons: [
                    {
                        extend: 'copyHtml5',
                        exportOptions: {
                            columns: ':contains("Office")'
                        }
                    },
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5'
                ]});
        }




        window.create_client_missed_appointment_status_report = function create_client_missed_appointment_status_report(data) {





            dTbles(".client_missed_appointment_status_report_div").empty();
            var table = '<table id="client_missed_appointment_status_dashboard_table" class=" client_missed_appointment_status_dashboard_table dashboard_report dataTables_processing  table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">\n\
             <thead> <tr> <th>Name</th> <th> No :</th>  </tr> </thead>  \n\
               <tbody id="client_missed_appointment_status_results_tbody" class="client_missed_appointment_status_results_tbody"> </tbody> </table>';
            dTbles('.client_missed_appointment_status_report_div').append(table);
            dTbles.each(data, function (i, val) {
                var name = val.group_name;
                var value = val.total;
                var tr_results = "<tr>\n\
                    <td>" + name + "</td>\n\
                    <td>" + value + "</td>\n\</td>\n\
                    </tr>";
                dTbles(".client_missed_appointment_status_results_tbody").append(tr_results);
            });
            dTbles(".client_missed_appointment_status_report_div").show();
            dTbles('.client_missed_appointment_status_dashboard_table').DataTable({dom: 'Bfrtip',
                "searching": false,
                "bInfo": false,
                "bPaginate": false,
                buttons: [
                    {
                        extend: 'copyHtml5',
                        exportOptions: {
                            columns: ':contains("Office")'
                        }
                    },
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5'
                ]});
        }





        window.create_client_defaulted_appointment_status_report = function create_client_defaulted_appointment_status_report(data) {





            dTbles(".client_defaulted_appointment_status_report_div").empty();
            var table = '<table id="client_defaulted_appointment_status_dashboard_table" class=" client_defaulted_appointment_status_dashboard_table dashboard_report dataTables_processing  table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">\n\
             <thead> <tr> <th>Name</th> <th> No :</th>  </tr> </thead>  \n\
               <tbody id="client_defaulted_appointment_status_results_tbody" class="client_defaulted_appointment_status_results_tbody"> </tbody> </table>';
            dTbles('.client_defaulted_appointment_status_report_div').append(table);
            dTbles.each(data, function (i, val) {
                var name = val.group_name;
                var value = val.total;
                var tr_results = "<tr>\n\
                    <td>" + name + "</td>\n\
                    <td>" + value + "</td>\n\</td>\n\
                    </tr>";
                dTbles(".client_defaulted_appointment_status_results_tbody").append(tr_results);
            });
            dTbles(".client_defaulted_appointment_status_report_div").show();
            dTbles('.client_defaulted_appointment_status_dashboard_table').DataTable({dom: 'Bfrtip',
                "searching": false,
                "bInfo": false,
                "bPaginate": false,
                buttons: [
                    {
                        extend: 'copyHtml5',
                        exportOptions: {
                            columns: ':contains("Office")'
                        }
                    },
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5'
                ]});
        }










        dTbles(document).on('click', ".select_mfl_btn", function () {


            //get data
            var mfl_code = dTbles(this).closest('tr').find('input[name="hidden_mfl_code"]').val();
            var controller = "admin";
            var get_function = "get_facility_details";
            var success_alert = "Facility added successfully ... :) ";
            var error_alert = "An Error Ocurred";
            dTbles.ajax({
                type: "GET",
                async: true,
                url: "<?php echo base_url(); ?>admin/get_facility_details/" + mfl_code,
                dataType: "JSON",
                success: function (response) {

                    dTbles.each(response, function (i, value) {
                        //dTbles(".add_facilty_div").hide();
                        // dTbles(".table_div").show();
                        dTbles(".facility_name").empty();
                        dTbles(".mfl_code").empty();
                        dTbles(".facility_type").empty();
                        dTbles(".facility_county").empty();
                        console.log("Facility Name : " + value.name + "MFL Code : " + value.code + "Facility Type : " + value.facility_type + "Location : " + value.owner);
                        dTbles(".facility_name").val(value.name);
                        dTbles(".mfl_code").val(value.code);
                        dTbles(".facility_type").val(value.facility_type);
                        dTbles(".facility_location").val(value.keph_level);
                        dTbles(".facility_county").val(value.regulatory_body);
                        dTbles(".add_facilty_div").show();
                        dTbles(".table_div").hide();
                    });
                    $(".edit_div").show();
                    $(".table_div").hide();
                }, error: function (data) {
                    sweetAlert("Oops...", "" + error_alert + "", "error");
                }

            });
        });
    });</script>



















<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.js"></script>





<!--
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>-->

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.standalone.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker3.css" />



<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">
    var dtp = jQuery.noConflict();
    dtp(document).ready(function () {
        dtp('#123').datepicker({
            format: "dd/mm/yyyy"
        });
    });
    var date_picker = jQuery.noConflict();
    date_picker(function () {
        date_picker(".appointment_date").datepicker({
            format: 'dd/mm/yyyy',
            startDate: '-0d',
            autoclose: true
        });
        date_picker(".transfer_appointment_date").datepicker({
            format: 'dd/mm/yyyy',
            startDate: '-0d',
            autoclose: true
        });
        date_picker(".dob").datepicker({
            format: 'dd/mm/yyyy',
            endDate: '+0d',
            autoclose: true
        });
        date_picker(".transfer_dob").datepicker({
            format: 'dd/mm/yyyy',
            endDate: '+0d',
            autoclose: true
        });
        date_picker(".date").datepicker({
            format: 'dd/mm/yyyy'
        });
        date_picker(".date_from").datepicker({
            format: 'dd-mm-yyyy',
            endDate: '+0d',
            autoclose: true
        });
        date_picker(".date_to").datepicker({
            format: 'dd-mm-yyyy',
            endDate: '+0d',
            autoclose: true
        });
    });</script>




<script type="text/javascript">
    $(document).ready(function () {







        setInterval(function () {
            partner_info();
            facility_info();
            users_info();
            content_info();
            client_info();
            ok_info();
            not_ok_info();
            deactivated_info();
            appointments_info();
            counties_info();
            notified_info();
            booked_info();
            missed_info();
            defaulted_info();
            groups_info();
            weekly_checkins_info();
            responded_info();
            sender_info();
            pending_info();
            late_info();
            unrecognised_info();
            tb_info();
            pmtct_info();
            adolescents_info();
            new_clients_info();
            art_info();
            paeds_info();
        }, 300000);
        function deactivated_info() {
            $.ajax({
                type: "GET",
                url: "<?php echo base_url(); ?>reports/deactivated_info/",
                dataType: "json",
                success: function (response) {

                    $(".deactivated_info").empty();
                    $(".deactivated_info").append(response);
                }, error: function (data) {
                    //sweetAlert("Oops...", "Something went wrong!", "error");
                }
            })
        }


        function partner_info() {
            $.ajax({
                type: "GET",
                url: "<?php echo base_url(); ?>reports/partner_info/",
                dataType: "json",
                success: function (response) {
                    $(".partner_info").empty();
                    $(".partner_info").append(response);
                }, error: function (data) {
                    //sweetAlert("Oops...", "Something went wrong!", "error");
                }
            })
        }


        function facility_info() {
            $.ajax({
                type: "GET",
                url: "<?php echo base_url(); ?>reports/facility_info/",
                dataType: "json",
                success: function (response) {
                    $(".facility_info").empty();
                    $(".facility_info").append(response);
                }, error: function (data) {
                    //  sweetAlert("Oops...", "Something went wrong!", "error");
                }
            })
        }

        function users_info() {
            $.ajax({
                type: "GET",
                url: "<?php echo base_url(); ?>reports/users_info/",
                dataType: "json",
                success: function (response) {
                    $(".users_info").empty();
                    $(".users_info").append(response);
                }, error: function (data) {
                    // sweetAlert("Oops...", "Something went wrong!", "error");
                }
            })
        }
        function content_info() {
            $.ajax({
                type: "GET",
                url: "<?php echo base_url(); ?>reports/content_info/",
                dataType: "json",
                success: function (response) {
                    $(".content_info").empty();
                    $(".content_info").append(response);
                }, error: function (data) {
                    // sweetAlert("Oops...", "Something went wrong!", "error");
                }
            })
        }

        function client_info() {
            $.ajax({
                type: "GET",
                url: "<?php echo base_url(); ?>reports/client_info/",
                dataType: "json",
                success: function (response) {
                    $(".client_info").empty();
                    $(".client_info").append(response);
                }, error: function (data) {
                    //  sweetAlert("Oops...", "Something went wrong!", "error");
                }
            })
        }


        function module_info() {
            $.ajax({
                type: "GET",
                url: "<?php echo base_url(); ?>reports/module_info/",
                dataType: "json",
                success: function (response) {
                    $(".module_info").empty();
                    $(".module_info").append(response);
                }, error: function (data) {
                    // sweetAlert("Oops...", "Something went wrong!", "error");
                }
            })
        }


        function ok_info() {
            $.ajax({
                type: "GET",
                url: "<?php echo base_url(); ?>reports/ok_info/",
                dataType: "json",
                success: function (response) {
                    $(".ok_info").empty();
                    $(".ok_info").append(response);
                }, error: function (data) {
                    // sweetAlert("Oops...", "Something went wrong!", "error");
                }
            })
        }


        function not_ok_info() {
            $.ajax({
                type: "GET",
                url: "<?php echo base_url(); ?>reports/not_ok_info/",
                dataType: "json",
                success: function (response) {
                    $(".not_ok_info").empty();
                    $(".not_ok_info").append(response);
                }, error: function (data) {
                    // sweetAlert("Oops...", "Something went wrong!", "error");
                }
            })
        }


        function appointments_info() {
            $.ajax({
                type: "GET",
                url: "<?php echo base_url(); ?>reports/appointments_info/",
                dataType: "json",
                success: function (response) {
                    $(".appointments_info").empty();
                    $(".appointments_info").append(response);
                }, error: function (data) {
                    //  sweetAlert("Oops...", "Something went wrong!", "error");
                }
            })
        }



        function counties_info() {
            $.ajax({
                type: "GET",
                url: "<?php echo base_url(); ?>reports/counties_info/",
                dataType: "json",
                success: function (response) {
                    $(".counties_info").empty();
                    $(".counties_info").append(response);
                }, error: function (data) {
                    //   sweetAlert("Oops...", "Something went wrong!", "error");
                }
            })
        }




        function sub_counties_info() {
            $.ajax({
                type: "GET",
                url: "<?php echo base_url(); ?>reports/sub_counties_info/",
                dataType: "json",
                success: function (response) {
                    $(".sub_counties_info").empty();
                    $(".sub_counties_info").append(response);
                }, error: function (data) {
                    //  sweetAlert("Oops...", "Something went wrong!", "error");
                }
            })
        }




        function notified_info() {
            $.ajax({
                type: "GET",
                url: "<?php echo base_url(); ?>reports/notified_info/",
                dataType: "json",
                success: function (response) {
                    $(".notified_info").empty();
                    $(".notified_info").append(response);
                }, error: function (data) {
                    //  sweetAlert("Oops...", "Something went wrong!", "error");
                }
            })
        }



        function booked_info() {
            $.ajax({
                type: "GET",
                url: "<?php echo base_url(); ?>reports/booked_info/",
                dataType: "json",
                success: function (response) {
                    $(".booked_info").empty();
                    $(".booked_info").append(response);
                }, error: function (data) {
                    //  sweetAlert("Oops...", "Something went wrong!", "error");
                }
            })
        }



        function missed_info() {
            $.ajax({
                type: "GET",
                url: "<?php echo base_url(); ?>reports/missed_info/",
                dataType: "json",
                success: function (response) {
                    $(".missed_info").empty();
                    $(".missed_info").append(response);
                }, error: function (data) {
                    //  sweetAlert("Oops...", "Something went wrong!", "error");
                }
            })
        }






        function defaulted_info() {
            $.ajax({
                type: "GET",
                url: "<?php echo base_url(); ?>reports/defaulted_info/",
                dataType: "json",
                success: function (response) {
                    $(".defaulted_info").empty();
                    $(".defaulted_info").append(response);
                }, error: function (data) {
                    //  sweetAlert("Oops...", "Something went wrong!", "error");
                }
            })
        }



        function groups_info() {
            $.ajax({
                type: "GET",
                url: "<?php echo base_url(); ?>reports/groups_info/",
                dataType: "json",
                success: function (response) {
                    $(".groups_info").empty();
                    $(".groups_info").append(response);
                }, error: function (data) {
                    //  sweetAlert("Oops...", "Something went wrong!", "error");
                }
            })
        }





        function pmtct_info() {
            $.ajax({
                type: "GET",
                url: "<?php echo base_url(); ?>reports/pmtct_info/",
                dataType: "json",
                success: function (response) {
                    $(".groups_info").empty();
                    $(".groups_info").append(response);
                }, error: function (data) {
                    //  sweetAlert("Oops...", "Something went wrong!", "error");
                }
            })
        }



        function tb_info() {
            $.ajax({
                type: "GET",
                url: "<?php echo base_url(); ?>reports/tb_info/",
                dataType: "json",
                success: function (response) {
                    $(".groups_info").empty();
                    $(".groups_info").append(response);
                }, error: function (data) {
                    //  sweetAlert("Oops...", "Something went wrong!", "error");
                }
            })
        }


        function adolescents_info() {
            $.ajax({
                type: "GET",
                url: "<?php echo base_url(); ?>reports/adolescents_info/",
                dataType: "json",
                success: function (response) {
                    $(".groups_info").empty();
                    $(".groups_info").append(response);
                }, error: function (data) {
                    //  sweetAlert("Oops...", "Something went wrong!", "error");
                }
            })
        }



        function new_clients_info() {
            $.ajax({
                type: "GET",
                url: "<?php echo base_url(); ?>reports/new_clients_info/",
                dataType: "json",
                success: function (response) {
                    $(".groups_info").empty();
                    $(".groups_info").append(response);
                }, error: function (data) {
                    //  sweetAlert("Oops...", "Something went wrong!", "error");
                }
            })
        }



        function unsuppressed_info() {
            $.ajax({
                type: "GET",
                url: "<?php echo base_url(); ?>reports/unsuppressed_info/",
                dataType: "json",
                success: function (response) {
                    $(".groups_info").empty();
                    $(".groups_info").append(response);
                }, error: function (data) {
                    //  sweetAlert("Oops...", "Something went wrong!", "error");
                }
            })
        }


        function art_info() {
            $.ajax({
                type: "GET",
                url: "<?php echo base_url(); ?>reports/art_info/",
                dataType: "json",
                success: function (response) {
                    $(".groups_info").empty();
                    $(".groups_info").append(response);
                }, error: function (data) {
                    //  sweetAlert("Oops...", "Something went wrong!", "error");
                }
            })
        }

        function paeds_info() {
            $.ajax({
                type: "GET",
                url: "<?php echo base_url(); ?>reports/paeds_info/",
                dataType: "json",
                success: function (response) {
                    $(".groups_info").empty();
                    $(".groups_info").append(response);
                }, error: function (data) {
                    //  sweetAlert("Oops...", "Something went wrong!", "error");
                }
            })
        }
        function hiv_tb_info() {
            $.ajax({
                type: "GET",
                url: "<?php echo base_url(); ?>reports/hiv_tb_info/",
                dataType: "json",
                success: function (response) {
                    $(".groups_info").empty();
                    $(".groups_info").append(response);
                }, error: function (data) {
                    //  sweetAlert("Oops...", "Something went wrong!", "error");
                }
            })
        }


        function weekly_checkins_info() {
            $.ajax({
                type: "GET",
                url: "<?php echo base_url(); ?>reports/weekly_checkins_info/",
                dataType: "json",
                success: function (response) {
                    $(".weekly_checkins_info").empty();
                    $(".weekly_checkins_info").append(response);
                }, error: function (data) {
                    //  sweetAlert("Oops...", "Something went wrong!", "error");
                }
            })
        }


        function responded_info() {
            $.ajax({
                type: "GET",
                url: "<?php echo base_url(); ?>reports/responded_info/",
                dataType: "json",
                success: function (response) {
                    $(".responded_info").empty();
                    $(".responded_info").append(response);
                }, error: function (data) {
                    //  sweetAlert("Oops...", "Something went wrong!", "error");
                }
            })
        }


        function sender_info() {
            $.ajax({
                type: "GET",
                url: "<?php echo base_url(); ?>reports/sender_info/",
                dataType: "json",
                success: function (response) {
                    $(".sender_info").empty();
                    $(".sender_info").append(response);
                }, error: function (data) {
                    //  sweetAlert("Oops...", "Something went wrong!", "error");
                }
            })
        }


        function pending_info() {
            $.ajax({
                type: "GET",
                url: "<?php echo base_url(); ?>reports/pending_info/",
                dataType: "json",
                success: function (response) {
                    $(".pending_info").empty();
                    $(".pending_info").append(response);
                }, error: function (data) {
                    //  sweetAlert("Oops...", "Something went wrong!", "error");
                }
            })
        }


        function late_info() {
            $.ajax({
                type: "GET",
                url: "<?php echo base_url(); ?>reports/late_info/",
                dataType: "json",
                success: function (response) {
                    $(".late_info").empty();
                    $(".late_info").append(response);
                }, error: function (data) {
                    //  sweetAlert("Oops...", "Something went wrong!", "error");
                }
            })
        }

        function unrecognised_info() {
            $.ajax({
                type: "GET",
                url: "<?php echo base_url(); ?>reports/unrecognised_info/",
                dataType: "json",
                success: function (response) {
                    $(".unrecognised_info").empty();
                    $(".unrecognised_info").append(response);
                }, error: function (data) {
                    //  sweetAlert("Oops...", "Something went wrong!", "error");
                }
            })
        }
    });</script>







<script type="text/javascript">
    var myVar;
    function showTime() {
        var d = new Date();
        var t = d.toLocaleTimeString();
        $("#clock").html("Current Time : " + d); // display time on the page
        $(".current_time").html(" " + d); // display time on the page
    }
    function stopFunction() {
        clearInterval(myVar); // stop the timer
    }
    $(document).ready(function () {
        setInterval(function () {

        }, 300000);
        setInterval(function () {
            window.location.reload(1);
        }, 3000000);
        setInterval('showTime()', 1000);
        function sender() {
            $(".sender_p").empty();
            $.ajax({
                url: "<?php echo base_url(); ?>chore/sender",
                type: 'GET',
                dataType: 'JSON',
                success: function (data) {
                    $(".sender_p").append("Sender is running");
                }, error: function (errorThrown) {
                    $(".sender_p").append("Sender is not running");
                }
            });
        }

        function scheduler() {
            $(".scheduler_p").empty();
            $.ajax({
                url: "<?php echo base_url(); ?>chore/scheduler",
                type: 'GET',
                dataType: 'JSON',
                success: function (data) {
                    $(".scheduler_p").append("Scheduler is running");
                }, error: function (errorThrown) {
                    $(".scheduler_p").append("Scheduler is not running");
                }
            });
        }

        function receiver() {
            $(".receiver_p").empty();
            $.ajax({
                url: "<?php echo base_url(); ?>chore/receiver",
                type: 'GET',
                dataType: 'JSON',
                success: function (data) {
                    $(".receiver_p").append("Receiver is running");
                }, error: function (errorThrown) {
                    $(".receiver_p").append("Receiver is not running");
                }
            });
        }


        function trigger() {
            $(".trigger_p").empty();
            $.ajax({
                url: "<?php echo base_url(); ?>chore/trigger",
                type: 'GET',
                dataType: 'JSON',
                success: function (data) {
                    $(".trigger_p").append("Trigger is running");
                }, error: function (errorThrown) {
                    $(".trigger_p").append("Trigger is not running");
                }
            });
        }

        function send_mail() {
            $(".send_mail_p").empty();
            $.ajax({
                url: "<?php echo base_url(); ?>chore/send_mail",
                type: 'GET',
                dataType: 'JSON',
                success: function (data) {
                    $(".send_mail_p").append("Send mail is running");
                }, error: function (errorThrown) {
                    $(".send_mail_p").append("Send mail is not running");
                }
            });
        }

        function broadcast() {
            $(".broadcast_p").empty();
            $.ajax({
                url: "<?php echo base_url(); ?>chore/broadcast",
                type: 'GET',
                dataType: 'JSON',
                success: function (data) {
                    $(".broadcast_p").append("Broadcast is running");
                }, error: function (errorThrown) {
                    $(".broadcast_p").append("Broadcast is not running");
                }
            });
        }

    });</script>






<div class="loading" id="loading" >
    <div class="panel-bod">
        <i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>
        <span class="sr-only">Loading...</span>
    </div>

    <!-- Place at bottom of page --></div>
<style type="text/css">
    /* Start by setting display:none to make this hidden.
Then we position it in relation to the viewport window
with position:fixed. Width, height, top and left speak
for themselves. Background we set to 80% white with
our animation centered, and no-repeating */
    .loader {
        display:    inline;
        position:   fixed;
        z-index:    1000;
        top:        0;
        left:       0;
        height:     100%;
        width:      100%;

        background: rgba( 255, 255, 255, .8 ) 
            50% 50% 
            no-repeat;
    }
</style>

<link rel="stylesheet" href="http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>





<script src="//code.jquery.com/jquery-2.1.4.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>





<!-- javascripts -->
<script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-ui-1.9.2.custom.min.js"></script>
<!-- bootstrap -->
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<!-- nice scroll -->
<script src="<?php echo base_url() ?>assets/js/jquery.scrollTo.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.nicescroll.js" type="text/javascript"></script>
<!-- custom select -->
<script src="<?php echo base_url(); ?>assets/js/jquery.customSelect.min.js" ></script>
<!--custome script for all page-->
<script src="<?php echo base_url(); ?>assets/js/scripts.js"></script>



<script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-Knob/1.2.13/jquery.knob.min.js"></script>


<script>
    $(function () {
        $(".county_performance").knob();
        $(".facility_performance").knob();
        $(".client_performance").knob();
    });
    function county_performance() {
        $.ajax({
            url: "<?php echo base_url() ?>reports/county_target",
            type: 'GAT',
            dataType: 'JSON',
            success: function (data) {
                var data = data;
                $(".county_performance").val(data);
            }, error: function (error) {
                console.log(error);
            }
        });
    }
    function facility_performance() {
        $.ajax({
            url: "<?php echo base_url() ?>reports/facility_target",
            type: 'GAT',
            dataType: 'JSON',
            success: function (data) {
                var data = data;
                $(".client_performance").val(data);
            }, error: function (error) {
                console.log(error);
            }
        });
    }
</script>



<!-- END CHART STYLES    -->


</section>
</section>
<!--main content end-->


</section>



</body>
<!-- END BODY-->

</html>
