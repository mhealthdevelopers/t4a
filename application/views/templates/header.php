<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><html lang="en"> 
    <!-- BEGIN HEAD--><head>

        <meta charset="UTF-8" />
        <title>T4A-Text 4 Adherence </title>
        <!-- <META HTTP-EQUIV="REFRESH" CONTENT="90">   -->  
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!--[if IE]>
           <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
           <![endif]-->
        <!-- GLOBAL STYLES -->
        <!-- GLOBAL STYLES -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/template/assets/plugins/bootstrap/css/bootstrap.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/template/assets/css/main.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/template/assets/css/theme.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/template/assets/css/MoneAdmin.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/template/assets/plugins/Font-Awesome/css/font-awesome.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/style.css" />
        <!--END GLOBAL STYLES -->
        <!--start crud styles 
     <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        -->
        <style type="text/css">
            table tr th, table tr td{font-size: 1.2rem;}
            .row{ margin:20px 20px 20px 20px;width: 100%;}
            .glyphicon{font-size: 20px;}
            .glyphicon-plus{float: right;color:#3399FF}
            a.glyphicon{text-decoration: none;color:#3399FF}
            a.glyphicon-trash{margin-left: 10px;color:#3399FF}
            .none{display: none;}
        </style>

        <!--end crud styles--> 
        <!-- START CHART STYLES    --> 
        <!-- Index Chart    -->   
        <script src="<?php echo base_url(); ?>assets/charts/lib/js/jquery.min.js"></script>
        <!-- END CHART STYLES    -->
        <!-- PAGE LEVEL STYLES -->
        <!-- END PAGE LEVEL  STYLES -->
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script type="text/javascript" src="<?php echo base_url(); ?>assets/date/jquery-1.4.2.min.js"></script>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/date/cupertino/jquery.ui.all.css" type="text/css">
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/date/jquery.ui.core.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/date/jquery.ui.widget.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/date/jquery.ui.datepicker.min.js"></script>
        <style type="text/css">
            .ui-datepicker
            {
                font-family: Arial;
                font-size: 13px;
                z-index: 1003 !important;
                background-color:#A91E22;
                color:#ffffff;
            }
        </style>



        <!-- Bootstrap CSS -->    
        <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
        <!-- bootstrap theme -->
        <link href="<?php echo base_url(); ?>assets/css/bootstrap-theme.css" rel="stylesheet">
        <!--external css-->
        <!-- font icon -->
        <link href="<?php echo base_url(); ?>assets/css/elegant-icons-style.css" rel="stylesheet" />
        <!-- owl carousel -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/owl.carousel.css" type="text/css">

        <!-- Custom styles -->
        <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/style-responsive.css" rel="stylesheet" />


        <link href="<?php echo base_url(); ?>assets/css/jquery-ui.css" rel="stylesheet" />
<!--        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/uniform/themes/default/css/uniform.default.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/inputlimiter/jquery.inputlimiter.1.0.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/chosen/chosen.min.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/colorpicker/css/colorpicker.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/tagsinput/jquery.tagsinput.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/daterangepicker/daterangepicker-bs3.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datepicker/css/datepicker.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/timepicker/css/bootstrap-timepicker.min.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/switch/static/stylesheets/bootstrap-switch.css" />-->





        <style type="text/css">
            /* Center the loader */
            #loader {
                position: absolute;
                left: 50%;
                top: 50%;
                z-index: 1;
                width: 150px;
                height: 150px;
                margin: -75px 0 0 -75px;
                border: 16px solid #f3f3f3;
                border-radius: 50%;
                border-top: 16px solid #3498db;
                width: 120px;
                height: 120px;
                -webkit-animation: spin 2s linear infinite;
                animation: spin 2s linear infinite;
            }

            @-webkit-keyframes spin {
                0% { -webkit-transform: rotate(0deg); }
                100% { -webkit-transform: rotate(360deg); }
            }

            @keyframes spin {
                0% { transform: rotate(0deg); }
                100% { transform: rotate(360deg); }
            }

            /* Add animation to "page content" */
            .animate-bottom {
                position: relative;
                -webkit-animation-name: animatebottom;
                -webkit-animation-duration: 1s;
                animation-name: animatebottom;
                animation-duration: 1s
            }

            @-webkit-keyframes animatebottom {
                from { bottom:-100px; opacity:0 } 
                to { bottom:0px; opacity:1 }
            }

            @keyframes animatebottom { 
                from{ bottom:-100px; opacity:0 } 
                to{ bottom:0; opacity:1 }
            }

            #myDiv {
                display: none;
                text-align: center;
            }
        </style>



    </head>
    <!-- END  HEAD-->
    <!-- BEGIN BODY-->
    <body class="padTop53 " >

        <!-- container section start -->
        <section id="container" class="">



            <!--header start-->
            <header class="header white-bg">
                <div class="toggle-nav">
                    <div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom"></div>
                </div>


                <!--logo start-->
                <div class="nav search-row" id="top_menu">


                    <a href="<?php echo base_url(); ?>" class="logo">
                        <img style="max-height: 100%; max-width: 100%;" src="<?php echo base_url(); ?>assets/images/moh.png" alt="" /></a>

                    <?php
                    $donor_id = $this->session->userdata('donor_id');
                    $facility_id = $this->session->userdata('facility_id');
                    $partner_id = $this->session->userdata('partner_id');
                    if (!empty($donor_id)) {
                        $donor_logo = $this->session->userdata('logo');
                        ?>
                        <a href="<?php echo base_url(); ?>" class="logo">
                            <img style="max-height: 100%; max-width: 100%;" src="<?php echo base_url(); ?>uploads/logo/Donor/<?php echo $donor_logo; ?>" alt="" />
                        </a>
                        <?php
                    } elseif (!empty($facility_id)) {
                        $partner_logo = $this->session->userdata('logo');
                        ?>
                        <a href="<?php echo base_url(); ?>" class="logo">
                            <img style="max-height: 100%; max-width: 100%;" src="<?php echo base_url(); ?>uploads/logo/Partner/<?php echo $partner_logo; ?>" alt="" />
                        </a>
                        <?php
                    } elseif (!empty($partner_id)) {
                        $partner_logo = $this->session->userdata('logo');
                        ?>
                        <a href="<?php echo base_url(); ?>" class="logo">
                            <img style="max-height: 100%; max-width: 100%;" src="<?php echo base_url(); ?>uploads/logo/Partner/<?php echo $partner_logo; ?>" alt="" />
                        </a>
                        <?php
                    } else {
                        ?>

                        <?php
                    }
                    ?>



                    <a href="<?php echo base_url(); ?>" class="logo"  >
                        <img style="max-height: 100%; max-width: 100%;"  src="<?php echo base_url(); ?>assets/images/t4a_logo.png" alt="Ushauri" /></a>



                </div>  
                <!--logo end-->

                <div class="nav search-row" id="top_menu">
                    <!--  search form start -->

                    <?php
                    $access_level = $this->session->userdata('access_level');


                    if ($access_level == "Admin") {
                        ?>
                        <span> Access Level :  Administrator </span>
                        <?php
                    } elseif ($access_level == "Donor") {

                        foreach ($output as $value) {
                            $name = $value->name;
                            ?>
                            <span> Access Level :  Donor </span><br/>
                            <span>Donor Name : <?php echo $name; ?> </span>

                            <?php
                        }
                    } elseif ($access_level == "Partner") {

                        foreach ($output as $value) {
                            $name = $value->name;
                            ?>
                            <span> Access Level :  Partner </span><br/>
                            <span>Partner Name : <?php echo $name; ?> </span>

                            <?php
                        }
                    } elseif ($access_level == "County") {

                        foreach ($output as $value) {
                            $name = $value->name;
                            ?>
                            <span> Access Level :  County </span><br/>
                            <span>County Name : <?php echo $name; ?> </span>

                            <?php
                        }
                    } elseif ($access_level == "Sub County") {

                        foreach ($output as $value) {
                            $name = $value->name;
                            ?>
                            <span> Access Level :  Sub County </span><br/>
                            <span>Sub County Name : <?php echo $name; ?> </span>

                            <?php
                        }
                    } elseif ($access_level == "Facility") {

                        foreach ($output as $value) {
                            $name = $value->name;
                            $mfl_code = $value->code;
                            ?>
                            <span> Access Level :  Facility </span><br/>
                            <span>Facility Name : <?php echo $name; ?> </span> :    
                            <span>MFL Code : <?php echo $mfl_code; ?></span>
                            <?php
                        }
                    }
                    ?>
                    <li id="clock"></li>
                    <!--  search form end -->
                </div>
                <div class="top-nav notification-row">
                    <!-- notificatoin dropdown start-->
                    <ul class="nav pull-right top-menu">



                        <!-- user login dropdown start-->
                        <li class="dropdown">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <span class="profile-ava">
                                    <img alt="" src="<?php echo base_url(); ?>assets/img/avatar1_small.jpg">
                                </span>
                                <span class="username"><?php echo $this->session->userdata('Fullname'); ?></span>
                                <b class="caret"></b>
                            </a>


                            <ul class="dropdown-menu extended logout">
                                <div class="log-arrow-up"></div>
                                <li class="eborder-top">
                                    <a href="<?php echo base_url(); ?>"><i class="icon_profile"></i> My Profile</a>
                                </li>


                                <li>
                                    <a href="<?php echo base_url(); ?>/Logout"><i class="icon_key_alt"></i> Log Out</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>"><i class="icon_book"></i> Documentation</a>
                                </li>

                            </ul>
                        </li>
                        <!-- user login dropdown end -->
                    </ul>
                    <!-- notificatoin dropdown end-->
                </div>
            </header>      
            <!--header end-->


            <!--sidebar start-->
            <aside style="padding-top: 60px;" >
                <div id="sidebar"  class="nav-collapse ">
                    <!-- sidebar menu start-->
                    <ul class="sidebar-menu">                
                        <li class="">
                            <a class="" href="<?php echo base_url(); ?>">
                                <i class="icon_house_alt"></i>
                                <span>Dashboard</span>
                            </a>
                        </li>




                        <?php
                        $array = $side_functions;
                        $number = array_column($array, 'level');
                        $found_key = array_search('2', $number);

                        $level2 = '2';
                        $newArray = array_filter($array, function ($var) use ($level2) {
                            return ($var['level'] == $level2);
                        });



                        $names = array_column($newArray, 'id');
                        $level2 = array_filter($names);

                        if (!empty($level2)) {
                            ?>
                            <li class="sub-menu">
                                <a href="javascript:;" class="">
                                    <i class="icon_desktop"></i>
                                    <span>Clients</span>
                                    <span class="menu-arrow arrow_carrot-right"></span>
                                </a>
                                <ul class="sub">
                                    <?php
                                    foreach ($side_functions as $value) {
                                        $level2 = $value['level'];
                                        if ($level2 == 2) {
                                            ?>

                                            <li>
                                                <a class="" href='<?php echo base_url() ?><?php echo $value['controller'] . '/' . $value['function']; ?>'>
                                                    <i class="<?php echo $value['icon_class']; ?>"></i>
                                                    <span>
                                                        <?php echo $value['module']; ?>  
                                                    </span>
                                                    <span class="<?php echo $value['span_class']; ?>"></span>
                                                </a>
                                            </li>

                                            <?php
                                        }
                                    }
                                    ?>
                                </ul>
                            </li> 
                            <?php
                        } else {
                            ?>
                            <?php
                        }
                        ?>

                        <?php
                        $array = $side_functions;
                        $number = array_column($array, 'level');
                        $found_key = array_search('2', $number);

                        $level3 = '3';
                        $newArray = array_filter($array, function ($var) use ($level3) {
                            return ($var['level'] == $level3);
                        });



                        $names = array_column($newArray, 'id');
                        $level3 = array_filter($names);

                        if (!empty($level3)) {
                            ?>







                            <li class="sub-menu">
                                <a href="javascript:;" class="">
                                    <i class="icon_desktop"></i>
                                    <span>Appointments</span>
                                    <span class="menu-arrow arrow_carrot-right"></span>
                                </a>
                                <ul class="sub">







                                    <li class="sub-menu">
                                        <a href="javascript:;" class="">

                                            <span>Appointments Diary </span>
                                            <span class="menu-arrow arrow_carrot-right"></span>
                                        </a>
                                        <ul class="sub" style="display:none;">
                                            <?php
                                            foreach ($side_functions as $value) {
                                                $level3 = $value['level'];
                                                if ($level3 == 3) {
                                                    ?>
                                                    <?php
                                                    $function = $value['function'];

                                                    if ($function == 'today_appointments' or $function == 'notified_clients' or $function == 'create_appointment') {
                                                        ?>
                                                        <li>
                                                            <a class="" href='<?php echo base_url() ?><?php echo $value['controller'] . '/' . $value['function']; ?>'>
                                                                <i class="<?php echo $value['icon_class']; ?>"></i>
                                                                <span>
                                                                    <?php echo $value['module']; ?>  
                                                                </span>
                                                                <span class="<?php echo $value['span_class']; ?>"></span>
                                                            </a>
                                                        </li>
                                                        <?php
                                                    }
                                                    ?>

                                                    <?php
                                                }
                                            }
                                            ?>


                                        </ul></li>

                                    <li class="sub-menu">
                                        <a href="javascript:;" class="">

                                            <span>Defaulters Diary</span>
                                            <span class="menu-arrow arrow_carrot-right"></span>
                                        </a>
                                        <ul class="sub" style="display:none;">
                                            <?php
                                            foreach ($side_functions as $value) {
                                                $level3 = $value['level'];
                                                if ($level3 == 3) {
                                                    ?>
                                                    <?php
                                                    $function = $value['function'];

                                                    if ($function == 'missed_clients' or $function == 'defaulted_clients' or $function == 'ltfu_clients') {
                                                        ?>
                                                        <li>
                                                            <a class="" href='<?php echo base_url() ?><?php echo $value['controller'] . '/' . $value['function']; ?>'>
                                                                <i class="<?php echo $value['icon_class']; ?>"></i>
                                                                <span>
                                                                    <?php echo $value['module']; ?>  
                                                                </span>
                                                                <span class="<?php echo $value['span_class']; ?>"></span>
                                                            </a>
                                                        </li>
                                                        <?php
                                                    }
                                                    ?>

                                                    <?php
                                                }
                                            }
                                            ?>


                                        </ul></li>


                                    <?php
                                    foreach ($side_functions as $value) {
                                        $level3 = $value['level'];
                                        if ($level3 == 3) {
                                            ?>
                                            <?php
                                            $function = $value['function'];

                                            if ($function == 'show_calendar' or $function == 'appointment_diary' or $function == 'appointments') {
                                                ?>
                                                <li>
                                                    <a class="" href='<?php echo base_url() ?><?php echo $value['controller'] . '/' . $value['function']; ?>'>
                                                        <i class="<?php echo $value['icon_class']; ?>"></i>
                                                        <span>
                                                            <?php echo $value['module']; ?>  
                                                        </span>
                                                        <span class="<?php echo $value['span_class']; ?>"></span>
                                                    </a>
                                                </li>
                                                <?php
                                            }
                                            ?>

                                            <?php
                                        }
                                    }
                                    ?>






                                </ul>
                            </li> 
                            <?php
                        } else {
                            ?>
                            <?php
                        }
                        ?>

                        <?php
                        $array = $side_functions;
                        $number = array_column($array, 'level');
                        $found_key = array_search('2', $number);

                        $level4 = '4';
                        $newArray = array_filter($array, function ($var) use ($level4) {
                            return ($var['level'] == $level4);
                        });



                        $names = array_column($newArray, 'id');
                        $level4 = array_filter($names);

                        if (!empty($level4)) {
                            ?>
                            <li class="sub-menu">
                                <a href="javascript:;" class="">
                                    <i class="icon_desktop"></i>
                                    <span>Wellness</span>
                                    <span class="menu-arrow arrow_carrot-right"></span>
                                </a>
                                <ul class="sub">
                                    <?php
                                    foreach ($side_functions as $value) {
                                        $level4 = $value['level'];
                                        if ($level4 == 4) {
                                            ?>

                                            <li>
                                                <a class="" href='<?php echo base_url() ?><?php echo $value['controller'] . '/' . $value['function']; ?>'>
                                                    <i class="<?php echo $value['icon_class']; ?>"></i>
                                                    <span>
                                                        <?php echo $value['module']; ?>  
                                                    </span>
                                                    <span class="<?php echo $value['span_class']; ?>"></span>
                                                </a>
                                            </li>

                                            <?php
                                        }
                                    }
                                    ?>
                                </ul>
                            </li> 
                            <?php
                        } else {
                            ?>
                            <?php
                        }
                        ?>

                        <?php
                        $array = $side_functions;
                        $number = array_column($array, 'level');
                        $found_key = array_search('2', $number);

                        $level5 = '5';
                        $newArray = array_filter($array, function ($var) use ($level5) {
                            return ($var['level'] == $level5);
                        });



                        $names = array_column($newArray, 'id');
                        $level5 = array_filter($names);

                        if (!empty($level5)) {
                            ?>
                            <li class="sub-menu">
                                <a href="javascript:;" class="">
                                    <i class="icon_desktop"></i>
                                    <span>Group</span>
                                    <span class="menu-arrow arrow_carrot-right"></span>
                                </a>
                                <ul class="sub">
                                    <?php
                                    foreach ($side_functions as $value) {
                                        $level5 = $value['level'];
                                        if ($level5 == 5) {
                                            ?>

                                            <li>
                                                <a class="" href='<?php echo base_url() ?><?php echo $value['controller'] . '/' . $value['function']; ?>'>
                                                    <i class="<?php echo $value['icon_class']; ?>"></i>
                                                    <span>
                                                        <?php echo $value['module']; ?>  
                                                    </span>
                                                    <span class="<?php echo $value['span_class']; ?>"></span>
                                                </a>
                                            </li>

                                            <?php
                                        }
                                    }
                                    ?>
                                </ul>
                            </li> 
                            <?php
                        } else {
                            ?>
                            <?php
                        }
                        ?>

                        <?php
                        $array = $side_functions;
                        $number = array_column($array, 'level');
                        $found_key = array_search('2', $number);

                        $level6 = '6';
                        $newArray = array_filter($array, function ($var) use ($level6) {
                            return ($var['level'] == $level6);
                        });



                        $names = array_column($newArray, 'id');
                        $level6 = array_filter($names);

                        if (!empty($level6)) {
                            ?>
                            <li class="sub-menu">
                                <a href="javascript:;" class="">
                                    <i class="icon_desktop"></i>
                                    <span>Administration</span>
                                    <span class="menu-arrow arrow_carrot-right"></span>
                                </a>
                                <ul class="sub">
                                    <?php
                                    foreach ($side_functions as $value) {
                                        $level6 = $value['level'];
                                        if ($level6 == 6) {
                                            ?>

                                            <li>
                                                <a class="" href='<?php echo base_url() ?><?php echo $value['controller'] . '/' . $value['function']; ?>'>
                                                    <i class="<?php echo $value['icon_class']; ?>"></i>
                                                    <span>
                                                        <?php echo $value['module']; ?>  
                                                    </span>
                                                    <span class="<?php echo $value['span_class']; ?>"></span>
                                                </a>
                                            </li>

                                            <?php
                                        }
                                    }
                                    ?>
                                </ul>
                            </li> 
                            <?php
                        } else {
                            ?>
                            <?php
                        }
                        ?>

                        <?php
                        $array = $side_functions;


                        $level7 = '7';
                        $newArray = array_filter($array, function ($var) use ($level7) {
                            return ($var['level'] == $level7);
                        });



                        $names = array_column($newArray, 'id');
                        $level7 = array_filter($names);

                        if (!empty($level7)) {
                            ?>
                            <li class="sub-menu">
                                <a href="javascript:;" class="">
                                    <i class="icon_desktop"></i>
                                    <span>Reports</span>
                                    <span class="menu-arrow arrow_carrot-right"></span>
                                </a>
                                <ul class="sub">
                                    <?php
                                    foreach ($side_functions as $value) {
                                        $level7 = $value['level'];
                                        if ($level7 == 7) {
                                            ?>

                                            <li>
                                                <a class="" href='<?php echo base_url() ?><?php echo $value['controller'] . '/' . $value['function']; ?>'>
                                                    <i class="<?php echo $value['icon_class']; ?>"></i>
                                                    <span>
                                                        <?php echo $value['module']; ?>  
                                                    </span>
                                                    <span class="<?php echo $value['span_class']; ?>"></span>
                                                </a>
                                            </li>

                                            <?php
                                        }
                                    }
                                    ?>
                                </ul>
                            </li> 
                            <?php
                        } else {
                            ?>
                            <?php
                        }
                        ?>
                        <?php
                        $array = $side_functions;


                        $level8 = '8';
                        $newArray = array_filter($array, function ($var) use ($level8) {
                            return ($var['level'] == $level8);
                        });



                        $names = array_column($newArray, 'id');
                        $level8 = array_filter($names);

                        if (!empty($level8)) {
                            ?>
                            <li class="sub-menu">
                                <a href="javascript:;" class="">
                                    <i class="icon_desktop"></i>
                                    <span>Support</span>
                                    <span class="menu-arrow arrow_carrot-right"></span>
                                </a>
                                <ul class="sub">
                                    <?php
                                    foreach ($side_functions as $value) {
                                        $level8 = $value['level'];
                                        if ($level8 == 8) {
                                            ?>

                                            <li>
                                                <a class="" href='<?php echo base_url() ?><?php echo $value['controller'] . '/' . $value['function']; ?>'>
                                                    <i class="<?php echo $value['icon_class']; ?>"></i>
                                                    <span>
                                                        <?php echo $value['module']; ?>  
                                                    </span>
                                                    <span class="<?php echo $value['span_class']; ?>"></span>
                                                </a>
                                            </li>

                                            <?php
                                        }
                                    }
                                    ?>
                                </ul>
                            </li> 
                            <?php
                        } else {
                            ?>
                            <?php
                        }
                        ?>





                        <li class="">
                            <a class="" href="<?php echo base_url(); ?>/Logout">
                                <i class="icon_key_alt"></i>
                                <span>Log Out</span>
                            </a>
                        </li>











                    </ul>
                    <!-- sidebar menu end-->
                </div>
            </aside>
            <!--sidebar end-->



            <!--main content start-->
            <section id="main-content">
                <section class="wrapper" style="margin-top: 10px;"> 


                    <?php
                    foreach ($top_functions as $value) {
                        ?>
                        <a class="quick-btn btn-xs" style="height: 40px; width: 60px;" href='<?php echo base_url() ?><?php echo $value->controller . '/' . $value->function; ?>'>
                            <i class="<?php echo $value->icon_class; ?>"></i>
                            <span><?php echo $value->module; ?></span>
                            <span class="<?php echo $value->span_class; ?>">

                            </span>
                        </a>

                        <?php
                    }
                    ?>


                    <?php
                    $function_name = $this->uri->segment(2);
                    if ($function_name == "index" || $function_name == "" || $function_name == "dashboard_trial" || $function_name == "dashboard" || $function_name == "facility_home") {
                        ?>
                        <div class="row">


                            <div class="form-group filter_div">


                                <?php
                                if ($access_level != "Facility") {
                                    ?>

                                    <div class = "col-lg-2">
                                        <select class = "form-control filter_county  select2" name = "filter_county" id = "filter_county">
                                            <option value = "">Please select County</option>
                                            <?php
                                            foreach ($filtered_county as $value) {
                                                ?>
                                                <option value="<?php echo $value->county_id; ?>"><?php echo $value->county_name; ?></option>
                                                <?php
                                            }
                                            ?>
                                            <option></option>
                                        </select>
                                    </div>
                                    <div class="col-lg-2">
                                        <span class="filter_sub_county_wait" style="display: none;">Loading , Please Wait ...</span>
                                        <select class="form-control filter_sub_county" name="filter_sub_county" id="filter_sub_county">
                                            <option value="">Please Select Sub County : </option>
                                        </select>
                                    </div>
                                    <div class="col-lg-2">
                                        <span class="filter_facility_wait" style="display: none;">Loading , Please Wait ...</span>

                                        <select class="form-control filter_facility" name="filter_facility" id="filter_facility">
                                            <option value="">Please select Facility : </option>
                                        </select>
                                    </div>

                                    <?php
                                } else {
                                    ?>


                                <?php }
                                ?>

                                <div class="col-lg-2">
                                    <input type="text" name="date_from" id="date_from" class="form-controL date_from " placeholder="Date From : "/>
                                </div>

                                <div class="col-lg-2">
                                    <input type="text" name="date_to" id="date_to" class="form-control date_to " placeholder="Date To : "/>
                                </div>
                                <div class="col-lg-2">
                                    <button class="btn btn-default filter_dashboard" name="filter_dashboard" id="filter_dashboard"> <span class="glyphicon glyphicon-filter"></span></button>
                                </div>




                            </div>

                        </div>
                        <?php
                    } else {
                        
                    }
                    ?>


