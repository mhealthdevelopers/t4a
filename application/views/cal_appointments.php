<!--END BLOCK SECTION -->
<hr />
<!-- COMMENT AND NOTIFICATION  SECTION -->
<div class="row" id="data">






    <div class="col-lg-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                ADHERENCE SUMMARY FOR 
            </div>
            <div class="panel-body">


            </div>
        </div>
    </div>


    <div class="col-lg-12">


        <div class="panel panel-primary" id="main_clinician">

            <div class="panel-heading"> 
                Scheduled Appointments in the  System
            </div>   
            <div >

                <div class="panel-body">

                    <table id="table" class="table table-bordered table-condensed table-hover table-responsive table-stripped">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>UPN</th>
                                <th>Client Name</th>
                                <th>Phone No</th>
                                <th>Appointment Date</th>
                                <th>Appointment Type</th>
                                <?php
                                $access_level = $this->session->userdata('access_level');
                                if ($access_level == "Facility") {
                                    ?>

                                    <th>Outgoing Msg</th>
                                    <th>Action</th>
                                    <?php
                                } else {
                                    ?>


                                    <?php
                                }
                                ?>


                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 1;
                            foreach ($appointments as $value) {
                                ?>
                                <tr>
                                    <td class="a-center"><?php echo $i; ?></td>



                                    <?php
                                    $view_client = $this->session->userdata('view_client');

                                    if ($view_client == "Yes") {
                                        ?>
                                        <td>
                                            <input type="hidden" id="client_id" name="client_id" class="client_id form-control" value="<?php echo $value->client_id; ?>"/>
                                            <button class="btn btn-default btn-small edit_btn" id="edit_btn">
                                                <?php echo $value->clinic_number; ?>
                                            </button>

                                        </td>
                                        <td><?php
                                            $client_name = ucwords(strtolower($value->f_name)) . ' ' . ucwords(strtolower($value->m_name)) . ' ' . ucwords(strtolower($value->l_name));

                                            echo $client_name;
                                            ?></td>
                                        <td><?php echo $value->phone_no; ?></td>
                                        <?php
                                    } else {
                                        ?>

                                        <td>XXXXXX XXXXXXX</td>
                                        <td>XXXXXX XXXXXXX</td>
                                        <td>XXXXXX XXXXXXX</td>
                                        <?php
                                    }
                                    ?>
                                    <td><?php echo $value->appntmnt_date; ?></td>
                                    <td><?php echo $value->app_type_1; ?></td>
                                    <?php
                                    $access_level = $this->session->userdata('access_level');
                                    if ($access_level == "Facility") {
                                        ?>

                                        <td><?php echo $value->app_msg; ?></td>
                                        <td>  
                                            <input type="hidden" id="client_id" name="client_id" class="client_id form-control" value="<?php echo $value->client_id; ?>"/>
                                            <input type="hidden" id="app_type_1" name="app_type_1" class="app_type_1 form-control" value="<?php echo $value->app_type_1; ?>"/>
                                            <button class="btn btn-primary btn-small confirm_btn" id="confirm_btn">Confirm</button></td>
                                        <?php
                                    } else {
                                        ?>



                                        <?php
                                    }
                                    ?> </tr>
                                <?php
                                $i++;
                            }
                            ?>
                        </tbody>
                    </table>

                </div>
            </div>                <div class="panel-footer">
                Get   in touch: support.tech@mhealthkenya.org                             </div>

        </div>        












    </div>



</div>
</div>
<!-- END COMMENT AND NOTIFICATION  SECTION -->

</div>








<script type="text/javascript">
    $(document).ready(function () {

        pre_art_summary();
        function pre_art_summary() {
            $.ajax({
                type: "GET",
                async: true,
                url: "<?php echo base_url(); ?>reports/art_attended_summary",
                dataType: "JSON",
                success: function (response) {
                    console.log(response);
                    var male_attended = 0;
                    var female_attended = 0;
                    $.each(response, function (i, value) {
                        var app_status = value.app_status;

                        var male = value.Male1;
                        var female = value.Female1;


                        if (app_status == 'Booked' || app_status == 'Notified') {
                            male_attended += parseInt(male);
                            female_attended += parseInt(female);
                        }

                        console.log(",ale => " + male_attended);
                        console.log("Female  => " + female_attended);

                        $("#confirm_client_id").empty();

                        $("#confirm_status").empty();

                        $("#confirm_f_name").empty();

                        $("#confirm_m_name").empty();

                        $("#confirm_l_name").empty();


                        $("#confirm_mobile").empty();


                        $("#confirm_client_id").val(value.app_status);
                        $('#confirm_clinic_number').val(value.male1);
                        $('#confirm_mobile').val(value.Female1);


                    });





                }, error: function (data) {
                    sweetAlert("", " An error occured ...", "error");

                }

            });


        }




















    });
</script>




<!--END MAIN WRAPPER -->