<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><html lang="en"> 
    <!-- BEGIN HEAD--><head>

        <meta charset="UTF-8" />
        <title>Ushauri-NASCOP</title>
        <!-- <META HTTP-EQUIV="REFRESH" CONTENT="90">   -->  
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!--[if IE]>
           <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
           <![endif]-->
        <!-- GLOBAL STYLES -->
        <!-- GLOBAL STYLES -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/template/assets/plugins/bootstrap/css/bootstrap.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/template/assets/css/main.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/template/assets/css/theme.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/template/assets/css/MoneAdmin.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/template/assets/plugins/Font-Awesome/css/font-awesome.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/style.css" />
        <!--END GLOBAL STYLES -->
        <!--start crud styles 
     <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        -->
        <style type="text/css">
            table tr th, table tr td{font-size: 1.2rem;}
            .row{ margin:20px 20px 20px 20px;width: 100%;}
            .glyphicon{font-size: 20px;}
            .glyphicon-plus{float: right;color:#3399FF}
            a.glyphicon{text-decoration: none;color:#3399FF}
            a.glyphicon-trash{margin-left: 10px;color:#3399FF}
            .none{display: none;}
        </style>

        <!--end crud styles--> 
        <!-- START CHART STYLES    --> 
        <!-- Index Chart    -->   
        <script src="<?php echo base_url(); ?>assets/charts/lib/js/jquery.min.js"></script>
        <!-- END CHART STYLES    -->
        <!-- PAGE LEVEL STYLES -->
        <!-- END PAGE LEVEL  STYLES -->
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script type="text/javascript" src="<?php echo base_url(); ?>assets/date/jquery-1.4.2.min.js"></script>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/date/cupertino/jquery.ui.all.css" type="text/css">
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/date/jquery.ui.core.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/date/jquery.ui.widget.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/date/jquery.ui.datepicker.min.js"></script>
        <style type="text/css">
            .ui-datepicker
            {
                font-family: Arial;
                font-size: 13px;
                z-index: 1003 !important;
                background-color:#A91E22;
                color:#ffffff;
            }
        </style>



        <!-- Bootstrap CSS -->    
        <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
        <!-- bootstrap theme -->
        <link href="<?php echo base_url(); ?>assets/css/bootstrap-theme.css" rel="stylesheet">
        <!--external css-->
        <!-- font icon -->
        <link href="<?php echo base_url(); ?>assets/css/elegant-icons-style.css" rel="stylesheet" />
        <!-- owl carousel -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/owl.carousel.css" type="text/css">

        <!-- Custom styles -->
        <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/style-responsive.css" rel="stylesheet" />


        <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.css" />
        <link rel="stylesheet" href="assets/css/main.css" />
        <link rel="stylesheet" href="assets/css/theme.css" />
        <link rel="stylesheet" href="assets/css/MoneAdmin.css" />
        <link rel="stylesheet" href="assets/plugins/Font-Awesome/css/font-awesome.css" />
        <!--END GLOBAL STYLES -->



        <style type="text/css">
            /* Center the loader */
            #loader {
                position: absolute;
                left: 50%;
                top: 50%;
                z-index: 1;
                width: 150px;
                height: 150px;
                margin: -75px 0 0 -75px;
                border: 16px solid #f3f3f3;
                border-radius: 50%;
                border-top: 16px solid #3498db;
                width: 120px;
                height: 120px;
                -webkit-animation: spin 2s linear infinite;
                animation: spin 2s linear infinite;
            }

            @-webkit-keyframes spin {
                0% { -webkit-transform: rotate(0deg); }
                100% { -webkit-transform: rotate(360deg); }
            }

            @keyframes spin {
                0% { transform: rotate(0deg); }
                100% { transform: rotate(360deg); }
            }

            /* Add animation to "page content" */
            .animate-bottom {
                position: relative;
                -webkit-animation-name: animatebottom;
                -webkit-animation-duration: 1s;
                animation-name: animatebottom;
                animation-duration: 1s
            }

            @-webkit-keyframes animatebottom {
                from { bottom:-100px; opacity:0 } 
                to { bottom:0px; opacity:1 }
            }

            @keyframes animatebottom { 
                from{ bottom:-100px; opacity:0 } 
                to{ bottom:0; opacity:1 }
            }

            #myDiv {
                display: none;
                text-align: center;
            }
        </style>



    </head>
    <!-- END  HEAD-->
    <!-- BEGIN BODY-->
    <body class="padTop53 " >

        <!-- container section start -->
        <section id="container" class="">



            <!--header start-->
            <header class="header white-bg">
                <div class="toggle-nav">
                    <div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom"></div>
                </div>


                <!--logo start-->
                <div class="nav search-row" id="top_menu">


                    <a href="<?php echo base_url(); ?>" class="logo">
                        <img style="max-height: 100%; max-width: 100%;" src="<?php echo base_url(); ?>assets/images/moh.png" alt="" /></a>

                    <?php
                    $donor_id = $this->session->userdata('donor_id');
                    $facility_id = $this->session->userdata('facility_id');
                    $partner_id = $this->session->userdata('partner_id');
                    if (!empty($donor_id)) {
                        $donor_logo = $this->session->userdata('logo');
                        ?>
                        <a href="<?php echo base_url(); ?>" class="logo">
                            <img style="max-height: 100%; max-width: 100%;" src="<?php echo base_url(); ?>uploads/logo/Donor/<?php echo $donor_logo; ?>" alt="" />
                        </a>
                        <?php
                    } elseif (!empty($facility_id)) {
                        $partner_logo = $this->session->userdata('logo');
                        ?>
                        <a href="<?php echo base_url(); ?>" class="logo">
                            <img style="max-height: 100%; max-width: 100%;" src="<?php echo base_url(); ?>uploads/logo/Partner/<?php echo $partner_logo; ?>" alt="" />
                        </a>
                        <?php
                    } elseif (!empty($partner_id)) {
                        $partner_logo = $this->session->userdata('logo');
                        ?>
                        <a href="<?php echo base_url(); ?>" class="logo">
                            <img style="max-height: 100%; max-width: 100%;" src="<?php echo base_url(); ?>uploads/logo/Partner/<?php echo $partner_logo; ?>" alt="" />
                        </a>
                        <?php
                    } else {
                        ?>

                        <?php
                    }
                    ?>



                    <a href="<?php echo base_url(); ?>" class="logo"  >
                        <img style="max-height: 100%; max-width: 100%;"  src="<?php echo base_url(); ?>assets/images/t4a_logo.png" alt="Ushauri" /></a>



                </div>  
                <!--logo end-->

                <div class="nav search-row" id="top_menu">
                    <!--  search form start -->

                    <?php
                    $access_level = $this->session->userdata('access_level');


                    if ($access_level == "Admin") {
                        ?>
                        <span> Access Level :  Administrator </span>
                        <?php
                    } elseif ($access_level == "Donor") {

                        foreach ($output as $value) {
                            $name = $value->name;
                            ?>
                            <span> Access Level :  Donor </span><br/>
                            <span>Donor Name : <?php echo $name; ?> </span>

                            <?php
                        }
                    } elseif ($access_level == "Partner") {

                        foreach ($output as $value) {
                            $name = $value->name;
                            ?>
                            <span> Access Level :  Partner </span><br/>
                            <span>Partner Name : <?php echo $name; ?> </span>

                            <?php
                        }
                    } elseif ($access_level == "County") {

                        foreach ($output as $value) {
                            $name = $value->name;
                            ?>
                            <span> Access Level :  County </span><br/>
                            <span>County Name : <?php echo $name; ?> </span>

                            <?php
                        }
                    } elseif ($access_level == "Sub County") {

                        foreach ($output as $value) {
                            $name = $value->name;
                            ?>
                            <span> Access Level :  Sub County </span><br/>
                            <span>Sub County Name : <?php echo $name; ?> </span>

                            <?php
                        }
                    } elseif ($access_level == "Facility") {

                        foreach ($output as $value) {
                            $name = $value->name;
                            $mfl_code = $value->code;
                            ?>
                            <span> Access Level :  Facility </span><br/>
                            <span>Facility Name : <?php echo $name; ?> </span> :    
                            <span>MFL Code : <?php echo $mfl_code; ?></span>
                            <?php
                        }
                    }
                    ?>
                    <li id="clock"></li>
                    <!--  search form end -->
                </div>
                <div class="top-nav notification-row">
                    <!-- notificatoin dropdown start-->
                    <ul class="nav pull-right top-menu">



                        <!-- user login dropdown start-->
                        <li class="dropdown">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <span class="profile-ava">
                                    <img alt="" src="<?php echo base_url(); ?>assets/img/avatar1_small.jpg">
                                </span>
                                <span class="username"><?php echo $this->session->userdata('Fullname'); ?></span>
                                <b class="caret"></b>
                            </a>


                            <ul class="dropdown-menu extended logout">
                                <div class="log-arrow-up"></div>
                                <li class="eborder-top">
                                    <a href="<?php echo base_url(); ?>"><i class="icon_profile"></i> My Profile</a>
                                </li>


                                <li>
                                    <a href="<?php echo base_url(); ?>/Logout"><i class="icon_key_alt"></i> Log Out</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>"><i class="icon_book"></i> Documentation</a>
                                </li>

                            </ul>
                        </li>
                        <!-- user login dropdown end -->
                    </ul>
                    <!-- notificatoin dropdown end-->
                </div>
            </header>      
            <!--header end-->


            <!--sidebar start-->
            <aside style="padding-top: 60px;" >
                <div id="sidebar"  class="nav-collapse ">
                    <!-- sidebar menu start-->
                    <ul class="sidebar-menu">                
                        <li class="">
                            <a class="" href="<?php echo base_url(); ?>">
                                <i class="icon_house_alt"></i>
                                <span>Dashboard</span>
                            </a>
                        </li>




                        <?php
                        $array = $side_functions;
                        $number = array_column($array, 'level');
                        $found_key = array_search('2', $number);

                        $level2 = '2';
                        $newArray = array_filter($array, function ($var) use ($level2) {
                            return ($var['level'] == $level2);
                        });



                        $names = array_column($newArray, 'id');
                        $level2 = array_filter($names);

                        if (!empty($level2)) {
                            ?>
                            <li class="sub-menu">
                                <a href="javascript:;" class="">
                                    <i class="icon_desktop"></i>
                                    <span>Clients</span>
                                    <span class="menu-arrow arrow_carrot-right"></span>
                                </a>
                                <ul class="sub">
                                    <?php
                                    foreach ($side_functions as $value) {
                                        $level2 = $value['level'];
                                        if ($level2 == 2) {
                                            ?>

                                            <li>
                                                <a class="" href='<?php echo base_url() ?><?php echo $value['controller'] . '/' . $value['function']; ?>'>
                                                    <i class="<?php echo $value['icon_class']; ?>"></i>
                                                    <span>
                                                        <?php echo $value['module']; ?>  
                                                    </span>
                                                    <span class="<?php echo $value['span_class']; ?>"></span>
                                                </a>
                                            </li>

                                            <?php
                                        }
                                    }
                                    ?>
                                </ul>
                            </li> 
                            <?php
                        } else {
                            ?>
                            <?php
                        }
                        ?>

                        <?php
                        $array = $side_functions;
                        $number = array_column($array, 'level');
                        $found_key = array_search('2', $number);

                        $level3 = '3';
                        $newArray = array_filter($array, function ($var) use ($level3) {
                            return ($var['level'] == $level3);
                        });



                        $names = array_column($newArray, 'id');
                        $level3 = array_filter($names);

                        if (!empty($level3)) {
                            ?>







                            <li class="sub-menu">
                                <a href="javascript:;" class="">
                                    <i class="icon_desktop"></i>
                                    <span>Appointments</span>
                                    <span class="menu-arrow arrow_carrot-right"></span>
                                </a>
                                <ul class="sub">







                                    <li class="sub-menu">
                                        <a href="javascript:;" class="">

                                            <span>Appointments Diary </span>
                                            <span class="menu-arrow arrow_carrot-right"></span>
                                        </a>
                                        <ul class="sub" style="display:none;">
                                            <?php
                                            foreach ($side_functions as $value) {
                                                $level3 = $value['level'];
                                                if ($level3 == 3) {
                                                    ?>
                                                    <?php
                                                    $function = $value['function'];

                                                    if ($function == 'today_appointments' or $function == 'notified_clients' or $function == 'create_appointment') {
                                                        ?>
                                                        <li>
                                                            <a class="" href='<?php echo base_url() ?><?php echo $value['controller'] . '/' . $value['function']; ?>'>
                                                                <i class="<?php echo $value['icon_class']; ?>"></i>
                                                                <span>
                                                                    <?php echo $value['module']; ?>  
                                                                </span>
                                                                <span class="<?php echo $value['span_class']; ?>"></span>
                                                            </a>
                                                        </li>
                                                        <?php
                                                    }
                                                    ?>

                                                    <?php
                                                }
                                            }
                                            ?>


                                        </ul></li>

                                    <li class="sub-menu">
                                        <a href="javascript:;" class="">

                                            <span>Defaulters Diary</span>
                                            <span class="menu-arrow arrow_carrot-right"></span>
                                        </a>
                                        <ul class="sub" style="display:none;">
                                            <?php
                                            foreach ($side_functions as $value) {
                                                $level3 = $value['level'];
                                                if ($level3 == 3) {
                                                    ?>
                                                    <?php
                                                    $function = $value['function'];

                                                    if ($function == 'missed_clients' or $function == 'defaulted_clients' or $function == 'ltfu_clients') {
                                                        ?>
                                                        <li>
                                                            <a class="" href='<?php echo base_url() ?><?php echo $value['controller'] . '/' . $value['function']; ?>'>
                                                                <i class="<?php echo $value['icon_class']; ?>"></i>
                                                                <span>
                                                                    <?php echo $value['module']; ?>  
                                                                </span>
                                                                <span class="<?php echo $value['span_class']; ?>"></span>
                                                            </a>
                                                        </li>
                                                        <?php
                                                    }
                                                    ?>

                                                    <?php
                                                }
                                            }
                                            ?>


                                        </ul></li>


                                    <?php
                                    foreach ($side_functions as $value) {
                                        $level3 = $value['level'];
                                        if ($level3 == 3) {
                                            ?>
                                            <?php
                                            $function = $value['function'];

                                            if ($function == 'show_calendar' or $function == 'appointment_diary' or $function == 'appointments') {
                                                ?>
                                                <li>
                                                    <a class="" href='<?php echo base_url() ?><?php echo $value['controller'] . '/' . $value['function']; ?>'>
                                                        <i class="<?php echo $value['icon_class']; ?>"></i>
                                                        <span>
                                                            <?php echo $value['module']; ?>  
                                                        </span>
                                                        <span class="<?php echo $value['span_class']; ?>"></span>
                                                    </a>
                                                </li>
                                                <?php
                                            }
                                            ?>

                                            <?php
                                        }
                                    }
                                    ?>






                                </ul>
                            </li> 
                            <?php
                        } else {
                            ?>
                            <?php
                        }
                        ?>

                        <?php
                        $array = $side_functions;
                        $number = array_column($array, 'level');
                        $found_key = array_search('2', $number);

                        $level4 = '4';
                        $newArray = array_filter($array, function ($var) use ($level4) {
                            return ($var['level'] == $level4);
                        });



                        $names = array_column($newArray, 'id');
                        $level4 = array_filter($names);

                        if (!empty($level4)) {
                            ?>
                            <li class="sub-menu">
                                <a href="javascript:;" class="">
                                    <i class="icon_desktop"></i>
                                    <span>Groups</span>
                                    <span class="menu-arrow arrow_carrot-right"></span>
                                </a>
                                <ul class="sub">
                                    <?php
                                    foreach ($side_functions as $value) {
                                        $level4 = $value['level'];
                                        if ($level4 == 4) {
                                            ?>

                                            <li>
                                                <a class="" href='<?php echo base_url() ?><?php echo $value['controller'] . '/' . $value['function']; ?>'>
                                                    <i class="<?php echo $value['icon_class']; ?>"></i>
                                                    <span>
                                                        <?php echo $value['module']; ?>  
                                                    </span>
                                                    <span class="<?php echo $value['span_class']; ?>"></span>
                                                </a>
                                            </li>

                                            <?php
                                        }
                                    }
                                    ?>
                                </ul>
                            </li> 
                            <?php
                        } else {
                            ?>
                            <?php
                        }
                        ?>

                        <?php
                        $array = $side_functions;
                        $number = array_column($array, 'level');
                        $found_key = array_search('2', $number);

                        $level5 = '5';
                        $newArray = array_filter($array, function ($var) use ($level5) {
                            return ($var['level'] == $level5);
                        });



                        $names = array_column($newArray, 'id');
                        $level5 = array_filter($names);

                        if (!empty($level5)) {
                            ?>
                            <li class="sub-menu">
                                <a href="javascript:;" class="">
                                    <i class="icon_desktop"></i>
                                    <span>Group</span>
                                    <span class="menu-arrow arrow_carrot-right"></span>
                                </a>
                                <ul class="sub">
                                    <?php
                                    foreach ($side_functions as $value) {
                                        $level5 = $value['level'];
                                        if ($level5 == 5) {
                                            ?>

                                            <li>
                                                <a class="" href='<?php echo base_url() ?><?php echo $value['controller'] . '/' . $value['function']; ?>'>
                                                    <i class="<?php echo $value['icon_class']; ?>"></i>
                                                    <span>
                                                        <?php echo $value['module']; ?>  
                                                    </span>
                                                    <span class="<?php echo $value['span_class']; ?>"></span>
                                                </a>
                                            </li>

                                            <?php
                                        }
                                    }
                                    ?>
                                </ul>
                            </li> 
                            <?php
                        } else {
                            ?>
                            <?php
                        }
                        ?>

                        <?php
                        $array = $side_functions;
                        $number = array_column($array, 'level');
                        $found_key = array_search('2', $number);

                        $level6 = '6';
                        $newArray = array_filter($array, function ($var) use ($level6) {
                            return ($var['level'] == $level6);
                        });



                        $names = array_column($newArray, 'id');
                        $level6 = array_filter($names);

                        if (!empty($level6)) {
                            ?>
                            <li class="sub-menu">
                                <a href="javascript:;" class="">
                                    <i class="icon_desktop"></i>
                                    <span>Administration</span>
                                    <span class="menu-arrow arrow_carrot-right"></span>
                                </a>
                                <ul class="sub">
                                    <?php
                                    foreach ($side_functions as $value) {
                                        $level6 = $value['level'];
                                        if ($level6 == 6) {
                                            ?>

                                            <li>
                                                <a class="" href='<?php echo base_url() ?><?php echo $value['controller'] . '/' . $value['function']; ?>'>
                                                    <i class="<?php echo $value['icon_class']; ?>"></i>
                                                    <span>
                                                        <?php echo $value['module']; ?>  
                                                    </span>
                                                    <span class="<?php echo $value['span_class']; ?>"></span>
                                                </a>
                                            </li>

                                            <?php
                                        }
                                    }
                                    ?>
                                </ul>
                            </li> 
                            <?php
                        } else {
                            ?>
                            <?php
                        }
                        ?>

                        <?php
                        $array = $side_functions;


                        $level7 = '7';
                        $newArray = array_filter($array, function ($var) use ($level7) {
                            return ($var['level'] == $level7);
                        });



                        $names = array_column($newArray, 'id');
                        $level7 = array_filter($names);

                        if (!empty($level7)) {
                            ?>
                            <li class="sub-menu">
                                <a href="javascript:;" class="">
                                    <i class="icon_desktop"></i>
                                    <span>Reports</span>
                                    <span class="menu-arrow arrow_carrot-right"></span>
                                </a>
                                <ul class="sub">
                                    <?php
                                    foreach ($side_functions as $value) {
                                        $level7 = $value['level'];
                                        if ($level7 == 7) {
                                            ?>

                                            <li>
                                                <a class="" href='<?php echo base_url() ?><?php echo $value['controller'] . '/' . $value['function']; ?>'>
                                                    <i class="<?php echo $value['icon_class']; ?>"></i>
                                                    <span>
                                                        <?php echo $value['module']; ?>  
                                                    </span>
                                                    <span class="<?php echo $value['span_class']; ?>"></span>
                                                </a>
                                            </li>

                                            <?php
                                        }
                                    }
                                    ?>
                                </ul>
                            </li> 
                            <?php
                        } else {
                            ?>
                            <?php
                        }
                        ?>
                        <?php
                        $array = $side_functions;


                        $level8 = '8';
                        $newArray = array_filter($array, function ($var) use ($level8) {
                            return ($var['level'] == $level8);
                        });



                        $names = array_column($newArray, 'id');
                        $level8 = array_filter($names);

                        if (!empty($level8)) {
                            ?>
                            <li class="sub-menu">
                                <a href="javascript:;" class="">
                                    <i class="icon_desktop"></i>
                                    <span>Support</span>
                                    <span class="menu-arrow arrow_carrot-right"></span>
                                </a>
                                <ul class="sub">
                                    <?php
                                    foreach ($side_functions as $value) {
                                        $level8 = $value['level'];
                                        if ($level8 == 8) {
                                            ?>

                                            <li>
                                                <a class="" href='<?php echo base_url() ?><?php echo $value['controller'] . '/' . $value['function']; ?>'>
                                                    <i class="<?php echo $value['icon_class']; ?>"></i>
                                                    <span>
                                                        <?php echo $value['module']; ?>  
                                                    </span>
                                                    <span class="<?php echo $value['span_class']; ?>"></span>
                                                </a>
                                            </li>

                                            <?php
                                        }
                                    }
                                    ?>
                                </ul>
                            </li> 
                            <?php
                        } else {
                            ?>
                            <?php
                        }
                        ?>





                        <li class="">
                            <a class="" href="<?php echo base_url(); ?>/Logout">
                                <i class="icon_key_alt"></i>
                                <span>Log Out</span>
                            </a>
                        </li>











                    </ul>
                    <!-- sidebar menu end-->
                </div>
            </aside>
            <!--sidebar end-->



            <!--main content start-->
            <section id="main-content">
                <section class="wrapper" style="margin-top: 10px;"> 


                    <?php
                    foreach ($top_functions as $value) {
                        ?>
                        <a class="quick-btn btn-xs" style="height: 40px; width: 60px;" href='<?php echo base_url() ?><?php echo $value->controller . '/' . $value->function; ?>'>
                            <i class="<?php echo $value->icon_class; ?>"></i>
                            <span><?php echo $value->module; ?></span>
                            <span class="<?php echo $value->span_class; ?>">

                            </span>
                        </a>

                        <?php
                    }
                    ?>


                    <?php
                    $function_name = $this->uri->segment(2);
                    if ($function_name == "index" || $function_name == "" || $function_name == "dashboard_trial" || $function_name == "dashboard" || $function_name == "facility_home") {
                        ?>

                        <div class="form-group">
                            <div class="col-lg-12">
                                <div class="row">
                                    <?php
                                    if ($access_level != "Facility") {
                                        ?>

                                        <div class = "col-lg-2">
                                            <select class = "form-control filter_county" name = "filter_county" id = "filter_county">
                                                <option value = "">Please select County</option>
                                                <?php
                                                foreach ($filtered_county as $value) {
                                                    ?>
                                                    <option value="<?php echo $value->county_id; ?>"><?php echo $value->county_name; ?></option>
                                                    <?php
                                                }
                                                ?>
                                                <option></option>
                                            </select>
                                        </div>
                                        <div class="col-lg-2">
                                            <span class="filter_sub_county_wait" style="display: none;">Loading , Please Wait ...</span>
                                            <select class="form-control filter_sub_county" name="filter_sub_county" id="filter_sub_county">
                                                <option value="">Please Select Sub County : </option>
                                            </select>
                                        </div>
                                        <div class="col-lg-2">
                                            <span class="filter_facility_wait" style="display: none;">Loading , Please Wait ...</span>

                                            <select class="form-control filter_facility" name="filter_facility" id="filter_facility">
                                                <option value="">Please select Facility : </option>
                                            </select>
                                        </div>

                                        <?php
                                    } else {
                                        ?>


                                    <?php }
                                    ?>

                                    <div class="col-lg-2">
                                        <input type="text" name="date_from" id="date_from" class="form-controL date_from " placeholder="Date From : "/>
                                    </div>

                                    <div class="col-lg-2">
                                        <input type="text" name="date_to" id="date_to" class="form-control date_to " placeholder="Date To : "/>
                                    </div>
                                    <div class="col-lg-2">
                                        <button class="btn btn-default filter_dashboard" name="filter_dashboard" id="filter_dashboard"> <span class="glyphicon glyphicon-filter"></span></button>
                                    </div>

                                </div>

                            </div>
                        </div>
                        <?php
                    } else {
                        
                    }
                    ?>


                    <!--END BLOCK SECTION -->

                    <!-- COMMENT AND NOTIFICATION  SECTION -->
                    <div class="row" id="data">


                        <div class="state col-lg-12">
                            <!-- Trigger the modal with a button -->
                            <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>

                            <!-- Modal -->
                            <div id="myModal" class="modal fade" role="dialog">
                                <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Modal Header</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p>Some text in the modal.</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>

                                </div>
                            </div>


                        </div>







                    </div>



                    <input type="text" name="user_county_id" value="<?php echo $this->session->userdata('county_id'); ?>" class="form-control user_county_id" id="user_county_id"/>




                    <script type="text/javascript">
                        $(document).ready(function () {







                            var user_access_level = $(".user_access_level").val();

                            if (user_access_level == "Admin") {

                                //            $(".add_access_level option[value='Facility']").remove();
                                //            $(".edit_access_level option[value='Facility']").remove();

                            } else if (user_access_level == "Partner") {
                                $(".add_access_level option[value='Admin']").remove();
                                $(".add_access_level option[value='Donor']").remove();
                                $(".edit_access_level option[value='Admin']").remove();
                                $(".edit_access_level option[value='Donor']").remove();

                            } else if (user_access_level == "Facility") {
                                $(".add_access_level option[value='Admin']").remove();
                                $(".add_access_level option[value='Donor']").remove();
                                $(".add_access_level option[value='Partner']").remove();
                                $(".add_access_level option[value='County']").remove();
                                $(".add_access_level option[value='Sub County']").remove();
                                $(".add_access_level option[value='Partner']").remove();
                                $(".edit_access_level option[value='Admin']").remove();
                                $(".edit_access_level option[value='Donor']").remove();
                                $(".edit_access_level option[value='Partner']").remove();
                                $(".edit_access_level option[value='County']").remove();
                                $(".edit_access_level option[value='Sub County']").remove();
                                $(".edit_access_level option[value='Partner']").remove();
                            } else if (user_access_level == "Donor") {
                                $(".add_access_level option[value='Admin']").remove();
                                $(".add_access_level option[value='Facility']").remove();
                                $(".add_access_level option[value='Partner']").remove();
                                $(".add_access_level option[value='County']").remove();
                                $(".add_access_level option[value='Sub County']").remove();
                                $(".add_access_level option[value='Partner']").remove();
                                $(".edit_access_level option[value='Admin']").remove();
                                $(".edit_access_level option[value='Facility']").remove();
                                $(".edit_access_level option[value='Partner']").remove();
                                $(".edit_access_level option[value='County']").remove();
                                $(".edit_access_level option[value='Sub County']").remove();
                                $(".edit_access_level option[value='Partner']").remove();


                            } else if (user_access_level == "County") {
                                $(".add_access_level option[value='Admin']").remove();
                                $(".add_access_level option[value='Donor']").remove();
                                $(".add_access_level option[value='Partner']").remove();
                                $(".edit_access_level option[value='Admin']").remove();
                                $(".edit_access_level option[value='Donor']").remove();
                                $(".edit_access_level option[value='Partner']").remove();

                                get_county_facilities();


                            } else {
                                $(".add_access_level option[value='Admin']").remove();
                                $(".add_access_level option[value='Facility']").remove();
                                $(".add_access_level option[value='Partner']").remove();
                                $(".add_access_level option[value='Donor']").remove();
                                $(".add_access_level option[value='County']").remove();
                                $(".add_access_level option[value='Sub County']").remove();




                                $(".edit_access_level option[value='Admin']").remove();
                                $(".edit_access_level option[value='Facility']").remove();
                                $(".edit_access_level option[value='Partner']").remove();
                                $(".edit_access_level option[value='Donor']").remove();
                                $(".edit_access_level option[value='County']").remove();
                                $(".edit_access_level option[value='Sub County']").remove();

                            }





                            $("#add_access_level").change(function () {
                                $(".add_loading_option").show();
                                $(".add_dynamic_options").hide();
                                var access_level = this.value;
                                var plse_select = "<option value=''>Please Select : </option>";
                                $.ajax({
                                    url: "<?php echo base_url() ?>admin/get_access_roles/" + access_level + "",
                                    type: 'GET',
                                    dataType: 'JSON',
                                    success: function (data) {
                                        $(".add_loading_option").hide();
                                        $(".add_dynamic_options").show();
                                        $(".add_role_names").empty();

                                        $(".add_role_names").append(plse_select);
                                        $.each(data, function (i, key) {
                                            var option = "<option value=" + key.id + ">" + key.name + "</option>";
                                            $(".add_role_names").append(option);
                                        });
                                    }, error: function (errorThrown) {

                                    }
                                });
                            });


                            function get_county_facilities() {



                                $(".add_loading_option").show();
                                $(".add_dynamic_options").hide();
                                var county_id = $(".user_county_id").val();

                                var plse_select = "<option value=''>Please Select : </option>";
                                $.ajax({
                                    url: "<?php echo base_url() ?>admin/get_county_facilities/" + county_id + "",
                                    type: 'GET',
                                    dataType: 'JSON',
                                    success: function (data) {

                                        $(".add_loading_option").hide();
                                        //$(".add_dynamic_options").show();
                                        $(".facility_id").empty();
                                        //                    $("#edit_facility_id").empty();

                                        $(".facility_id").append(plse_select);
                                        $.each(data, function (i, key) {
                                            var option = "<option value=" + key.id + ">" + key.name + "</option>";
                                            $(".facility_id").append(option);
                                        });
                                    }, error: function (errorThrown) {

                                    }
                                });













                            }




                            $(".add_county_id").change(function () {
                                $(".add_loading_option").show();
                                $(".add_dynamic_options").hide();
                                var county_id = this.value;
                                var tokenizer = $(".tokenizer").val();
                                var plse_select = "<option value=''>Please Select : </option>";
                                $.ajax({
                                    url: "<?php echo base_url() ?>admin/get_sub_counties/" + county_id + "",
                                    type: 'POST',
                                    dataType: 'JSON',
                                    data: {tokenizer: tokenizer},

                                    success: function (data) {
                                        $(".add_loading_option").hide();
                                        $(".add_dynamic_options").show();
                                        $(".add_subcounty_id").empty();

                                        $(".add_subcounty_id").append(plse_select);
                                        $.each(data, function (i, key) {
                                            var option = "<option value=" + key.id + ">" + key.name + "</option>";
                                            $(".add_subcounty_id").append(option);
                                        });
                                    }, error: function (errorThrown) {

                                    }
                                });
                            });
                            $(".edit_county_id").change(function () {
                                $(".edit_loading_option").show();
                                $(".edit_dynamic_options").hide();
                                var county_id = this.value;
                                var tokenizer = $(".tokenizer").val();
                                var plse_select = "<option value=''>Please Select : </option>";
                                $.ajax({
                                    url: "<?php echo base_url() ?>admin/get_sub_counties/" + county_id + "",
                                    type: 'POST',
                                    dataType: 'JSON', data: {tokenizer: tokenizer},
                                    success: function (data) {
                                        $(".edit_loading_option").hide();
                                        $(".edit_dynamic_options").show();
                                        $(".edit_subcounty_id").empty();

                                        $(".edit_subcounty_id").append(plse_select);
                                        $.each(data, function (i, key) {
                                            var option = "<option value=" + key.id + ">" + key.name + "</option>";
                                            $(".edit_subcounty_id").append(option);
                                        });
                                    }, error: function (errorThrown) {

                                    }
                                });
                            });




                            $("#edit_access_level").change(function () {
                                $(".edit_loading_option").show();
                                $(".edit_dynamic_options").hide();
                                var access_level = this.value;
                                var plse_select = "<option value=''>Please Select : </option>";
                                $.ajax({
                                    url: "<?php echo base_url() ?>admin/get_access_roles/" + access_level + "",
                                    type: 'GET',
                                    dataType: 'JSON',
                                    success: function (data) {
                                        $(".edit_loading_option").hide();
                                        $(".edit_dynamic_options").show();
                                        $(".edit_role_names").empty();

                                        $(".edit_role_names").append(plse_select);
                                        $.each(data, function (i, key) {
                                            var option = "<option value=" + key.id + ">" + key.name + "</option>";
                                            $(".edit_role_names").append(option);
                                        });
                                    }, error: function (errorThrown) {

                                    }
                                });
                            });





                            $("#edit_access_level").change(function () {

                                var access_level = this.value;
                                if (access_level == "Admin") {

                                    $('select#edit_facility_id option').removeAttr("selected");
                                    $("#edit_donor_div").show();
                                    $("#edit_partner_div").hide();
                                    $("#edit_facility_div").hide();
                                } else if (access_level == "Partner") {

                                    $('select#edit_donor_id option').removeAttr("selected");
                                    $("#edit_donor_div").hide();
                                    $("#edit_partner_div").show();
                                    $("#edit_facility_div").hide();
                                } else if (access_level == "Facility") {
                                    $('select#edit_partner_id option').removeAttr("selected");
                                    $('select#edit_donor_id option').removeAttr("selected");
                                    $("#edit_donor_div").hide();
                                    $("#edit_partner_div").hide();
                                    $("#edit_facility_div").show();
                                } else if (access_level == "Donor") {

                                    $('select#edit_partner_id option').removeAttr("selected");
                                    $('select#edit_facility_id option').removeAttr("selected");
                                    $("#edit_donor_div").show();
                                    $("#edit_partner_div").hide();
                                    $("#edit_facility_div").hide();
                                }
                                $("#edit_dynamic_options").show();



                            });




                            $("#add_access_level").change(function () {
                                var access_level = this.value;

                                if (access_level == "Admin") {
                                    $('select#partner_id option').removeAttr("selected");
                                    $('select#facility_id option').removeAttr("selected");


                                    $("#add_donor_div").show();
                                    $("#add_partner_div").hide();
                                    $("#add_facility_div").hide();
                                    $("#add_subcounty_div").hide();
                                    $("#add_county_div").hide();
                                } else if (access_level == "Partner") {
                                    $('select#donor_id option').removeAttr("selected");
                                    $('select#facility_id option').removeAttr("selected");

                                    $("#add_donor_div").hide();
                                    $("#add_partner_div").show();
                                    $("#add_facility_div").hide();
                                    $("#add_subcounty_div").hide();
                                    $("#add_county_div").hide();
                                } else if (access_level == "Facility") {
                                    $('select#partner_id option').removeAttr("selected");
                                    $('select#donor_id option').removeAttr("selected");

                                    $("#add_donor_div").hide();
                                    $("#add_partner_div").hide();
                                    $("#add_facility_div").show();
                                    $("#add_subcounty_div").hide();
                                    $("#add_county_div").hide();
                                } else if (access_level == "Donor") {
                                    $('select#partner_id option').removeAttr("selected");
                                    $('select#facility_id option').removeAttr("selected");

                                    $("#add_donor_div").show();
                                    $("#add_partner_div").hide();
                                    $("#add_facility_div").hide();
                                    $("#add_subcounty_div").hide();
                                    $("#add_county_div").hide();


                                } else if (access_level == "County") {
                                    $('select#partner_id option').removeAttr("selected");
                                    $('select#facility_id option').removeAttr("selected");
                                    $('select#donor_id option').removeAttr("selected");
                                    $("#add_subcounty_div").hide();
                                    $("#add_county_div").show();
                                    $("#add_partner_div").hide();
                                    $("#add_donor_div").hide();
                                    $("#add_facility_div").hide();

                                } else if (access_level == "Sub County") {
                                    $('select#partner_id option').removeAttr("selected");
                                    $('select#facility_id option').removeAttr("selected");
                                    $('select#donor_id option').removeAttr("selected");
                                    $('select#county_id option').removeAttr("selected");

                                    $("#add_county_div").show();
                                    $("#add_subcounty_div").show();

                                    $("#add_partner_div").hide();
                                    $("#add_donor_div").hide();
                                    $("#add_facility_div").hide();
                                }
                                //$("#add_dynamic_options").show();



                            });









                            $(document).on('click', ".add_btn", function () {
                                $(".name").empty();


                                $(".f_name").empty();

                                $(".m_name").empty();

                                $(".l_name").empty();

                                $(".dob").empty();

                                $(".phone_no").empty();

                                $(".e_mail").empty();



                                $(".add_div").show();
                                $(".table_div").hide();


                            });


                            function get_access_level(access_level) {
                                $(".edit_loading_option").show();
                                $(".edit_dynamic_options").hide();

                                var plse_select = "<option value=''>Please Select : </option>";
                                $.ajax({
                                    url: "<?php echo base_url() ?>admin/get_access_roles/" + access_level + "",
                                    type: 'GET',
                                    dataType: 'JSON',
                                    success: function (data) {
                                        $(".edit_loading_option").hide();
                                        $(".edit_dynamic_options").show();
                                        $(".edit_role_names").empty();

                                        $(".edit_role_names").append(plse_select);
                                        $.each(data, function (i, key) {
                                            var option = "<option value=" + key.id + ">" + key.name + "</option>";
                                            $(".edit_role_names").append(option);
                                        });
                                    }, error: function (errorThrown) {

                                    }
                                });
                            }

                            $(document).on('click', ".edit_btn", function () {
                                $(".loader").show();

                                //get data
                                var data_id = $(this).closest('tr').find('input[name="id"]').val();
                                var error_alert = "An Error Ocurred";



                                $.ajax({
                                    type: "GET",
                                    async: true,
                                    url: "<?php echo base_url(); ?>admin/get_user_data/" + data_id,
                                    dataType: "JSON",
                                    success: function (response) {
                                        $(".loader").hide();
                                        $.each(response, function (i, value) {
                                            $("#edit_user_id").empty();



                                            $("#edit_f_name").empty();

                                            $("#edit_m_name").empty();

                                            $("#edit_l_name").empty();

                                            $("#edit_dob").empty();

                                            $("#edit_phone_no").empty();

                                            $("#edit_e_mail").empty();


                                            $('#edit_user_id').val(value.id);
                                            $('#edit_status option[value=' + value.status + ']').attr("selected", "selected");
                                            // $('#edit_access_level option[value=' + value.access_level + ']').attr("selected", "selected");
                                            $('#edit_access_level option[value="' + value.access_level + '"]').attr("selected", "selected");
                                            $('#edit_donor_id option[value=' + value.donor_id + ']').attr("selected", "selected");
                                            $('#edit_partner_id option[value=' + value.partner_id + ']').attr("selected", "selected");
                                            $('#edit_facility_id option[value=' + value.facility_id + ']').attr("selected", "selected");
                                            $('#edit_bio_data option[value=' + value.view_client + ']').attr("selected", "selected");
                                            $('#edit_rcv_app_list option[value=' + value.rcv_app_list + ']').attr("selected", "selected");
                                            $('#edit_f_name').val(value.f_name);
                                            $('#edit_m_name').val(value.m_name);
                                            $('#edit_l_name').val(value.l_name);
                                            $('#edit_phone_no').val(value.phone_no);
                                            $('#edit_dob').val(value.dob);
                                            $('#edit_e_mail').val(value.e_mail);
                                            $('#edit_created_at').val(value.created_at);
                                            $('#edit_timestamp').val(value.timestamp);
                                            $('#edit_daily_report option[value=' + value.daily_report + ']').attr("selected", "selected");
                                            $('#edit_weekly_report option[value=' + value.weekly_report + ']').attr("selected", "selected");
                                            $('#edit_monthly_report option[value=' + value.monthly_report + ']').attr("selected", "selected");
                                            $('#edit_month3_report option[value=' + value.month3_report + ']').attr("selected", "selected");
                                            $('#edit_month6_report option[value=' + value.month6_report + ']').attr("selected", "selected");
                                            $('#edit_yearly_report option[value=' + value.Yearly_report + ']').attr("selected", "selected");



                                            var access_level = value.access_level;
                                            console.log("Access level => " + access_level);

                                            if (access_level == "Admin") {

                                                $('select#edit_facility_id option').removeAttr("selected");
                                                $("#edit_donor_div").show();
                                                $("#edit_partner_div").hide();
                                                $("#edit_facility_div").hide();
                                                $("#edit_county_div").hide();
                                                $("#edit_subcounty_div").hide();
                                                get_access_level(access_level);
                                            } else if (access_level == "Partner") {

                                                $('select#edit_donor_id option').removeAttr("selected");
                                                $("#edit_donor_div").hide();
                                                $("#edit_partner_div").show();
                                                $("#edit_facility_div").hide();
                                                $("#edit_county_div").hide();
                                                $("#edit_subcounty_div").hide();
                                                get_access_level(access_level);
                                            } else if (access_level == "Facility") {
                                                $('select#edit_partner_id option').removeAttr("selected");
                                                $('select#edit_donor_id option').removeAttr("selected");
                                                $("#edit_donor_div").hide();
                                                $("#edit_partner_div").hide();
                                                $("#edit_facility_div").show();
                                                $("#edit_county_div").hide();
                                                $("#edit_subcounty_div").hide();
                                                get_access_level(access_level);
                                            } else if (access_level == "Donor") {

                                                $('select#edit_partner_id option').removeAttr("selected");
                                                $('select#edit_facility_id option').removeAttr("selected");
                                                $("#edit_donor_div").show();
                                                $("#edit_partner_div").hide();
                                                $("#edit_facility_div").hide();
                                                $("#edit_county_div").hide();
                                                $("#edit_subcounty_div").hide();
                                                get_access_level(access_level);

                                            } else if (access_level == "County") {

                                                $('select#edit_partner_id option').removeAttr("selected");
                                                $('select#edit_donor_id option').removeAttr("selected");
                                                $("#edit_donor_div").hide();
                                                $("#edit_partner_div").hide();
                                                $("#edit_facility_div").hide();
                                                $("#edit_county_div").show();
                                                $("#edit_subcounty_div").hide();
                                                get_access_level(access_level);

                                            } else if (access_level == "Sub County") {

                                                $('select#edit_partner_id option').removeAttr("selected");
                                                $('select#edit_donor_id option').removeAttr("selected");
                                                $("#edit_donor_div").hide();
                                                $("#edit_partner_div").hide();
                                                $("#edit_facility_div").hide();
                                                $("#edit_county_div").show();
                                                $("#edit_subcounty_div").show();
                                                get_access_level(access_level);
                                            }
                                            $("#edit_dynamic_options").show();






                                        });


                                        $(".edit_div").show();
                                        $(".table_div").hide();














                                    }, error: function (data) {
                                        $(".loader").hide();
                                        sweetAlert("Oops...", "" + error_alert + "", "error");

                                    }

                                });









                            });



                            $(document).on('click', ".delete_btn", function () {

                                $(".loader").show();
                                //get data
                                var data_id = $(this).closest('tr').find('input[name="id"]').val();
                                var error_alert = "An Error Ocurred";


                                $.ajax({
                                    type: "GET",
                                    async: true,
                                    url: "<?php echo base_url(); ?>admin/get_user_data/" + data_id,
                                    dataType: "JSON",
                                    success: function (response) {
                                        $(".loader").hide();
                                        $.each(response, function (i, value) {
                                            $("#delete_user_id").empty();

                                            $('#delete_user_id').val(value.id);
                                            $('#delete_f_name').val(value.f_name);
                                            $('#delete_m_name').val(value.m_name);
                                            $('#delete_l_name').val(value.l_name);

                                            var delete_descripton = "Do you want to delete User : " + value.f_name + " " + value.m_name + " " + value.l_name + "";
                                            $(".delete_description").append(delete_descripton);
                                        });


                                        $(".delete_div").show();
                                        $(".table_div").hide();


                                    }, error: function (data) {
                                        $(".loader").hide();
                                        sweetAlert("Oops...", "" + error_alert + "", "error");

                                    }

                                });









                            });

                            $(document).on('click', ".reset_btn", function () {

                                $(".loader").show();
                                //get data
                                var data_id = $(this).closest('tr').find('input[name="id"]').val();
                                var error_alert = "An Error Ocurred";


                                $.ajax({
                                    type: "GET",
                                    async: true,
                                    url: "<?php echo base_url(); ?>admin/get_user_data/" + data_id,
                                    dataType: "JSON",
                                    success: function (response) {
                                        $(".loader").hide();
                                        $.each(response, function (i, value) {
                                            $("#reset_user_id").empty();

                                            $('#reset_user_id').val(value.id);
                                            $('#reset_f_name').val(value.f_name);
                                            $('#reset_m_name').val(value.m_name);
                                            $('#reset_l_name').val(value.l_name);

                                            var reset_descripton = "Do you want to reset password for  User : " + value.f_name + " " + value.m_name + " " + value.l_name + "";
                                            $(".reset_description").append(reset_descripton);
                                        });


                                        $(".reset_div").show();
                                        $(".table_div").hide();


                                    }, error: function (data) {
                                        $(".loader").hide();
                                        sweetAlert("Oops...", "" + error_alert + "", "error");

                                    }

                                });









                            });



                            $(".close_add_div").click(function () {
                                $(".add_div").hide();
                                $(".table_div").show();
                                $(".f_name").empty();

                                $(".l_name").empty();

                                $(".phone_no").empty();
                                $(".e_mail").empty();

                                $(".m_name").empty();
                                $(".dob").empty();

                            });

                            $(".close_delete_div").click(function () {
                                $(".delete_div").hide();
                                $(".table_div").show();
                            });


                            $(".close_edit_div").click(function () {
                                $(".edit_div").hide();
                                $(".table_div").show();
                            });


                            $(".close_reset_div").click(function () {
                                $(".reset_div").hide();
                                $(".table_div").show();
                            });





                            $(".submit_add_div").click(function () {
                                var controller = "admin";
                                var submit_function = "add_user";
                                var form_class = "add_form";
                                var success_alert = "User added successfully ... :) ";
                                var error_alert = "An Error Ocurred";
                                submit_data(controller, submit_function, form_class, success_alert, error_alert);
                            });



                            $(".submit_edit_div").click(function () {
                                var controller = "admin";
                                var submit_function = "edit_user";
                                var form_class = "edit_form";
                                var success_alert = "User data updated successfully ... :) ";
                                var error_alert = "An Error Ocurred";
                                submit_data(controller, submit_function, form_class, success_alert, error_alert);
                            });




                            $(".submit_delete_div").click(function () {
                                var controller = "admin";
                                var submit_function = "delete_user";
                                var form_class = "delete_form";
                                var success_alert = "User data delete successfully ... :) ";
                                var error_alert = "An Error Ocurred";
                                submit_data(controller, submit_function, form_class, success_alert, error_alert);
                            });
                            $(".submit_reset_div").click(function () {
                                var controller = "admin";
                                var submit_function = "reset_user";
                                var form_class = "reset_form";
                                var success_alert = "User password reset successfully ... :) ";
                                var error_alert = "An Error Ocurred";
                                submit_data(controller, submit_function, form_class, success_alert, error_alert);
                            });






                        });
                    </script>




                    <!--END MAIN WRAPPER -->
                    <div class="loader" id="loader" style="display: none;"></div>

                    <!-- FOOTER -->
                    <div id="footer">





                        <p>&copy;  mHealth Kenya &nbsp;2016 &nbsp; &nbsp;         <b> Powered by : mHealth  Kenya </b> </p>
                          <!-- <img src="template/assets/img/mhealth_4.png" alt="" />   -->
                    </div>
                    <!--END FOOTER -->
                    <!-- GLOBAL SCRIPTS -->
                    <script src="<?php echo base_url(); ?>assets/template/assets/plugins/jquery-2.0.3.min.js"></script>
                    <script src="<?php echo base_url(); ?>assets/template/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
                    <script src="<?php echo base_url(); ?>assets/template/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
                    <!-- END GLOBAL SCRIPTS -->
                    <!--start crud styles 
                    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
                    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
                    <!-- START CLOSE CHART STYLES    -->  
                    <!-- indesx chart      -->
                    <script src="<?php echo base_url(); ?>assets/charts/lib/js/jquery.min.js"></script>
                    <script src="<?php echo base_url(); ?>assets/charts/lib/js/chartphp.js"></script>
                    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/charts/lib/js/chartphp.css">



                    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" />

                    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>

                    <script type="text/javascript">
                        $(document).ready(function () {

<?php
$function_name = $this->uri->segment(2);

if ($function_name == "appointment_diary") {
    ?>
                                $(".download_excel").click(function () {
                                    var href = "<?php echo base_url(); ?>home/download_app_diary";
                                    window.location.href = href; //causes the browser to refresh and load the requested url
                                });
    <?php
} else {
    ?>


    <?php
}
?>

                            $('.filter_county').on('change', function () {
                                // Does some stuff and logs the event to the console
                                $(".filter_sub_county").hide();
                                $(".filter_sub_county_wait").show();
                                var county_id = this.value;
                                $.ajax({
                                    url: "<?php echo base_url(); ?>Reports/filter_sub_county/" + county_id + "/",
                                    type: 'GET',
                                    dataType: 'JSON',
                                    success: function (data) {
                                        $(".filter_sub_county").empty();
                                        var select = "<option value=''> Please Select : </option>";
                                        $(".filter_sub_county").append(select);
                                        $.each(data, function (i, value) {
                                            $(".filter_sub_county_wait").hide();
                                            $(".filter_sub_county").show();
                                            var sub_county_options = "<option value=" + value.sub_county_id + ">" + value.sub_county_name + "</option>";
                                            $(".filter_sub_county").append(sub_county_options);
                                        });

                                    }, error: function (jqXHR) {

                                    }
                                })
                            });


                            $('.filter_sub_county').on('change', function () {
                                // Does some stuff and logs the event to the console
                                $(".filter_facility").hide();
                                $(".filter_facility_wait").show();
                                var sub_county_id = this.value;
                                $.ajax({
                                    url: "<?php echo base_url(); ?>Reports/filter_facilities/" + sub_county_id + "/",
                                    type: 'GET',
                                    dataType: 'JSON',
                                    success: function (data) {
                                        $(".filter_facility").empty();
                                        var select = "<option value=''>Please Select : </option>";
                                        $(".filter_facility").append(select);
                                        $.each(data, function (i, value) {
                                            $(".filter_facility_wait").hide();
                                            $(".filter_facility").show();
                                            var facility_options = "<option value=" + value.mfl_code + ">" + value.facility_name + "</option>";
                                            $(".filter_facility").append(facility_options);
                                        });

                                    }, error: function (jqXHR) {

                                    }
                                })
                            });




                            window.submit_data = function (controller, submit_function, form_class, success_alert, error_alert) {

                                $(".loader").show();
                                $("#" + form_class + "").submit(function (event) {
                                    event.preventDefault();
                                    //                $('.loader').show();
                                    dataString = $("#" + form_class + "").serialize();
                                    $(".btn").prop('disabled', true);
                                    $.ajax({
                                        type: "POST",
                                        url: "<?php echo base_url() ?>" + controller + "/" + submit_function + "",
                                        data: dataString,
                                        success: function (data) {
                                            $(".loader").hide();

                                            data = JSON.parse(data);
                                            var response = data[0].response;
                                            if (response === true) {




                                                swal({
                                                    title: "Success!",
                                                    text: "" + success_alert + "",
                                                    type: "success",
                                                    confirmButtonText: "Okay!",
                                                    closeOnConfirm: true
                                                }, function () {
                                                    window.location.reload(1);
                                                });

                                            } else if (response === 'Taken') {
                                                $(".btn").prop('disabled', false);
                                                sweetAlert("Info", "Clinic No already taken ", 'info');
                                            } else if (response === 'Phone Taken') {
                                                $(".btn").prop('disabled', false);
                                                sweetAlert("Info", "Phone No already used in the System ", 'info');
                                            } else if (response === 'Email Taken') {
                                                $(".btn").prop('disabled', false);
                                                sweetAlert("Info", "Email Address already used in the System ", 'info');
                                            } else if (response === 'Phone Email Taken') {
                                                $(".btn").prop('disabled', false);
                                                sweetAlert("Info", "Phone No and Email Address already used in the System ", 'info');
                                            } else if (response === 'Under Age') {
                                                $(".btn").prop('disabled', false);
                                                sweetAlert("Info", "Under Age are not allowed in the System ", 'info');
                                            } else if (response === 'Enrollment greater than DoB') {
                                                $(".btn").prop('disabled', false);
                                                sweetAlert("Info", "Enrollment Date cannot be before than Date of BIrth", 'info');
                                            } else if (response === 'ART greater than DoB') {
                                                $(".btn").prop('disabled', false);
                                                sweetAlert("Info", "ART Date cannot be before  than Date of Birth ", 'info');
                                            } else if (response === 'ART less than Enrollment') {
                                                $(".btn").prop('disabled', false);
                                                sweetAlert("Info", "ART Date cannot be after than Enrollment Date ", 'info');
                                            } else {
                                                $(".btn").prop('disabled', false);
                                                sweetAlert("Oops...", "" + error_alert + "", "error");
                                            }




                                        }, error: function (data) {
                                            $('.loader').hide();
                                            $(".btn").prop('disabled', false);
                                            // sweetAlert("Oops...", "" + error_alert + "", "error");
                                        }

                                    });
                                    event.preventDefault();
                                    return false;
                                });
                            }








                        });</script>

                    <?php
                    $function_name = $this->uri->segment(2);

                    if ($function_name == "facilities") {
                        ?>

                        <?php
                    } else {
                        ?>


                        <?php
                    }
                    ?>




                

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jqc-1.12.3/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.0/b-colvis-1.5.0/b-flash-1.5.0/b-html5-1.5.0/b-print-1.5.0/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.3/sl-1.2.4/datatables.min.css"/>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/jqc-1.12.3/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.0/b-colvis-1.5.0/b-flash-1.5.0/b-html5-1.5.0/b-print-1.5.0/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.3/sl-1.2.4/datatables.min.js"></script>





                    <!--<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs-3.3.7/jq-2.2.4/jszip-2.5.0/pdfmake-0.1.18/dt-1.10.13/af-2.1.3/b-1.2.4/b-colvis-1.2.4/b-flash-1.2.4/b-html5-1.2.4/b-print-1.2.4/cr-1.3.2/fc-3.2.2/fh-3.1.2/kt-2.2.0/r-2.1.1/rg-1.0.0/rr-1.2.0/sc-1.4.2/se-1.2.0/datatables.min.css"/>-->

<!--<script type="text/javascript" src="https://cdn.datatables.net/v/bs-3.3.7/jq-2.2.4/jszip-2.5.0/pdfmake-0.1.18/dt-1.10.13/af-2.1.3/b-1.2.4/b-colvis-1.2.4/b-flash-1.2.4/b-html5-1.2.4/b-print-1.2.4/cr-1.3.2/fc-3.2.2/fh-3.1.2/kt-2.2.0/r-2.1.1/rg-1.0.0/rr-1.2.0/sc-1.4.2/se-1.2.0/datatables.min.js"></script>-->



                    <script type="text/javascript">
                        var dTbles = jQuery.noConflict();
                        dTbles(document).ready(function () {


                            //
                            //        var table = dTbles('#table').DataTable({
                            //            buttons: ['copy', 'excel', 'pdf', 'colvis'],
                            //            responsive: true,
                            //            "lengthMenu": [[5, 10, 25, 50, -1], [10, 25, 50, "All"]]
                            //        });
                            //        table.buttons().container()
                            //                .appendTo('#table_wrapper .col-sm-6:eq(0)');

<?php
if ($function_name == "facility_home") {
    ?>
                                var table = dTbles('#today_app_table').DataTable({
                                    buttons: ['copy', 'excel', 'pdf', 'colvis'],
                                    responsive: true,
                                    "lengthMenu": [[5, 10, 25, 50, -1], [10, 25, 50, "All"]]
                                });
                                table.buttons().container()
                                        .appendTo('#table_wrapper .col-sm-6:eq(0)');
                                var table = dTbles('.missed_table').DataTable({
                                    buttons: ['copy', 'excel', 'pdf', 'colvis'],
                                    responsive: true,
                                    "lengthMenu": [[5, 10, 25, 50, -1], [10, 25, 50, "All"]]
                                });
                                table.buttons().container()
                                        .appendTo('#table_wrapper .col-sm-6:eq(0)');
                                var table = dTbles('.defaulted_table').DataTable({
                                    buttons: ['copy', 'excel', 'pdf', 'colvis'],
                                    responsive: true,
                                    "lengthMenu": [[5, 10, 25, 50, -1], [10, 25, 50, "All"]]
                                });
                                table.buttons().container()
                                        .appendTo('#table_wrapper .col-sm-6:eq(0)');
                                var table = dTbles('.ltfu_table').DataTable({
                                    buttons: ['copy', 'excel', 'pdf', 'colvis'],
                                    responsive: true,
                                    "lengthMenu": [[5, 10, 25, 50, -1], [10, 25, 50, "All"]]
                                });
                                table.buttons().container()
                                        .appendTo('#table_wrapper .col-sm-6:eq(0)');
    <?php
}
?>




                            var table = dTbles('.table').DataTable({
                                buttons: ['copy', 'excel', 'pdf', 'colvis'],
                                responsive: true,
                                "lengthMenu": [[5, 10, 25, 50, -1], [10, 25, 50, "All"]]
                            });
                            table.buttons().container()
                                    .appendTo('#table_wrapper .col-sm-6:eq(0)');






                            function create_audit_trail_results_layout() {

                                var value = dTbles(".audit_trail_search_value").val();
                                dTbles.ajax({
                                    type: 'GET',
                                    url: "<?php echo base_url(); ?>support/search_audit_trail/" + value,
                                    dataType: 'JSON',
                                    "dataSrc": "results",
                                    success: function (results) {
                                        var check_data = jQuery.isEmptyObject(results);
                                        if (check_data === true) {
                                            dTbles(".audit_trail_search_results_div").hide();

                                        } else {

                                            dTbles(".audit_trail_search_results_div").empty();

                                            var table = '<table class=" dataTables_processing  table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">\n\
                                 <thead> <tr> <th>Date Added : </th> <th> Username  </th> <th>Description  :</th>  <th>Phone Number   : </th>   </tr> </thead>  \n\
                                   <tbody id="audit_trail_results_tbody" class="audit_trail_results_tbody"> </tbody> </table>';


                                            dTbles('.audit_trail_search_results_div').append(table);

                                            dTbles.each(results, function (i, messsages) {


                                                var tr_results = "<tr>\n\
                                        <td>" + messsages.created_at + "</td>\n\
                                        <td>" + messsages.user_name + "</td>\n\
                                        <td>" + messsages.description + "</td>\n\
                                        <td>" + messsages.phone_no + "</td>\n\
                                        </tr>";

                                                dTbles(".audit_trail_results_tbody").append(tr_results);
                                                dTbles(".audit_trail_search_results_div").show();
                                            });
                                            //dTbles('.dataTables_processing').DataTable({});


                                            var table = dTbles('.dataTables_processing').DataTable({
                                                buttons: ['copy', 'excel', 'pdf', 'colvis'],
                                                responsive: true,
                                                "lengthMenu": [[5, 10, 25, 50, -1], [10, 25, 50, "All"]]
                                            });
                                            table.buttons().container()
                                                    .appendTo('#table_wrapper .col-sm-6:eq(0)');


                                        }
                                    }
                                });
                            }

                            dTbles(".audit_trail_serach_btn").click(function () {


                                if (!dTbles("input[name='audit_trail_search_option']:checked").val()) {

                                    sweetAlert("Info", "Nothing is checked!, Please select one of the Search Parameters... ", 'info');
                                } else {
                                    create_audit_trail_results_layout();
                                }
                            });










                            function create_incoming_results_layout() {

                                var value = dTbles(".incoming_search_value").val();
                                dTbles.ajax({
                                    type: 'GET',
                                    url: "<?php echo base_url(); ?>support/search_incoming/" + value,
                                    dataType: 'JSON',
                                    "dataSrc": "results",
                                    success: function (results) {
                                        var check_data = jQuery.isEmptyObject(results);
                                        if (check_data === true) {
                                            dTbles(".incoming_search_results_div").hide();

                                        } else {

                                            dTbles(".incoming_search_results_div").empty();

                                            var table = '<table class=" dataTables_processing  table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">\n\
                                 <thead> <tr> <th>From  No</th> <th>Destination No :</th> <th>Message : </th><th>Date Added : </th> <th>Time Stamp   : </th><th>Processed : </th>   </tr> </thead>  \n\
                                   <tbody id="incoming_results_tbody" class="incoming_results_tbody"> </tbody> </table>';


                                            dTbles('.incoming_search_results_div').append(table);

                                            dTbles.each(results, function (i, messsages) {


                                                var tr_results = "<tr>\n\
                                        <td>" + messsages.source + "</td>\n\
                                        <td>" + messsages.destination + "</td>\n\
                                        <td>" + messsages.msg + "</td>\n\
                                        <td>" + messsages.created_at + "</td>\n\
                                        <td>" + messsages.updated_at + "</td>\n\
                                        <td>" + messsages.processed + "</td>\n\
                                        </tr>";
                                                console.log(tr_results);
                                                dTbles(".incoming_results_tbody").append(tr_results);
                                                dTbles(".incoming_search_results_div").show();
                                            });
                                            dTbles('.dataTables_processing').DataTable({});



                                        }
                                    }
                                });
                            }

                            dTbles(".incoming_serach_btn").click(function () {


                                if (!dTbles("input[name='incoming_search_option']:checked").val()) {

                                    sweetAlert("Info", "Nothing is checked!, Please select one of the Search Parameters... ", 'info');
                                } else {
                                    create_incoming_results_layout();
                                }
                            });




                            function create_outgoing_results_layout() {

                                var value = dTbles(".outgoing_search_value").val();
                                dTbles.ajax({
                                    type: 'GET',
                                    url: "<?php echo base_url(); ?>support/search_outgoing/" + value,
                                    dataType: 'JSON',
                                    "dataSrc": "results",
                                    success: function (results) {
                                        var check_data = jQuery.isEmptyObject(results);
                                        if (check_data === true) {
                                            dTbles(".outgoing_search_results_div").hide();

                                        } else {

                                            dTbles(".outgoing_search_results_div").empty();

                                            var table = '<table class=" dataTables_processing  table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">\n\
                                 <thead> <tr> <th>From  No</th> <th>Destination No :</th> <th>Message : </th><th>Date Added : </th> <th>Time Stamp   : </th><th>Processed : </th>   </tr> </thead>  \n\
                                   <tbody id="outgoing_results_tbody" class="outgoing_results_tbody"> </tbody> </table>';


                                            dTbles('.outgoing_search_results_div').append(table);

                                            dTbles.each(results, function (i, messsages) {


                                                var tr_results = "<tr>\n\
                                        <td>" + messsages.source + "</td>\n\
                                        <td>" + messsages.destination + "</td>\n\
                                        <td>" + messsages.msg + "</td>\n\
                                        <td>" + messsages.created_at + "</td>\n\
                                        <td>" + messsages.updated_at + "</td>\n\
                                        <td>" + messsages.processed + "</td>\n\
                                        </tr>";
                                                console.log(tr_results);
                                                dTbles(".outgoing_results_tbody").append(tr_results);
                                                dTbles(".outgoing_search_results_div").show();
                                            });
                                            dTbles('.dataTables_processing').DataTable({});



                                        }
                                    }
                                });
                            }

                            dTbles(".outgoing_serach_btn").click(function () {


                                if (!dTbles("input[name='outgoing_search_option']:checked").val()) {

                                    sweetAlert("Info", "Nothing is checked!, Please select one of the Search Parameters... ", 'info');
                                } else {
                                    create_outgoing_results_layout();
                                }
                            });















                            function create_results_layout() {

                                var value = dTbles(".search_value").val();
                                dTbles.ajax({
                                    type: 'GET',
                                    url: "<?php echo base_url(); ?>admin/search_facility/" + value,
                                    dataType: 'JSON',
                                    "dataSrc": "results",
                                    success: function (results) {
                                        var check_data = jQuery.isEmptyObject(results);
                                        if (check_data === true) {
                                            dTbles(".search_results_div").hide();

                                        } else {

                                            dTbles(".search_results_div").empty();

                                            var table = '<table class=" dataTables_processing  table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">\n\
                                 <thead> <tr> <th>Name</th> <th>MFL No :</th> <th>County : </th><th>Sub County : </th> <th>Consituency : </th><th>Owner: </th> <th>Add Facility </th>  </tr> </thead>  \n\
                                   <tbody id="results_tbody" class="results_tbody"> </tbody> </table>';
                                            dTbles('.search_results_div').append(table);

                                            dTbles.each(results, function (i, facilities) {


                                                var tr_results = "<tr>\n\
                                        <td>" + facilities.facility_name + "</td>\n\
                                        <td>" + facilities.mfl_code + "</td>\n\
                                        <td>" + facilities.owner + "</td>\n\
                                        <td>" + facilities.county_name + "</td>\n\
                                        <td>" + facilities.sub_county_name + "</td>\n\
                                        <td>" + facilities.consituency_name + "</td>\n\
                                        <td><input type='hidden' id='hidden_mfl_code' class=' hidden_mfl_code form-control' name='hidden_mfl_code' value='" + facilities.mfl_code + "'/>\n\
                                        <button class='btn btn-xs btn-default select_mfl_btn' id='select_donor_btn' data-original-title='Select Facility'>\n\
                                        <span class='glyphicon glyphicon-plus'></span> </button></td>  \n\
                                        </tr>";
                                                dTbles(".results_tbody").append(tr_results);
                                                dTbles(".search_results_div").show();
                                            });
                                            dTbles('.dataTables_processing').DataTable({});



                                        }
                                    }
                                });
                            }

                            dTbles(".search_value").keyup(function () {

                                if (!dTbles("input[name='search_option']:checked").val()) {

                                    sweetAlert("Info", "Nothing is checked!, Please select one of the Search Parameters... ", 'info');
                                } else {
                                    create_results_layout();
                                }
                            });






















                            window.create_percentage_counties_report = function create_percentage_counties_report(data) {




                                dTbles(".percentage_counties_div").empty();

                                var table = '<table id="percentage_counties_table" class=" percentage_counties_table dashboard_report dataTables_processing  table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">\n\
                                 <thead> <tr> <th>Name</th> <th> No :</th>  </tr> </thead>  \n\
                                   <tbody id="percentage_counties_tbody" class="percentage_counties_tbody"> </tbody> </table>';
                                dTbles('.percentage_counties_div').append(table);

                                dTbles.each(data, function (i, val) {
                                    var target_counties = val.target_counties;
                                    var actual_counties = val.actual_counties;

                                    var tr_results = "<tr>\n\
                                        <td>Target Counties</td>\n\
                                        <td>" + target_counties + "</td>\n\
                                        </tr><tr>\n\
                                         <td>Actual Counties</td>\n\
                                        <td>" + actual_counties + "</td>\n\</tr>";
                                    dTbles(".percentage_counties_tbody").append(tr_results);

                                });
                                dTbles(".percentage_counties_div").show();

                                dTbles('.percentage_counties_table').DataTable({dom: 'Bfrtip',
                                    "searching": false,
                                    "bInfo": false,
                                    "bPaginate": false,
                                    buttons: [
                                        {
                                            extend: 'copyHtml5',
                                            exportOptions: {
                                                columns: ':contains("Office")'
                                            }
                                        },
                                        'excelHtml5',
                                        'csvHtml5',
                                        'pdfHtml5'
                                    ]});


                            }





                            window.create_client_reg_report = function create_client_reg_report(data) {




                                dTbles(".client_reg_report_div").empty();

                                var table = '<table id="client_reg_dashboard_table" class=" client_reg_dashboard_table dashboard_report dataTables_processing  table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">\n\
                                 <thead> <tr> <th>Name</th> <th> No :</th>  </tr> </thead>  \n\
                                   <tbody id="client_reg_results_tbody" class="client_reg_results_tbody"> </tbody> </table>';
                                dTbles('.client_reg_report_div').append(table);

                                dTbles.each(data, function (i, val) {
                                    var name = val.k;
                                    var value = val.v;

                                    var tr_results = "<tr>\n\
                                        <td>" + name + "</td>\n\
                                        <td>" + value + "</td>\n\</td>\n\
                                        </tr>";
                                    dTbles(".client_reg_results_tbody").append(tr_results);

                                });
                                dTbles(".client_reg_report_div").show();

                                dTbles('.client_reg_dashboard_table').DataTable({dom: 'Bfrtip',
                                    "searching": false,
                                    "bInfo": false,
                                    "bPaginate": false,
                                    buttons: [
                                        {
                                            extend: 'copyHtml5',
                                            exportOptions: {
                                                columns: ':contains("Office")'
                                            }
                                        },
                                        'excelHtml5',
                                        'csvHtml5',
                                        'pdfHtml5'
                                    ]});


                            }










                            window.create_client_appointment_report = function create_client_appointment_report(data) {




                                dTbles(".client_appointment_report_div").empty();

                                var table = '<table id="client_appointment_dashboard_table" class=" client_appointment_dashboard_table dashboard_report dataTables_processing  table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">\n\
                                 <thead> <tr> <th>Name</th> <th> No :</th>  </tr> </thead>  \n\
                                   <tbody id="client_appointment_results_tbody" class="client_appointment_results_tbody"> </tbody> </table>';
                                dTbles('.client_appointment_report_div').append(table);

                                dTbles.each(data, function (i, val) {
                                    var name = val.k;
                                    var value = val.v;

                                    var tr_results = "<tr>\n\
                                        <td>" + name + "</td>\n\
                                        <td>" + value + "</td>\n\</td>\n\
                                        </tr>";
                                    dTbles(".client_appointment_results_tbody").append(tr_results);

                                });
                                dTbles(".client_appointment_report_div").show();

                                dTbles('.client_appointment_dashboard_table').DataTable({dom: 'Bfrtip',
                                    "searching": false,
                                    "bInfo": false,
                                    "bPaginate": false,
                                    buttons: [
                                        {
                                            extend: 'copyHtml5',
                                            exportOptions: {
                                                columns: ':contains("Office")'
                                            }
                                        },
                                        'excelHtml5',
                                        'csvHtml5',
                                        'pdfHtml5'
                                    ]});


                            }

                            window.create_gender_report = function create_gender_report(data) {


                                var table = dTbles('#gender_dashboard_table').DataTable({
                                    lengthChange: false,
                                    buttons: ['copy', 'excel', 'pdf', 'colvis']
                                });
                                table.buttons().container()
                                        .appendTo('#table_wrapper .col-sm-6:eq(0)');




                                dTbles(".gender_report_div").empty();

                                var table = '<table id="gender_dashboard_table" class=" gender_dashboard_table dashboard_report dataTables_processing  table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">\n\
                                 <thead> <tr> <th>Name</th> <th> No :</th>  </tr> </thead>  \n\
                                   <tbody id="gender_results_tbody" class="gender_results_tbody"> </tbody> </table>';
                                dTbles('.gender_report_div').append(table);

                                dTbles.each(data, function (i, val) {
                                    var name = val.name;
                                    var value = val.value;

                                    var tr_results = "<tr>\n\
                                        <td>" + name + "</td>\n\
                                        <td>" + value + "</td>\n\</td>\n\
                                        </tr>";
                                    dTbles(".gender_results_tbody").append(tr_results);

                                });
                                dTbles(".gender_report_div").show();

                                dTbles('.gender_dashboard_table').DataTable({dom: 'Bfrtip',
                                    "searching": false,
                                    "bInfo": false,
                                    "bPaginate": false,
                                    buttons: [
                                        {
                                            extend: 'copyHtml5',
                                            exportOptions: {
                                                columns: ':contains("Office")'
                                            }
                                        },
                                        'excelHtml5',
                                        'csvHtml5',
                                        'pdfHtml5'
                                    ]});


                            }


                            window.create_message_distribution_report = function create_message_distribution_report(data) {

                                dTbles(".message_distribution_report_div").empty();

                                var table = dTbles('#message_distributon_dashboard_table').DataTable({
                                    lengthChange: false,
                                    buttons: ['copy', 'excel', 'pdf', 'colvis']
                                });
                                table.buttons().container()
                                        .appendTo('#table_wrapper .col-sm-6:eq(0)');





                                var table = '<table id="message_distributon_dashboard_table" class=" message_distributon_dashboard_table dashboard_report dataTables_processing  table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">\n\
                                 <thead> <tr> <th>Name</th> <th> No :</th>  </tr> </thead>  \n\
                                   <tbody id="message_distribution_results_tbody" class="message_distribution_results_tbody"> </tbody> </table>';
                                dTbles('.message_distribution_report_div').append(table);

                                dTbles.each(data, function (i, val) {
                                    var name = val.message_type;
                                    var value = val.no_messages;
                                    //console.log('Message Type => ' + name + ' No of Messages => ' + value + ' end ');
                                    var tr_results = "<tr>\n\
                                        <td>" + name + "</td>\n\
                                        <td>" + value + "</td>\n\</td>\n\
                                        </tr>";
                                    dTbles(".message_distribution_results_tbody").append(tr_results);

                                });
                                dTbles(".message_distribution_report_div").show();

                                dTbles('.message_distributon_dashboard_table').DataTable({
                                    dom: 'Bfrtip',
                                    "searching": false,
                                    "bInfo": false,
                                    "bPaginate": false,
                                    buttons: [
                                        {
                                            extend: 'copyHtml5',
                                            exportOptions: {
                                                columns: ':contains("Office")'
                                            }
                                        },
                                        'excelHtml5',
                                        'csvHtml5',
                                        'pdfHtml5'
                                    ]});


                            }


                            window.create_client_language_report = function create_client_language_report(data) {





                                dTbles(".language_report_div").empty();

                                var table = '<table id="language_dashboard_table" class=" language_dashboard_table dashboard_report dataTables_processing  table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">\n\
                                 <thead> <tr> <th>Name</th> <th> No :</th>  </tr> </thead>  \n\
                                   <tbody id="language_results_tbody" class="language_results_tbody"> </tbody> </table>';
                                dTbles('.language_report_div').append(table);

                                dTbles.each(data, function (i, val) {
                                    var name = val.NAME;
                                    var value = val.VALUE;

                                    var tr_results = "<tr>\n\
                                        <td>" + name + "</td>\n\
                                        <td>" + value + "</td>\n\</td>\n\
                                        </tr>";
                                    dTbles(".language_results_tbody").append(tr_results);

                                });
                                dTbles(".language_report_div").show();
                                dTbles('.language_dashboard_table').DataTable({dom: 'Bfrtip',
                                    "searching": false,
                                    "bInfo": false,
                                    "bPaginate": false,
                                    buttons: [
                                        {
                                            extend: 'copyHtml5',
                                            exportOptions: {
                                                columns: ':contains("Office")'
                                            }
                                        },
                                        'excelHtml5',
                                        'csvHtml5',
                                        'pdfHtml5'
                                    ]});



                            }
                            window.create_client_marital_report = function create_client_marital_report(data) {





                                dTbles(".marital_report_div").empty();

                                var table = '<table id="marital_dashboard_table" class=" marital_dashboard_table dashboard_report dataTables_processing  table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">\n\
                                 <thead> <tr> <th>Name</th> <th> No :</th>  </tr> </thead>  \n\
                                   <tbody id="marital_results_tbody" class="marital_results_tbody"> </tbody> </table>';
                                dTbles('.marital_report_div').append(table);

                                dTbles.each(data, function (i, val) {
                                    var name = val.NAME;
                                    var value = val.VALUE;

                                    var tr_results = "<tr>\n\
                                        <td>" + name + "</td>\n\
                                        <td>" + value + "</td>\n\</td>\n\
                                        </tr>";
                                    dTbles(".marital_results_tbody").append(tr_results);

                                });
                                dTbles(".marital_report_div").show();
                                dTbles('.marital_dashboard_table').DataTable({dom: 'Bfrtip',
                                    "searching": false,
                                    "bInfo": false,
                                    "bPaginate": false,
                                    buttons: [
                                        {
                                            extend: 'copyHtml5',
                                            exportOptions: {
                                                columns: ':contains("Office")'
                                            }
                                        },
                                        'excelHtml5',
                                        'csvHtml5',
                                        'pdfHtml5'
                                    ]});



                            }
                            window.create_client_type_report = function create_client_type_report(data) {





                                dTbles(".type_report_div").empty();

                                var table = '<table id="type_dashboard_table" class=" type_dashboard_table dashboard_report dataTables_processing  table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">\n\
                                 <thead> <tr> <th>Name</th> <th> No :</th>  </tr> </thead>  \n\
                                   <tbody id="type_results_tbody" class="type_results_tbody"> </tbody> </table>';
                                dTbles('.type_report_div').append(table);

                                dTbles.each(data, function (i, val) {
                                    var name = val.k;
                                    var value = val.v;

                                    var tr_results = "<tr>\n\
                                        <td>" + name + "</td>\n\
                                        <td>" + value + "</td>\n\</td>\n\
                                        </tr>";
                                    dTbles(".type_results_tbody").append(tr_results);

                                });
                                dTbles(".type_report_div").show();
                                dTbles('.type_dashboard_table').DataTable({dom: 'Bfrtip',
                                    "searching": false,
                                    "bInfo": false,
                                    "bPaginate": false,
                                    buttons: [
                                        {
                                            extend: 'copyHtml5',
                                            exportOptions: {
                                                columns: ':contains("Office")'
                                            }
                                        },
                                        'excelHtml5',
                                        'csvHtml5',
                                        'pdfHtml5'
                                    ]});



                            }

                            window.create_client_category_report = function create_client_category_report(data) {





                                dTbles(".client_category_report_div").empty();

                                var table = '<table id="client_category_dashboard_table" class=" client_category_dashboard_table dashboard_report dataTables_processing  table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">\n\
                                 <thead> <tr> <th>Name</th> <th> No :</th>  </tr> </thead>  \n\
                                   <tbody id="client_category_results_tbody" class="client_category_results_tbody"> </tbody> </table>';
                                dTbles('.client_category_report_div').append(table);

                                dTbles.each(data, function (i, val) {
                                    var name = val.k;
                                    var value = val.v;

                                    var tr_results = "<tr>\n\
                                        <td>" + name + "</td>\n\
                                        <td>" + value + "</td>\n\</td>\n\
                                        </tr>";
                                    dTbles(".client_category_results_tbody").append(tr_results);

                                });
                                dTbles(".client_category_report_div").show();
                                dTbles('.client_category_dashboard_table').DataTable({dom: 'Bfrtip',
                                    "searching": false,
                                    "bInfo": false,
                                    "bPaginate": false,
                                    buttons: [
                                        {
                                            extend: 'copyHtml5',
                                            exportOptions: {
                                                columns: ':contains("Office")'
                                            }
                                        },
                                        'excelHtml5',
                                        'csvHtml5',
                                        'pdfHtml5'
                                    ]});



                            }

                            window.create_client_age_group_report = function create_client_age_group_report(data) {





                                dTbles(".client_age_group_report_div").empty();

                                var table = '<table id="client_age_group_dashboard_table" class=" client_age_group_dashboard_table dashboard_report dataTables_processing  table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">\n\
                                 <thead> <tr> <th>Name</th> <th> No :</th>  </tr> </thead>  \n\
                                   <tbody id="client_age_group_results_tbody" class="client_age_group_results_tbody"> </tbody> </table>';
                                dTbles('.client_age_group_report_div').append(table);

                                dTbles.each(data, function (i, val) {
                                    var name = val.k;
                                    var value = val.v;

                                    var tr_results = "<tr>\n\
                                        <td>" + name + "</td>\n\
                                        <td>" + value + "</td>\n\</td>\n\
                                        </tr>";
                                    dTbles(".client_age_group_results_tbody").append(tr_results);

                                });
                                dTbles(".client_age_group_report_div").show();
                                dTbles('.client_age_group_dashboard_table').DataTable({dom: 'Bfrtip',
                                    "searching": false,
                                    "bInfo": false,
                                    "bPaginate": false,
                                    buttons: [
                                        {
                                            extend: 'copyHtml5',
                                            exportOptions: {
                                                columns: ':contains("Office")'
                                            }
                                        },
                                        'excelHtml5',
                                        'csvHtml5',
                                        'pdfHtml5'
                                    ]});



                            }

                            window.create_client_condition_report = function create_client_condition_report(data) {





                                dTbles(".client_condition_report_div").empty();

                                var table = '<table id="client_condition_dashboard_table" class=" client_condition_dashboard_table dashboard_report dataTables_processing  table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">\n\
                                 <thead> <tr> <th>Name</th> <th> No :</th>  </tr> </thead>  \n\
                                   <tbody id="client_condition_results_tbody" class="client_condition_results_tbody"> </tbody> </table>';
                                dTbles('.client_condition_report_div').append(table);

                                dTbles.each(data, function (i, val) {
                                    var name = val.k;
                                    var value = val.v;

                                    var tr_results = "<tr>\n\
                                        <td>" + name + "</td>\n\
                                        <td>" + value + "</td>\n\</td>\n\
                                        </tr>";
                                    dTbles(".client_condition_results_tbody").append(tr_results);

                                });
                                dTbles(".client_condition_report_div").show();
                                dTbles('.client_condition_dashboard_table').DataTable({dom: 'Bfrtip',
                                    "searching": false,
                                    "bInfo": false,
                                    "bPaginate": false,
                                    buttons: [
                                        {
                                            extend: 'copyHtml5',
                                            exportOptions: {
                                                columns: ':contains("Office")'
                                            }
                                        },
                                        'excelHtml5',
                                        'csvHtml5',
                                        'pdfHtml5'
                                    ]});



                            }

                            window.create_client_status_report = function create_client_status_report(data) {





                                dTbles(".client_status_report_div").empty();

                                var table = '<table id="client_status_dashboard_table" class=" client_status_dashboard_table dashboard_report dataTables_processing  table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">\n\
                                 <thead> <tr> <th>Name</th> <th> No :</th>  </tr> </thead>  \n\
                                   <tbody id="client_status_results_tbody" class="client_status_results_tbody"> </tbody> </table>';
                                dTbles('.client_status_report_div').append(table);

                                dTbles.each(data, function (i, val) {
                                    var name = val.k;
                                    var value = val.v;

                                    var tr_results = "<tr>\n\
                                        <td>" + name + "</td>\n\
                                        <td>" + value + "</td>\n\</td>\n\
                                        </tr>";
                                    dTbles(".client_status_results_tbody").append(tr_results);

                                });
                                dTbles(".client_status_report_div").show();
                                dTbles('.client_status_dashboard_table').DataTable({dom: 'Bfrtip',
                                    "searching": false,
                                    "bInfo": false,
                                    "bPaginate": false,
                                    buttons: [
                                        {
                                            extend: 'copyHtml5',
                                            exportOptions: {
                                                columns: ':contains("Office")'
                                            }
                                        },
                                        'excelHtml5',
                                        'csvHtml5',
                                        'pdfHtml5'
                                    ]});



                            }

                            window.create_client_consented_report = function create_client_consented_report(data) {





                                dTbles(".client_consented_report_div").empty();

                                var table = '<table id="client_consented_dashboard_table" class=" client_consented_dashboard_table dashboard_report dataTables_processing  table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">\n\
                                 <thead> <tr> <th>Name</th> <th> No :</th>  </tr> </thead>  \n\
                                   <tbody id="client_consented_results_tbody" class="client_consented_results_tbody"> </tbody> </table>';
                                dTbles('.client_consented_report_div').append(table);

                                dTbles.each(data, function (i, val) {
                                    var name = val.k;
                                    var value = val.v;

                                    var tr_results = "<tr>\n\
                                        <td>" + name + "</td>\n\
                                        <td>" + value + "</td>\n\</td>\n\
                                        </tr>";
                                    dTbles(".client_consented_results_tbody").append(tr_results);

                                });
                                dTbles(".client_consented_report_div").show();
                                dTbles('.client_consented_dashboard_table').DataTable({dom: 'Bfrtip',
                                    "searching": false,
                                    "bInfo": false,
                                    "bPaginate": false,
                                    buttons: [
                                        {
                                            extend: 'copyHtml5',
                                            exportOptions: {
                                                columns: ':contains("Office")'
                                            }
                                        },
                                        'excelHtml5',
                                        'csvHtml5',
                                        'pdfHtml5'
                                    ]});



                            }

                            window.create_client_consented_gender_report = function create_client_consented_gender_report(data) {





                                dTbles(".client_consented_gender_report_div").empty();

                                var table = '<table id="client_consented_gender_dashboard_table" class=" client_consented_gender_dashboard_table dashboard_report dataTables_processing  table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">\n\
                                 <thead> <tr> <th>Name</th> <th> No :</th>  </tr> </thead>  \n\
                                   <tbody id="client_consented_gender_results_tbody" class="client_consented_gender_results_tbody"> </tbody> </table>';
                                dTbles('.client_consented_gender_report_div').append(table);

                                dTbles.each(data, function (i, val) {
                                    var name = val.gender;
                                    var value = val.total_client;

                                    var tr_results = "<tr>\n\
                                        <td>" + name + "</td>\n\
                                        <td>" + value + "</td>\n\</td>\n\
                                        </tr>";
                                    dTbles(".client_consented_gender_results_tbody").append(tr_results);

                                });
                                dTbles(".client_consented_gender_report_div").show();
                                dTbles('.client_consented_gender_dashboard_table').DataTable({dom: 'Bfrtip',
                                    "searching": false,
                                    "bInfo": false,
                                    "bPaginate": false,
                                    buttons: [
                                        {
                                            extend: 'copyHtml5',
                                            exportOptions: {
                                                columns: ':contains("Office")'
                                            }
                                        },
                                        'excelHtml5',
                                        'csvHtml5',
                                        'pdfHtml5'
                                    ]});



                            }

                            window.create_client_consented_marital_report = function create_client_consented_marital_report(data) {





                                dTbles(".client_consented_marital_report_div").empty();

                                var table = '<table id="client_consented_marital_dashboard_table" class=" client_consented_marital_dashboard_table dashboard_report dataTables_processing  table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">\n\
                                 <thead> <tr> <th>Name</th> <th> No :</th>  </tr> </thead>  \n\
                                   <tbody id="client_consented_marital_results_tbody" class="client_consented_marital_results_tbody"> </tbody> </table>';
                                dTbles('.client_consented_marital_report_div').append(table);

                                dTbles.each(data, function (i, val) {
                                    var name = val.marital;
                                    var value = val.total_client;

                                    var tr_results = "<tr>\n\
                                        <td>" + name + "</td>\n\
                                        <td>" + value + "</td>\n\</td>\n\
                                        </tr>";
                                    dTbles(".client_consented_marital_results_tbody").append(tr_results);

                                });
                                dTbles(".client_consented_marital_report_div").show();
                                dTbles('.client_consented_marital_dashboard_table').DataTable({dom: 'Bfrtip',
                                    "searching": false,
                                    "bInfo": false,
                                    "bPaginate": false,
                                    buttons: [
                                        {
                                            extend: 'copyHtml5',
                                            exportOptions: {
                                                columns: ':contains("Office")'
                                            }
                                        },
                                        'excelHtml5',
                                        'csvHtml5',
                                        'pdfHtml5'
                                    ]});



                            }


                            window.create_client_consented_category_report = function create_client_consented_category_report(data) {





                                dTbles(".client_consented_category_report_div").empty();

                                var table = '<table id="client_consented_category_dashboard_table" class=" client_consented_category_dashboard_table dashboard_report dataTables_processing  table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">\n\
                                 <thead> <tr> <th>Name</th> <th> No :</th>  </tr> </thead>  \n\
                                   <tbody id="client_consented_category_results_tbody" class="client_consented_category_results_tbody"> </tbody> </table>';
                                dTbles('.client_consented_category_report_div').append(table);

                                dTbles.each(data, function (i, val) {
                                    var name = val.group_name;
                                    var value = val.total_client;

                                    var tr_results = "<tr>\n\
                                        <td>" + name + "</td>\n\
                                        <td>" + value + "</td>\n\</td>\n\
                                        </tr>";
                                    dTbles(".client_consented_category_results_tbody").append(tr_results);

                                });
                                dTbles(".client_consented_category_report_div").show();
                                dTbles('.client_consented_category_dashboard_table').DataTable({dom: 'Bfrtip',
                                    "searching": false,
                                    "bInfo": false,
                                    "bPaginate": false,
                                    buttons: [
                                        {
                                            extend: 'copyHtml5',
                                            exportOptions: {
                                                columns: ':contains("Office")'
                                            }
                                        },
                                        'excelHtml5',
                                        'csvHtml5',
                                        'pdfHtml5'
                                    ]});



                            }


                            window.create_client_appointment_status_report = function create_client_appointment_status_report(data) {





                                dTbles(".client_appointment_status_report_div").empty();

                                var table = '<table id="client_appointment_status_dashboard_table" class=" client_appointment_status_dashboard_table dashboard_report dataTables_processing  table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">\n\
                                 <thead> <tr> <th>Name</th> <th> No :</th>  </tr> </thead>  \n\
                                   <tbody id="client_appointment_status_results_tbody" class="client_appointment_status_results_tbody"> </tbody> </table>';
                                dTbles('.client_appointment_status_report_div').append(table);

                                dTbles.each(data, function (i, val) {
                                    var name = val.app_status;
                                    var value = val.total;

                                    var tr_results = "<tr>\n\
                                        <td>" + name + "</td>\n\
                                        <td>" + value + "</td>\n\</td>\n\
                                        </tr>";
                                    dTbles(".client_appointment_status_results_tbody").append(tr_results);

                                });
                                dTbles(".client_appointment_status_report_div").show();
                                dTbles('.client_appointment_status_dashboard_table').DataTable({dom: 'Bfrtip',
                                    "searching": false,
                                    "bInfo": false,
                                    "bPaginate": false,
                                    buttons: [
                                        {
                                            extend: 'copyHtml5',
                                            exportOptions: {
                                                columns: ':contains("Office")'
                                            }
                                        },
                                        'excelHtml5',
                                        'csvHtml5',
                                        'pdfHtml5'
                                    ]});



                            }


                            window.create_client_booked_appointment_status_report = function create_client_booked_appointment_status_report(data) {





                                dTbles(".client_booked_appointment_status_report_div").empty();

                                var table = '<table id="client_booked_appointment_status_dashboard_table" class=" client_booked_appointment_status_dashboard_table dashboard_report dataTables_processing  table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">\n\
                                 <thead> <tr> <th>Name</th> <th> No :</th>  </tr> </thead>  \n\
                                   <tbody id="client_booked_appointment_status_results_tbody" class="client_booked_appointment_status_results_tbody"> </tbody> </table>';
                                dTbles('.client_booked_appointment_status_report_div').append(table);

                                dTbles.each(data, function (i, val) {
                                    var name = val.group_name;
                                    var value = val.total;

                                    var tr_results = "<tr>\n\
                                        <td>" + name + "</td>\n\
                                        <td>" + value + "</td>\n\</td>\n\
                                        </tr>";
                                    dTbles(".client_booked_appointment_status_results_tbody").append(tr_results);

                                });
                                dTbles(".client_booked_appointment_status_report_div").show();
                                dTbles('.client_booked_appointment_status_dashboard_table').DataTable({dom: 'Bfrtip',
                                    "searching": false,
                                    "bInfo": false,
                                    "bPaginate": false,
                                    buttons: [
                                        {
                                            extend: 'copyHtml5',
                                            exportOptions: {
                                                columns: ':contains("Office")'
                                            }
                                        },
                                        'excelHtml5',
                                        'csvHtml5',
                                        'pdfHtml5'
                                    ]});



                            }



                            window.create_client_notified_appointment_status_report = function create_client_notified_appointment_status_report(data) {





                                dTbles(".client_notified_appointment_status_report_div").empty();

                                var table = '<table id="client_notified_appointment_status_dashboard_table" class=" client_notified_appointment_status_dashboard_table dashboard_report dataTables_processing  table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">\n\
                                 <thead> <tr> <th>Name</th> <th> No :</th>  </tr> </thead>  \n\
                                   <tbody id="client_notified_appointment_status_results_tbody" class="client_notified_appointment_status_results_tbody"> </tbody> </table>';
                                dTbles('.client_notified_appointment_status_report_div').append(table);

                                dTbles.each(data, function (i, val) {
                                    var name = val.group_name;
                                    var value = val.total;

                                    var tr_results = "<tr>\n\
                                        <td>" + name + "</td>\n\
                                        <td>" + value + "</td>\n\</td>\n\
                                        </tr>";
                                    dTbles(".client_notified_appointment_status_results_tbody").append(tr_results);

                                });
                                dTbles(".client_notified_appointment_status_report_div").show();
                                dTbles('.client_notified_appointment_status_dashboard_table').DataTable({dom: 'Bfrtip',
                                    "searching": false,
                                    "bInfo": false,
                                    "bPaginate": false,
                                    buttons: [
                                        {
                                            extend: 'copyHtml5',
                                            exportOptions: {
                                                columns: ':contains("Office")'
                                            }
                                        },
                                        'excelHtml5',
                                        'csvHtml5',
                                        'pdfHtml5'
                                    ]});



                            }




                            window.create_client_missed_appointment_status_report = function create_client_missed_appointment_status_report(data) {





                                dTbles(".client_missed_appointment_status_report_div").empty();

                                var table = '<table id="client_missed_appointment_status_dashboard_table" class=" client_missed_appointment_status_dashboard_table dashboard_report dataTables_processing  table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">\n\
                                 <thead> <tr> <th>Name</th> <th> No :</th>  </tr> </thead>  \n\
                                   <tbody id="client_missed_appointment_status_results_tbody" class="client_missed_appointment_status_results_tbody"> </tbody> </table>';
                                dTbles('.client_missed_appointment_status_report_div').append(table);

                                dTbles.each(data, function (i, val) {
                                    var name = val.group_name;
                                    var value = val.total;

                                    var tr_results = "<tr>\n\
                                        <td>" + name + "</td>\n\
                                        <td>" + value + "</td>\n\</td>\n\
                                        </tr>";
                                    dTbles(".client_missed_appointment_status_results_tbody").append(tr_results);

                                });
                                dTbles(".client_missed_appointment_status_report_div").show();
                                dTbles('.client_missed_appointment_status_dashboard_table').DataTable({dom: 'Bfrtip',
                                    "searching": false,
                                    "bInfo": false,
                                    "bPaginate": false,
                                    buttons: [
                                        {
                                            extend: 'copyHtml5',
                                            exportOptions: {
                                                columns: ':contains("Office")'
                                            }
                                        },
                                        'excelHtml5',
                                        'csvHtml5',
                                        'pdfHtml5'
                                    ]});



                            }





                            window.create_client_defaulted_appointment_status_report = function create_client_defaulted_appointment_status_report(data) {





                                dTbles(".client_defaulted_appointment_status_report_div").empty();

                                var table = '<table id="client_defaulted_appointment_status_dashboard_table" class=" client_defaulted_appointment_status_dashboard_table dashboard_report dataTables_processing  table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">\n\
                                 <thead> <tr> <th>Name</th> <th> No :</th>  </tr> </thead>  \n\
                                   <tbody id="client_defaulted_appointment_status_results_tbody" class="client_defaulted_appointment_status_results_tbody"> </tbody> </table>';
                                dTbles('.client_defaulted_appointment_status_report_div').append(table);

                                dTbles.each(data, function (i, val) {
                                    var name = val.group_name;
                                    var value = val.total;

                                    var tr_results = "<tr>\n\
                                        <td>" + name + "</td>\n\
                                        <td>" + value + "</td>\n\</td>\n\
                                        </tr>";
                                    dTbles(".client_defaulted_appointment_status_results_tbody").append(tr_results);

                                });
                                dTbles(".client_defaulted_appointment_status_report_div").show();
                                dTbles('.client_defaulted_appointment_status_dashboard_table').DataTable({dom: 'Bfrtip',
                                    "searching": false,
                                    "bInfo": false,
                                    "bPaginate": false,
                                    buttons: [
                                        {
                                            extend: 'copyHtml5',
                                            exportOptions: {
                                                columns: ':contains("Office")'
                                            }
                                        },
                                        'excelHtml5',
                                        'csvHtml5',
                                        'pdfHtml5'
                                    ]});



                            }










                            dTbles(document).on('click', ".select_mfl_btn", function () {


                                //get data
                                var mfl_code = dTbles(this).closest('tr').find('input[name="hidden_mfl_code"]').val();
                                var controller = "admin";
                                var get_function = "get_facility_details";
                                var success_alert = "Facility added successfully ... :) ";
                                var error_alert = "An Error Ocurred";



                                dTbles.ajax({
                                    type: "GET",
                                    async: true,
                                    url: "<?php echo base_url(); ?>admin/get_facility_details/" + mfl_code,
                                    dataType: "JSON",
                                    success: function (response) {

                                        dTbles.each(response, function (i, value) {
                                            //dTbles(".add_facilty_div").hide();
                                            // dTbles(".table_div").show();
                                            dTbles(".facility_name").empty();

                                            dTbles(".mfl_code").empty();

                                            dTbles(".facility_type").empty();
                                            dTbles(".facility_county").empty();



                                            console.log("Facility Name : " + value.name + "MFL Code : " + value.code + "Facility Type : " + value.facility_type + "Location : " + value.owner);

                                            dTbles(".facility_name").val(value.name);

                                            dTbles(".mfl_code").val(value.code);

                                            dTbles(".facility_type").val(value.facility_type);

                                            dTbles(".facility_location").val(value.keph_level);
                                            dTbles(".facility_county").val(value.regulatory_body);


                                            dTbles(".add_facilty_div").show();
                                            dTbles(".table_div").hide();


                                        });


                                        $(".edit_div").show();
                                        $(".table_div").hide();


                                    }, error: function (data) {
                                        sweetAlert("Oops...", "" + error_alert + "", "error");

                                    }

                                });




                            });














                        });</script>



















                    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.css" />
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.js"></script>





                    <!--
                    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
                    
                    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
                    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>-->

                    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css" />
                    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.standalone.min.css" />
                    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker3.css" />



                    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
                    <script type="text/javascript">
                        var dtp = jQuery.noConflict();
                        dtp(document).ready(function () {
                            dtp('#123').datepicker({
                                format: "dd/mm/yyyy"
                            });
                        });













                        var date_picker = jQuery.noConflict();
                        date_picker(function () {
                            date_picker(".appointment_date").datepicker({
                                format: 'dd/mm/yyyy',
                                startDate: '-0d',
                                autoclose: true
                            });

                            date_picker(".transfer_appointment_date").datepicker({
                                format: 'dd/mm/yyyy',
                                startDate: '-0d',
                                autoclose: true
                            });

                            date_picker(".dob").datepicker({
                                format: 'dd/mm/yyyy',
                                endDate: '+0d',
                                autoclose: true
                            });

                            date_picker(".transfer_dob").datepicker({
                                format: 'dd/mm/yyyy',
                                endDate: '+0d',
                                autoclose: true
                            });


                            date_picker(".date").datepicker({
                                format: 'dd/mm/yyyy'
                            });

                            date_picker(".date_from").datepicker({
                                format: 'dd-mm-yyyy',
                                endDate: '+0d',
                                autoclose: true
                            });


                            date_picker(".date_to").datepicker({
                                format: 'dd-mm-yyyy',
                                endDate: '+0d',
                                autoclose: true
                            });

                        });</script>




                    <script type="text/javascript">
                        $(document).ready(function () {







                            setInterval(function () {
                                partner_info();
                                facility_info();
                                users_info();
                                content_info();
                                client_info();
                                ok_info();
                                not_ok_info();
                                deactivated_info();
                                appointments_info();
                                counties_info();
                                notified_info();
                                booked_info();
                                missed_info();
                                defaulted_info();
                                groups_info();
                                weekly_checkins_info();
                                responded_info();
                                sender_info();
                                pending_info();
                                late_info();
                                unrecognised_info();

                                tb_info();
                                pmtct_info();
                                adolescents_info();
                                new_clients_info();

                                art_info();
                                paeds_info();
                            }, 300000);
                            function deactivated_info() {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>reports/deactivated_info/",
                                    dataType: "json",
                                    success: function (response) {

                                        $(".deactivated_info").empty();
                                        $(".deactivated_info").append(response);
                                    }, error: function (data) {
                                        //sweetAlert("Oops...", "Something went wrong!", "error");
                                    }
                                })
                            }


                            function partner_info() {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>reports/partner_info/",
                                    dataType: "json",
                                    success: function (response) {
                                        $(".partner_info").empty();
                                        $(".partner_info").append(response);
                                    }, error: function (data) {
                                        //sweetAlert("Oops...", "Something went wrong!", "error");
                                    }
                                })
                            }


                            function facility_info() {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>reports/facility_info/",
                                    dataType: "json",
                                    success: function (response) {
                                        $(".facility_info").empty();
                                        $(".facility_info").append(response);
                                    }, error: function (data) {
                                        //  sweetAlert("Oops...", "Something went wrong!", "error");
                                    }
                                })
                            }

                            function users_info() {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>reports/users_info/",
                                    dataType: "json",
                                    success: function (response) {
                                        $(".users_info").empty();
                                        $(".users_info").append(response);
                                    }, error: function (data) {
                                        // sweetAlert("Oops...", "Something went wrong!", "error");
                                    }
                                })
                            }
                            function content_info() {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>reports/content_info/",
                                    dataType: "json",
                                    success: function (response) {
                                        $(".content_info").empty();
                                        $(".content_info").append(response);
                                    }, error: function (data) {
                                        // sweetAlert("Oops...", "Something went wrong!", "error");
                                    }
                                })
                            }

                            function client_info() {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>reports/client_info/",
                                    dataType: "json",
                                    success: function (response) {
                                        $(".client_info").empty();
                                        $(".client_info").append(response);
                                    }, error: function (data) {
                                        //  sweetAlert("Oops...", "Something went wrong!", "error");
                                    }
                                })
                            }


                            function module_info() {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>reports/module_info/",
                                    dataType: "json",
                                    success: function (response) {
                                        $(".module_info").empty();
                                        $(".module_info").append(response);
                                    }, error: function (data) {
                                        // sweetAlert("Oops...", "Something went wrong!", "error");
                                    }
                                })
                            }


                            function ok_info() {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>reports/ok_info/",
                                    dataType: "json",
                                    success: function (response) {
                                        $(".ok_info").empty();
                                        $(".ok_info").append(response);
                                    }, error: function (data) {
                                        // sweetAlert("Oops...", "Something went wrong!", "error");
                                    }
                                })
                            }


                            function not_ok_info() {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>reports/not_ok_info/",
                                    dataType: "json",
                                    success: function (response) {
                                        $(".not_ok_info").empty();
                                        $(".not_ok_info").append(response);
                                    }, error: function (data) {
                                        // sweetAlert("Oops...", "Something went wrong!", "error");
                                    }
                                })
                            }


                            function appointments_info() {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>reports/appointments_info/",
                                    dataType: "json",
                                    success: function (response) {
                                        $(".appointments_info").empty();
                                        $(".appointments_info").append(response);
                                    }, error: function (data) {
                                        //  sweetAlert("Oops...", "Something went wrong!", "error");
                                    }
                                })
                            }



                            function counties_info() {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>reports/counties_info/",
                                    dataType: "json",
                                    success: function (response) {
                                        $(".counties_info").empty();
                                        $(".counties_info").append(response);
                                    }, error: function (data) {
                                        //   sweetAlert("Oops...", "Something went wrong!", "error");
                                    }
                                })
                            }




                            function sub_counties_info() {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>reports/sub_counties_info/",
                                    dataType: "json",
                                    success: function (response) {
                                        $(".sub_counties_info").empty();
                                        $(".sub_counties_info").append(response);
                                    }, error: function (data) {
                                        //  sweetAlert("Oops...", "Something went wrong!", "error");
                                    }
                                })
                            }




                            function notified_info() {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>reports/notified_info/",
                                    dataType: "json",
                                    success: function (response) {
                                        $(".notified_info").empty();
                                        $(".notified_info").append(response);
                                    }, error: function (data) {
                                        //  sweetAlert("Oops...", "Something went wrong!", "error");
                                    }
                                })
                            }



                            function booked_info() {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>reports/booked_info/",
                                    dataType: "json",
                                    success: function (response) {
                                        $(".booked_info").empty();
                                        $(".booked_info").append(response);
                                    }, error: function (data) {
                                        //  sweetAlert("Oops...", "Something went wrong!", "error");
                                    }
                                })
                            }



                            function missed_info() {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>reports/missed_info/",
                                    dataType: "json",
                                    success: function (response) {
                                        $(".missed_info").empty();
                                        $(".missed_info").append(response);
                                    }, error: function (data) {
                                        //  sweetAlert("Oops...", "Something went wrong!", "error");
                                    }
                                })
                            }






                            function defaulted_info() {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>reports/defaulted_info/",
                                    dataType: "json",
                                    success: function (response) {
                                        $(".defaulted_info").empty();
                                        $(".defaulted_info").append(response);
                                    }, error: function (data) {
                                        //  sweetAlert("Oops...", "Something went wrong!", "error");
                                    }
                                })
                            }



                            function groups_info() {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>reports/groups_info/",
                                    dataType: "json",
                                    success: function (response) {
                                        $(".groups_info").empty();
                                        $(".groups_info").append(response);
                                    }, error: function (data) {
                                        //  sweetAlert("Oops...", "Something went wrong!", "error");
                                    }
                                })
                            }





                            function pmtct_info() {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>reports/pmtct_info/",
                                    dataType: "json",
                                    success: function (response) {
                                        $(".groups_info").empty();
                                        $(".groups_info").append(response);
                                    }, error: function (data) {
                                        //  sweetAlert("Oops...", "Something went wrong!", "error");
                                    }
                                })
                            }



                            function tb_info() {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>reports/tb_info/",
                                    dataType: "json",
                                    success: function (response) {
                                        $(".groups_info").empty();
                                        $(".groups_info").append(response);
                                    }, error: function (data) {
                                        //  sweetAlert("Oops...", "Something went wrong!", "error");
                                    }
                                })
                            }


                            function adolescents_info() {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>reports/adolescents_info/",
                                    dataType: "json",
                                    success: function (response) {
                                        $(".groups_info").empty();
                                        $(".groups_info").append(response);
                                    }, error: function (data) {
                                        //  sweetAlert("Oops...", "Something went wrong!", "error");
                                    }
                                })
                            }



                            function new_clients_info() {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>reports/new_clients_info/",
                                    dataType: "json",
                                    success: function (response) {
                                        $(".groups_info").empty();
                                        $(".groups_info").append(response);
                                    }, error: function (data) {
                                        //  sweetAlert("Oops...", "Something went wrong!", "error");
                                    }
                                })
                            }



                            function unsuppressed_info() {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>reports/unsuppressed_info/",
                                    dataType: "json",
                                    success: function (response) {
                                        $(".groups_info").empty();
                                        $(".groups_info").append(response);
                                    }, error: function (data) {
                                        //  sweetAlert("Oops...", "Something went wrong!", "error");
                                    }
                                })
                            }


                            function art_info() {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>reports/art_info/",
                                    dataType: "json",
                                    success: function (response) {
                                        $(".groups_info").empty();
                                        $(".groups_info").append(response);
                                    }, error: function (data) {
                                        //  sweetAlert("Oops...", "Something went wrong!", "error");
                                    }
                                })
                            }

                            function paeds_info() {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>reports/paeds_info/",
                                    dataType: "json",
                                    success: function (response) {
                                        $(".groups_info").empty();
                                        $(".groups_info").append(response);
                                    }, error: function (data) {
                                        //  sweetAlert("Oops...", "Something went wrong!", "error");
                                    }
                                })
                            }
                            function hiv_tb_info() {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>reports/hiv_tb_info/",
                                    dataType: "json",
                                    success: function (response) {
                                        $(".groups_info").empty();
                                        $(".groups_info").append(response);
                                    }, error: function (data) {
                                        //  sweetAlert("Oops...", "Something went wrong!", "error");
                                    }
                                })
                            }


                            function weekly_checkins_info() {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>reports/weekly_checkins_info/",
                                    dataType: "json",
                                    success: function (response) {
                                        $(".weekly_checkins_info").empty();
                                        $(".weekly_checkins_info").append(response);
                                    }, error: function (data) {
                                        //  sweetAlert("Oops...", "Something went wrong!", "error");
                                    }
                                })
                            }


                            function responded_info() {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>reports/responded_info/",
                                    dataType: "json",
                                    success: function (response) {
                                        $(".responded_info").empty();
                                        $(".responded_info").append(response);
                                    }, error: function (data) {
                                        //  sweetAlert("Oops...", "Something went wrong!", "error");
                                    }
                                })
                            }


                            function sender_info() {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>reports/sender_info/",
                                    dataType: "json",
                                    success: function (response) {
                                        $(".sender_info").empty();
                                        $(".sender_info").append(response);
                                    }, error: function (data) {
                                        //  sweetAlert("Oops...", "Something went wrong!", "error");
                                    }
                                })
                            }


                            function pending_info() {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>reports/pending_info/",
                                    dataType: "json",
                                    success: function (response) {
                                        $(".pending_info").empty();
                                        $(".pending_info").append(response);
                                    }, error: function (data) {
                                        //  sweetAlert("Oops...", "Something went wrong!", "error");
                                    }
                                })
                            }


                            function late_info() {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>reports/late_info/",
                                    dataType: "json",
                                    success: function (response) {
                                        $(".late_info").empty();
                                        $(".late_info").append(response);
                                    }, error: function (data) {
                                        //  sweetAlert("Oops...", "Something went wrong!", "error");
                                    }
                                })
                            }

                            function unrecognised_info() {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>reports/unrecognised_info/",
                                    dataType: "json",
                                    success: function (response) {
                                        $(".unrecognised_info").empty();
                                        $(".unrecognised_info").append(response);
                                    }, error: function (data) {
                                        //  sweetAlert("Oops...", "Something went wrong!", "error");
                                    }
                                })
                            }
                        });</script>







                    <script type="text/javascript">
                        var myVar;
                        function showTime() {
                            var d = new Date();
                            var t = d.toLocaleTimeString();
                            $("#clock").html("Current Time : " + d); // display time on the page
                            $(".current_time").html(" " + d); // display time on the page
                        }
                        function stopFunction() {
                            clearInterval(myVar); // stop the timer
                        }
                        $(document).ready(function () {
                            setInterval(function () {

                            }, 300000);
                            setInterval(function () {
                                window.location.reload(1);
                            }, 3000000);
                            setInterval('showTime()', 1000);
                            function sender() {
                                $(".sender_p").empty();
                                $.ajax({
                                    url: "<?php echo base_url(); ?>chore/sender",
                                    type: 'GET',
                                    dataType: 'JSON',
                                    success: function (data) {
                                        $(".sender_p").append("Sender is running");
                                    }, error: function (errorThrown) {
                                        $(".sender_p").append("Sender is not running");
                                    }
                                });
                            }

                            function scheduler() {
                                $(".scheduler_p").empty();
                                $.ajax({
                                    url: "<?php echo base_url(); ?>chore/scheduler",
                                    type: 'GET',
                                    dataType: 'JSON',
                                    success: function (data) {
                                        $(".scheduler_p").append("Scheduler is running");
                                    }, error: function (errorThrown) {
                                        $(".scheduler_p").append("Scheduler is not running");
                                    }
                                });
                            }

                            function receiver() {
                                $(".receiver_p").empty();
                                $.ajax({
                                    url: "<?php echo base_url(); ?>chore/receiver",
                                    type: 'GET',
                                    dataType: 'JSON',
                                    success: function (data) {
                                        $(".receiver_p").append("Receiver is running");
                                    }, error: function (errorThrown) {
                                        $(".receiver_p").append("Receiver is not running");
                                    }
                                });
                            }


                            function trigger() {
                                $(".trigger_p").empty();
                                $.ajax({
                                    url: "<?php echo base_url(); ?>chore/trigger",
                                    type: 'GET',
                                    dataType: 'JSON',
                                    success: function (data) {
                                        $(".trigger_p").append("Trigger is running");
                                    }, error: function (errorThrown) {
                                        $(".trigger_p").append("Trigger is not running");
                                    }
                                });
                            }

                            function send_mail() {
                                $(".send_mail_p").empty();
                                $.ajax({
                                    url: "<?php echo base_url(); ?>chore/send_mail",
                                    type: 'GET',
                                    dataType: 'JSON',
                                    success: function (data) {
                                        $(".send_mail_p").append("Send mail is running");
                                    }, error: function (errorThrown) {
                                        $(".send_mail_p").append("Send mail is not running");
                                    }
                                });
                            }

                            function broadcast() {
                                $(".broadcast_p").empty();
                                $.ajax({
                                    url: "<?php echo base_url(); ?>chore/broadcast",
                                    type: 'GET',
                                    dataType: 'JSON',
                                    success: function (data) {
                                        $(".broadcast_p").append("Broadcast is running");
                                    }, error: function (errorThrown) {
                                        $(".broadcast_p").append("Broadcast is not running");
                                    }
                                });
                            }

                        });</script>






                    <div class="loading" id="loading" >
                        <div class="panel-bod">
                            <i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>
                            <span class="sr-only">Loading...</span>
                        </div>

                        <!-- Place at bottom of page --></div>
                    <style type="text/css">
                        /* Start by setting display:none to make this hidden.
                    Then we position it in relation to the viewport window
                    with position:fixed. Width, height, top and left speak
                    for themselves. Background we set to 80% white with
                    our animation centered, and no-repeating */
                        .loader {
                            display:    inline;
                            position:   fixed;
                            z-index:    1000;
                            top:        0;
                            left:       0;
                            height:     100%;
                            width:      100%;

                            background: rgba( 255, 255, 255, .8 ) 
                                50% 50% 
                                no-repeat;
                        }
                    </style>

                    <link rel="stylesheet" href="http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
                    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
                    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>





                    <script src="//code.jquery.com/jquery-2.1.4.min.js"></script>
                    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
                    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
                    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>





                    <!-- javascripts -->
                    <script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
                    <script src="<?php echo base_url(); ?>assets/js/jquery-1.8.3.min.js"></script>
                    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-ui-1.9.2.custom.min.js"></script>
                    <!-- bootstrap -->
                    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
                    <!-- nice scroll -->
                    <script src="<?php echo base_url() ?>assets/js/jquery.scrollTo.min.js"></script>
                    <script src="<?php echo base_url(); ?>assets/js/jquery.nicescroll.js" type="text/javascript"></script>
                    <!-- custom select -->
                    <script src="<?php echo base_url(); ?>assets/js/jquery.customSelect.min.js" ></script>
                    <!--custome script for all page-->
                    <script src="<?php echo base_url(); ?>assets/js/scripts.js"></script>



                    <script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-Knob/1.2.13/jquery.knob.min.js"></script>


                    <script>
                        $(function () {
                            $(".county_performance").knob();
                            $(".facility_performance").knob();
                            $(".client_performance").knob();

                        });

                        function county_performance() {
                            $.ajax({
                                url: "<?php echo base_url() ?>reports/county_target",
                                type: 'GAT',
                                dataType: 'JSON',
                                success: function (data) {
                                    var data = data;

                                    $(".county_performance").val(data);

                                }, error: function (error) {
                                    console.log(error);
                                }
                            });
                        }
                        function facility_performance() {
                            $.ajax({
                                url: "<?php echo base_url() ?>reports/facility_target",
                                type: 'GAT',
                                dataType: 'JSON',
                                success: function (data) {
                                    var data = data;

                                    $(".client_performance").val(data);

                                }, error: function (error) {
                                    console.log(error);
                                }
                            });
                        }
                    </script>



                    <!-- END CHART STYLES    -->


                </section>
            </section>
            <!--main content end-->


        </section>



    </body>
    <!-- END BODY-->

</html>
