<!--END BLOCK SECTION -->
<hr />
<!-- COMMENT AND NOTIFICATION  SECTION -->
<div class="row" id="data">

    <div class="col-lg-12">


        <div class="panel panel-primary" id="main_clinician">

            <div class="panel-heading"> 
                Audit Trails
            </div>   
            <div >


                <div class="panel-body"> 


                    <div class="table_div" id="table_div">

                        <!-- Content Wrapper. Contains page content -->
                        <div class="content-wrapper">
                            <!-- Content Header (Page header) -->
                            <section class="content-header">
                                <h1>

                                    <small></small>
                                </h1>
                                <ol class="breadcrumb">
                                    <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
                                    <li><a href="<?php echo base_url(); ?>support/audit_tail">Search Audit Trails</a></li>
                                </ol>
                            </section>

                            <!-- Main content -->
                            <section class="content">
                                <div class="row">
                                    <div class="donation_search_div" id="donation_search_div">
                                        <div class="col-md-12">
                                            <!-- general form elements -->
                                            <div class="box box-primary">
                                                <div class="box-header with-border">
                                                    <!--   --> <h3 class="box-title">System Logs  Search</h3>
                                                </div><!-- /.box-header -->
                                                <!-- Donation search Form  -->
                                                <form role="form" id="audit_trail_search_form" class="audit_trail_search_form"> </form>
                                                <div class="box-body">

                                                    <div class="checkbox">
                                                        <label>Search By: Phone No
                                                            <input type="radio" class="flat-red search_option" value="Phone No " name="audit_trail_search_option" checked="checked">
                                                        </label>


                                                    </div>

                                                    <div class="search_input_div" id="search_input_div" style="display: inline;">
                                                        <div class="form-group">
                                                            <label for="GG"></label>
                                                            <input type="text" class="form-control audit_trail_search_value" id="audit_trail_search_value" name="audit_trail_search_value"  placeholder="Enter Phone Number ...">
                                                        </div>
                                                        <button class="btn btn-small audit_trail_serach_btn "><i class="icon-search"></i>Search</button>
                                                    </div>


                                                    <div class="audit_trail_search_results_div" id="audit_trail_search_results_div" style="display: inline;">

                                                    </div>


                                                </div><!-- /.box-body -->

                                                <div class="box-footer">
                                                </div>




                                            </div><!-- /.box -->



                                            <!-- Form Element sizes -->


                                        </div><!--/.col (left) -->
                                    </div>





                                </div><!--/.col (right) -->       

                            </section>
                        </div><!-- /.box -->
                        <!-- general form elements disabled -->

                    </div>















































                </div>
            </div>                <div class="panel-footer">
                Get   in touch: support.tech@mhealthkenya.org                             </div>

        </div>        












    </div>



</div>
</div>
<!-- END COMMENT AND NOTIFICATION  SECTION -->

</div>








<script type="text/javascript">
    $(document).ready(function () {
        $(document).on('click', ".add_audit_trail", function () {

            $(".audit_trail_name").empty();

            $(".code").empty();

            $(".audit_trail_type").empty();

            $(".location").empty();

            //get data
            var audit_trail_name = $(this).closest('tr').find('input[name="audit_trail_name"]').val();

            var mfl_code = $(this).closest('tr').find('input[name="code"]').val();

            var audit_trail_type = $(this).closest('tr').find('input[name="audit_trail_type"]').val();

            var location = $(this).closest('tr').find('input[name="sub_county_name"]').val();

            var county_name = $(this).closest('tr').find('input[name="county_name"]').val();





            $(".audit_trail_name").val(audit_trail_name);

            $(".mfl_code").val(mfl_code);

            $(".audit_trail_type").val(audit_trail_type);

            $(".audit_trail_location").val(location);
            $(".audit_trail_county").val(county_name);


            $(".add_facilty_div").show();
            $(".table_div").hide();

            //dTbles('#myModal2').modal('show');
            //jQuery("#addFacilityModal").modal("show");





        });





        $(".close_add_audit_trail_btn").click(function () {
            $(".add_facilty_div").hide();
            $(".table_div").show();
            $(".audit_trail_name").empty();

            $(".mfl_code").empty();

            $(".audit_trail_type").empty();
            $(".audit_trail_county").empty();

        });



        $(".submit_audit_trail").click(function () {
            var controller = "admin";
            var submit_function = "assign_partner_audit_trail";
            var form_class = "add_audit_trail_form";
            var success_alert = "Facility added successfully ... :) ";
            var error_alert = "An Error Ocurred";
            submit_data(controller, submit_function, form_class, success_alert, error_alert);
        });






    });
</script>















<!--END MAIN WRAPPER -->