<?php

use Ghunti\HighchartsPHP\Highchart;
use Ghunti\HighchartsPHP\HighchartJsExpr;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class MY_Controller extends CI_Controller {

    public $hc;

    function __construct() {
        parent::__construct();
    }

    function check_authorization($function_name) {
        $user_id = $this->session->userdata('user_id');
        $check_acess = $this->db->query("Select * from tbl_module inner join tbl_user_permission on tbl_user_permission.module_id = tbl_module.id where tbl_module.status='Active' and tbl_user_permission.user_id='$user_id' and tbl_module.function='$function_name'")->num_rows();
        if ($check_acess > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function get_access_level() {
        $access_level = $this->session->userdata('access_level');
        $facility_id = $this->session->userdata('facility_id');
        $partner_id = $this->session->userdata('partner_id');
        $donor_id = $this->session->userdata('donor_id');
        $county_id = $this->session->userdata('county_id');
        $subcounty_id = $this->session->userdata('subcounty_id');

        if ($access_level == "Donor") {
            $query = "Select name from tbl_donor where id='$donor_id'";
        } elseif ($access_level == "Partner") {
            $query = "Select name from tbl_partner where id='$partner_id'";
        } elseif ($access_level == "Facility") {

            $query = "Select name,code from tbl_master_facility where code='$facility_id'";
        } elseif ($access_level == "Admin") {

            $query = "Select name from tbl_partner where id = '$partner_id'";
        } elseif ($access_level == "County") {

            $query = "Select name from tbl_county where id = '$county_id'";
        } elseif ($access_level == "Sub County") {

            $query = "Select name from tbl_sub_county where id = '$subcounty_id'";
        }
        $results = $this->db->query($query)->result();

        return $results;
    }

    function get_county_filtered_values() {
        $access_level = $this->session->userdata('access_level');
        $facility_id = $this->session->userdata('facility_id');
        $partner_id = $this->session->userdata('partner_id');
        $donor_id = $this->session->userdata('donor_id');
        $county_id = $this->session->userdata('county_id');
        $sub_county_id = $this->session->userdata('sub_county_id');
        if ($access_level == "Donor") {
            $query = "SELECT tbl_county.name as county_name , tbl_county.id as county_id FROM tbl_county INNER JOIN tbl_partner_facility ON tbl_partner_facility.`county_id` = tbl_county.`id` GROUP BY tbl_county.id ";
        } elseif ($access_level == "Partner") {
            $query = "SELECT tbl_county.name as county_name , tbl_county.id as county_id FROM tbl_county INNER JOIN tbl_partner_facility ON tbl_partner_facility.`county_id` = tbl_county.`id` where tbl_partner_facility.partner_id='$partner_id' GROUP BY tbl_county.id ";
        } elseif ($access_level == "County") {


            $query = "SELECT tbl_county.name as county_name , tbl_county.id as county_id FROM tbl_county "
                    . " INNER JOIN tbl_partner_facility ON tbl_partner_facility.`county_id` = tbl_county.`id` "
                    . " where tbl_partner_facility.county_id='$county_id' GROUP BY tbl_county.id ";
        } elseif ($access_level == "Sub County") {


            $query = "SELECT tbl_county.name as county_name , tbl_county.id as county_id FROM tbl_county "
                    . " INNER JOIN tbl_partner_facility ON tbl_partner_facility.`county_id` = tbl_county.`id` "
                    . " where tbl_partner_facility.county_id='$county_id' AND tbl_partner_facility.sub_county_id='$sub_county_id' GROUP BY tbl_county.id ";
        } elseif ($access_level == "Facility") {


            $query = "SELECT tbl_county.name as county_name , tbl_county.id as county_id FROM tbl_county INNER JOIN tbl_partner_facility ON tbl_partner_facility.`county_id` = tbl_county.`id` where tbl_partner_facility.partner_id='$partner_id' GROUP BY tbl_county.id ";
        } else {
            $query = "SELECT tbl_county.name as county_name , tbl_county.id as county_id FROM tbl_county INNER JOIN tbl_partner_facility ON tbl_partner_facility.`county_id` = tbl_county.`id` GROUP BY tbl_county.id ";
        }
        $results = $this->db->query($query)->result();

        return $results;
    }

    function load_scripts() {
        $script = new Highchart();
        return $script->printScripts(TRUE);
    }

     


function send_message($source, $destination, $msg, $outgoing_id) {
        $this->load->library('africastalking');
        //Africa's Talking Library.
        // require_once('AfricasTalkingGateway.php');
        //Africa's Talking API key and username.
        $username = 'mhealthkenya';
        $apikey = '9318d173cb9841f09c73bdd117b3c7ce3e6d1fd559d3ca5f547ff2608b6f3212';

        //Shortcode.

        $gateway = new AfricasTalkingGateway($username, $apikey);
        try {
            $results = $gateway->sendMessage($destination, $msg, $source);

            print_r($results);
            foreach ($results as $result) {
                $number = $result->number;
                $status = $result->status;
                $messageid = $result->messageId;
                $cost = $result->cost;
                $statusCode = $result->statusCode;


            }
            return TRUE;
        } catch (GatewayException $e) {
            echo "Oops an error encountered while sending: " . $e->getMessage();
            return FALSE;
        }
    }





    function check_username($username) {
        return $this->db->get_where('users', array('username' => $username))->result_array();
    }

    function check_email($e_mail) {
        return $this->db->get_where('users', array('e_mail' => $e_mail));
    }

    function check_phoneno($phoneno) {
        return $this->db->get_where('users', array('phone_no' => $phoneno));
    }

}
