<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * CodeIgniter Infobip SMS endpoint Class
 *
 * This library will help import retrieve southwell SMS based on your user credentials  
 * 
 * This library treats the first row of a CSV file
 * as a column header row.
 * 
 *
 * @package         CodeIgniter
 * @subpackage      Libraries
 * @category        Libraries
 * @author          Dindi Harris
 */
class SMS {

    protected $CI;

    // We'll use a constructor, as you can't directly call a function
    // from a property definition.
    public function __construct() {
        // Assign the CodeIgniter super-object
        $this->CI = & get_instance();
    }

    public function send_message($source, $destination, $msg) {
        error_reporting(E_ERROR | E_PARSE);


        $senderid = $source;

        if ($destination <> '') {

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_PORT => "3001",
                CURLOPT_URL => "http://197.248.10.20:3001/api/senders/sender",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_HTTPHEADER => array(
                    "cache-control: no-cache",
                    "destination: $destination",
                    "msg: $msg",
                    "source: $senderid"
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                return FALSE;
            } else {
                return TRUE;
            }
        } else {
            $out_put = " Reason: Phone number is missing";
            return $out_put;
        }
    }

}
