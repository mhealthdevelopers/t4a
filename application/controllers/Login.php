<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Login extends CI_Controller {

    public $data = '';

    function __construct() {
        parent::__construct();
        $this->data = new DBCentral();
        $this->load->library('session');
        $this->check_access();
    }

    function index() {

        $view = 'Login';
        $this->load->view($view);
    }

    function check_access() {
        $logged_in = $this->session->userdata("logged_in");

        if ($logged_in) {
            $first_access = $this->session->userdata('first_access');
            $user_id = $this->session->userdata('user_id');
            if ($first_access == "Yes") {
                redirect("reset/reset_pass/$user_id", "refresh");
            } else {
                redirect("Reports/dashboard", "refresh");
            }
        } else {
            
        }
    }

    function check_auth() {

        $validate_user = $this->data->check_auth();
        $login_success = "Login success";
        $invalid_user = "User does not exist";
        $wrong_password = "Wrong password";
        if ($validate_user === $login_success) {
            $status = $this->check_last_pass_reset();
            if ($status == "Reset") {
                echo "Pass Exp";
            } elseif ($status == "Proceed") {
                echo "Login Success";
            }
        } elseif ($validate_user === $invalid_user) {
            echo "User does not exist";
        } elseif ($validate_user === $wrong_password) {
            echo "Wrong Password";
        }
    }

    function reset() {
        $this->load->view('reset');
    }

    function reset_password() {
        $password = $this->input->post('password');
        $password2 = $this->input->post('password2');
        if ($password == $password2) {
            $this->data->reset_password($password);
        } else {
            echo 'Password Mismatch';
        }
    }

    function Logout() {
        $this->session->sess_destroy();
        $this->index();
    }

    function new_login() {
        $this->load->view('New_Login');
    }

    function check_last_pass_reset() {
        $username = $this->input->post('username');

        if (filter_var($username, FILTER_VALIDATE_EMAIL)) {

            $check_existence = $this->db->get_where('users', array('e_mail' => $username, 'status' => 'Active'))->num_rows();
            $get_user_pass = $this->db->get_where('users', array('e_mail' => $username, 'status' => 'Active'))->result_array();
        } else {

            $check_existence = $this->db->get_where('users', array('phone_no' => $username, 'status' => 'Active'))->num_rows();
            $get_user_pass = $this->db->get_where('users', array('phone_no' => $username, 'status' => 'Active'))->result_array();
        }
        if ($check_existence > 0) {
            foreach ($get_user_pass as $value) {
                $last_pass_change = $value['last_pass_change'];
                $user_id = $value['id'];
                if (empty($last_pass_change)) {
                    $this->db->trans_start();
                    $data_update = array(
                        'first_access' => 'Yes'
                    );
                    $this->db->where('id', $user_id);
                    $this->db->update('users', $data_update);
                    $this->db->trans_complete();
                    if ($this->db->trans_status() === FALSE) {
                        
                    } else {
                        //Reset Password 
                        return "Reset";
                    }
                } else {
                    $current_date = date("Y-m-d");
                    $current_date = date_create($current_date);
                    $last_pass_change = date_create($last_pass_change);
                    $date_diff = date_diff($last_pass_change, $current_date);

                    $diff = $date_diff->format("%R%a days");
                    $diff = substr($diff, 1, 2);
                    if ($diff >= 30) {
                        $this->db->trans_start();
                        $data_update = array(
                            'first_access' => 'Yes'
                        );
                        $this->db->where('id', $user_id);
                        $this->db->update('users', $data_update);
                        $this->db->trans_complete();
                        if ($this->db->trans_status() === FALSE) {
                            
                        } else {
                            //Reset Password
                            return "Reset";
                        }
                    } elseif ($diff <= 30) {
                        return "Proceed";
                    }
                }
            }
        }
    }

}
