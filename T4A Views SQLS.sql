
CREATE   VIEW `all_system_msgs` AS 
SELECT
  `tbl_outgoing`.`id`              AS `id`,
  `tbl_outgoing`.`message_type_id` AS `message_type_id`,
  `tbl_outgoing`.`recepient_type`  AS `recepient_type`,
  `tbl_message_types`.`name`       AS `name`,
  DATE_FORMAT(`tbl_outgoing`.`date_added`,'%M %Y') AS `month_year`,
  `tbl_outgoing`.`date_added`      AS `date_added`
FROM (`tbl_outgoing`
   JOIN `tbl_message_types`
     ON ((`tbl_message_types`.`id` = `tbl_outgoing`.`message_type_id`)))
ORDER BY `tbl_outgoing`.`date_added`;



CREATE OR REPLACE   VIEW `unscheduled_refills` AS 
SELECT
  `tbl_partner_facility`.`partner_id`    AS `partner_id`,
  `tbl_partner_facility`.`mfl_code`      AS `mfl_code`,
  `tbl_partner_facility`.`county_id`     AS `county_id`,
  `tbl_partner_facility`.`sub_county_id` AS `sub_county_id`,
  COUNT(`tbl_appointment`.`id`)          AS `no_of_appointments`,
  DATE_FORMAT(`tbl_appointment`.`appntmnt_date`,'%M %Y') AS `appointment_month`
FROM ((`tbl_appointment`
    JOIN `tbl_client`
      ON ((`tbl_client`.`id` = `tbl_appointment`.`client_id`)))
   JOIN `tbl_partner_facility`
     ON ((`tbl_partner_facility`.`mfl_code` = `tbl_client`.`mfl_code`)))
WHERE ((`tbl_appointment`.`visit_type` = 'Un-Scheduled')
       AND (`tbl_appointment`.`app_type_1` = 'Re-Fill'))
GROUP BY `tbl_partner_facility`.`mfl_code`,DATE_FORMAT(`tbl_appointment`.`appntmnt_date`,'%M %Y') ;




CREATE OR REPLACE VIEW `missed_clinical_appointments` AS 
SELECT
  `tbl_appointment`.`app_status`         AS `app_status`,
  `tbl_appointment`.`app_type_1`         AS `app_type_1`,
  `tbl_partner_facility`.`partner_id`    AS `partner_id`,
  `tbl_partner_facility`.`mfl_code`      AS `mfl_code`,
  `tbl_partner_facility`.`county_id`     AS `county_id`,
  `tbl_partner_facility`.`sub_county_id` AS `sub_county_id`,
  COUNT(`tbl_appointment`.`id`)          AS `no_of_appointments`,
  DATE_FORMAT(`tbl_appointment`.`appntmnt_date`,'%M %Y') AS `appointment_month`
FROM ((`tbl_appointment`
    JOIN `tbl_client`
      ON ((`tbl_client`.`id` = `tbl_appointment`.`client_id`)))
   JOIN `tbl_partner_facility`
     ON ((`tbl_partner_facility`.`mfl_code` = `tbl_client`.`mfl_code`)))
WHERE ((`tbl_appointment`.`visit_type` = 'Scheduled')
       AND (`tbl_appointment`.`app_status` IN('Missed','Defaulted','LTFU'))
       AND (`tbl_appointment`.`app_type_1` = 'Clinical Review')
       AND (`tbl_appointment`.`appntmnt_date` <= CURDATE()))
GROUP BY `tbl_partner_facility`.`mfl_code`,DATE_FORMAT(`tbl_appointment`.`appntmnt_date`,'%M %Y') ;





CREATE OR REPLACE VIEW `vw_client_app_report` AS 
SELECT
  `tbl_client`.`clinic_number`        AS `clinic_number`,
  `tbl_appointment`.`app_type_1`      AS `app_type_1`,
  `tbl_appointment`.`active_app`      AS `active_app`,
  `tbl_appointment`.`appntmnt_date`   AS `appntmnt_date`,
  DATE_FORMAT(`tbl_appointment`.`appntmnt_date`,'%M %Y') AS `app_month_year`,
  `tbl_appointment`.`app_status`      AS `app_status`,
  `tbl_client`.`mfl_code`             AS `mfl_code`,
  `tbl_master_facility`.`name`        AS `facility_name`,
  `tbl_gender`.`name`                 AS `gender`,
  `tbl_groups`.`name`                 AS `group_name`,
  `tbl_marital_status`.`marital`      AS `marital`,
  `tbl_partner_facility`.`partner_id` AS `partner_id`,
  `tbl_partner`.`name`                AS `partner_name`,
  `tbl_client`.`date_added`           AS `date_added`
FROM (((((((`tbl_client`
         JOIN `tbl_partner_facility`
           ON ((`tbl_partner_facility`.`mfl_code` = `tbl_client`.`mfl_code`)))
        JOIN `tbl_master_facility`
          ON ((`tbl_master_facility`.`code` = `tbl_partner_facility`.`mfl_code`)))
       JOIN `tbl_groups`
         ON ((`tbl_groups`.`id` = `tbl_client`.`group_id`)))
      JOIN `tbl_gender`
        ON ((`tbl_gender`.`id` = `tbl_client`.`gender`)))
     JOIN `tbl_marital_status`
       ON ((`tbl_marital_status`.`id` = `tbl_client`.`marital`)))
    JOIN `tbl_partner`
      ON ((`tbl_partner`.`id` = `tbl_partner_facility`.`partner_id`)))
   JOIN `tbl_appointment`
     ON ((`tbl_appointment`.`client_id` = `tbl_client`.`id`)))
WHERE (`tbl_appointment`.`appntmnt_date` > '1970-01-01')
ORDER BY `tbl_appointment`.`appntmnt_date` ;

CREATE OR REPLACE VIEW `vw_client_msgs` AS 
SELECT
  `tbl_outgoing`.`id`              AS `id`,
  `tbl_outgoing`.`date_added`      AS `date_added`,
  `tbl_message_types`.`name`       AS `name`,
  `tbl_outgoing`.`status`          AS `status`,
  `tbl_outgoing`.`message_type_id` AS `message_type_id`,
  `tbl_outgoing`.`recepient_type`  AS `recepient_type`
FROM ((`tbl_outgoing`
    JOIN `tbl_client`
      ON ((`tbl_client`.`id` = `tbl_outgoing`.`clnt_usr_id`)))
   JOIN `tbl_message_types`
     ON ((`tbl_message_types`.`id` = `tbl_outgoing`.`message_type_id`)))
WHERE (`tbl_outgoing`.`recepient_type` = 'Client') ;


CREATE OR REPLACE VIEW `vw_client_report` AS 
SELECT
  `tbl_client`.`clinic_number`        AS `clinic_number`,
  `tbl_client`.`mfl_code`             AS `mfl_code`,
  `tbl_master_facility`.`name`        AS `facility_name`,
  `tbl_gender`.`name`                 AS `gender`,
  `tbl_groups`.`name`                 AS `group_name`,
  `tbl_marital_status`.`marital`      AS `marital`,
  `tbl_partner_facility`.`partner_id` AS `partner_id`,
  `tbl_partner`.`name`                AS `partner_name`,
  `tbl_client`.`date_added`           AS `date_added`,
  DATE_FORMAT(`tbl_client`.`date_added`,'%M %Y') AS `month_year`
FROM ((((((`tbl_client`
        JOIN `tbl_partner_facility`
          ON ((`tbl_partner_facility`.`mfl_code` = `tbl_client`.`mfl_code`)))
       JOIN `tbl_master_facility`
         ON ((`tbl_master_facility`.`code` = `tbl_partner_facility`.`mfl_code`)))
      JOIN `tbl_groups`
        ON ((`tbl_groups`.`id` = `tbl_client`.`group_id`)))
     JOIN `tbl_gender`
       ON ((`tbl_gender`.`id` = `tbl_client`.`gender`)))
    JOIN `tbl_marital_status`
      ON ((`tbl_marital_status`.`id` = `tbl_client`.`marital`)))
   JOIN `tbl_partner`
     ON ((`tbl_partner`.`id` = `tbl_partner_facility`.`partner_id`)))
GROUP BY `tbl_client`.`id`
ORDER BY `tbl_client`.`date_added` ;


CREATE OR REPLACE VIEW `vw_client_summary_report` AS 
SELECT
  `tbl_county`.`code`          AS `county_code`,
  `tbl_county`.`name`          AS `county`,
  `tbl_sub_county`.`name`      AS `sub_county`,
  `tbl_master_facility`.`code` AS `mfl_code`,
  `tbl_master_facility`.`name` AS `facility`,
  `tbl_partner`.`name`         AS `partner`,
  COUNT(`tbl_client`.`id`)     AS `no_of_clients`,
  COUNT(`tbl_appointment`.`id`) AS `no_of_appointments`
FROM ((((((`tbl_client`
        JOIN `tbl_partner_facility`
          ON ((`tbl_partner_facility`.`mfl_code` = `tbl_client`.`mfl_code`)))
       JOIN `tbl_sub_county`
         ON ((`tbl_sub_county`.`id` = `tbl_partner_facility`.`sub_county_id`)))
      JOIN `tbl_county`
        ON ((`tbl_county`.`id` = `tbl_partner_facility`.`county_id`)))
     JOIN `tbl_master_facility`
       ON ((`tbl_master_facility`.`code` = `tbl_partner_facility`.`mfl_code`)))
    JOIN `tbl_partner`
      ON ((`tbl_partner`.`id` = `tbl_partner_facility`.`partner_id`)))
   LEFT JOIN `tbl_appointment`
     ON ((`tbl_appointment`.`client_id` = `tbl_client`.`id`)))
GROUP BY `tbl_partner_facility`.`mfl_code`
ORDER BY `tbl_county`.`code`;



CREATE OR REPLACE VIEW `vw_clinic_bkd_attended` AS 
SELECT
  `tbl_appointment`.`app_type_1`         AS `app_type_1`,
  `tbl_partner_facility`.`partner_id`    AS `partner_id`,
  `tbl_partner_facility`.`mfl_code`      AS `mfl_code`,
  `tbl_partner_facility`.`county_id`     AS `county_id`,
  `tbl_partner_facility`.`sub_county_id` AS `sub_county_id`,
  COUNT(`tbl_appointment`.`id`)          AS `no_of_appointments`,
  DATE_FORMAT(`tbl_appointment`.`appntmnt_date`,'%M %Y') AS `appointment_month`
FROM ((`tbl_appointment`
    JOIN `tbl_client`
      ON ((`tbl_client`.`id` = `tbl_appointment`.`client_id`)))
   JOIN `tbl_partner_facility`
     ON ((`tbl_partner_facility`.`mfl_code` = `tbl_client`.`mfl_code`)))
WHERE ((`tbl_appointment`.`visit_type` = 'Scheduled')
       AND (`tbl_appointment`.`appointment_kept` = 'Yes')
       AND (`tbl_appointment`.`active_app` = '0')
       AND (`tbl_appointment`.`app_type_1` = 'Clinical Review')
       AND (`tbl_appointment`.`appntmnt_date` <= CURDATE()))
GROUP BY `tbl_partner_facility`.`mfl_code`,DATE_FORMAT(`tbl_appointment`.`appntmnt_date`,'%M %Y') ;


CREATE OR REPLACE VIEW `vw_defaulter_bookings_visits` AS 
SELECT
  `a`.`id`              AS `client_id`,
  CONCAT(`a`.`f_name`,' ',`a`.`m_name`,' ',`a`.`l_name`) AS `client_name`,
  `a`.`clinic_number`   AS `clinic_number`,
  `a`.`status`          AS `status`,
  `d`.`expln_app`       AS `expln_app`,
  `c`.`name`            AS `gender`,
  TIMESTAMPDIFF(YEAR,`a`.`dob`,CURDATE()) AS `age`,
  `a`.`dob`             AS `dob`,
  CURDATE()             AS `today`,
  `b`.`appntmnt_date`   AS `missed_appointment_date`,
  `b`.`app_type_1`      AS `missed_appointment_type`,
  `b`.`fnl_outcome_dte` AS `fnl_outcome_dte`,
  `b`.`active_app`      AS `active_app`,
  `d`.`appntmnt_date`   AS `next_re_fill_appointment`,
  `d`.`app_type_1`      AS `next_refill_app`,
  `a`.`mfl_code`        AS `mfl_code`,
  `e`.`partner_id`      AS `partner_id`,
  `e`.`county_id`       AS `county_id`,
  `e`.`sub_county_id`   AS `sub_county_id`
FROM ((((`t4a_chs`.`tbl_client` `a`
      JOIN `t4a_chs`.`tbl_appointment` `b`
        ON ((`b`.`client_id` = `a`.`id`)))
     JOIN `t4a_chs`.`tbl_gender` `c`
       ON ((`c`.`id` = `a`.`gender`)))
    JOIN `t4a_chs`.`tbl_partner_facility` `e`
      ON ((`e`.`mfl_code` = `a`.`mfl_code`)))
   LEFT JOIN `t4a_chs`.`tbl_appointment` `d`
     ON ((`d`.`client_id` = `a`.`id`)))
WHERE ((`b`.`active_app` = '0')
       AND (`d`.`active_app` = '1')
       AND (`d`.`app_type_1` = 'Clinical Review')
       AND (`d`.`appntmnt_date` > CURDATE())
       AND (`d`.`client_id` = `b`.`client_id`));
       
       
CREATE OR REPLACE VIEW `vw_defaulter_tracing_details` AS 
SELECT
  `a`.`id`             AS `client_id`,
  CONCAT(`a`.`f_name`,' ',`a`.`m_name`,' ',`a`.`l_name`) AS `client_name`,
  `a`.`clinic_number`  AS `clinic_number`,
  `c`.`name`           AS `gender`,
  DATE_FORMAT(`a`.`art_date`,'%D %M %Y') AS `art_date`,
  TIMESTAMPDIFF(YEAR,`a`.`dob`,CURDATE()) AS `age`,
  DATE_FORMAT(`a`.`enrollment_date`,'%D %M %Y') AS `enrollment_date`,
  DATE_FORMAT(`a`.`art_date`,'%M') AS `ART_COHORT_MONTH`,
  DATE_FORMAT(`a`.`dob`,'%D %M %Y') AS `dob`,
  DATE_FORMAT(CURDATE(),'%D %M %Y') AS `today`,
  DATE_FORMAT(`b`.`appntmnt_date`,'%D %M %Y') AS `missed_appointment_date`,
  `a`.`phone_no`       AS `client_phone`,
  `a`.`shared_no_name` AS `trtmnt_supprtr_name`,
  `a`.`alt_phone_no`   AS `trtmnt_sprtr_phone_no`,
  `a`.`mfl_code`       AS `mfl_code`,
  `d`.`county_id`      AS `county_id`,
  `d`.`sub_county_id`  AS `sub_county_id`
FROM (((`tbl_client` `a`
     JOIN `tbl_appointment` `b`
       ON ((`b`.`client_id` = `a`.`id`)))
    JOIN `tbl_gender` `c`
      ON ((`c`.`id` = `a`.`gender`)))
   JOIN `tbl_partner_facility` `d`
     ON ((`d`.`mfl_code` = `a`.`mfl_code`)))
WHERE ((`b`.`app_status` = 'Missed')
        OR (`b`.`app_status` = 'Defaulted')
        OR ((`b`.`app_status` = 'LTFU')
            AND (`b`.`active_app` = '1'))) ;
            
            
            
CREATE OR REPLACE VIEW `vw_missed_appointments` AS 
SELECT
  `tbl_partner_facility`.`partner_id`    AS `partner_id`,
  `tbl_partner_facility`.`mfl_code`      AS `mfl_code`,
  `tbl_partner_facility`.`county_id`     AS `county_id`,
  `tbl_partner_facility`.`sub_county_id` AS `sub_county_id`,
  COUNT(`tbl_appointment`.`id`)          AS `no_of_appointments`,
  DATE_FORMAT(`tbl_appointment`.`appntmnt_date`,'%M %Y') AS `appointment_month`
FROM ((`tbl_appointment`
    JOIN `tbl_client`
      ON ((`tbl_client`.`id` = `tbl_appointment`.`client_id`)))
   JOIN `tbl_partner_facility`
     ON ((`tbl_partner_facility`.`mfl_code` = `tbl_client`.`mfl_code`)))
WHERE ((`tbl_appointment`.`visit_type` = 'Scheduled')
       AND (`tbl_appointment`.`app_status` = 'Missed')
       AND (`tbl_appointment`.`appntmnt_date` <= CURDATE()))
GROUP BY `tbl_partner_facility`.`mfl_code`,DATE_FORMAT(`tbl_appointment`.`appntmnt_date`,'%M %Y') ;


CREATE OR REPLACE VIEW `vw_missed_clinical_appointments` AS 
SELECT
  `tbl_appointment`.`app_status`         AS `app_status`,
  `tbl_appointment`.`app_type_1`         AS `app_type_1`,
  `tbl_partner_facility`.`partner_id`    AS `partner_id`,
  `tbl_partner_facility`.`mfl_code`      AS `mfl_code`,
  `tbl_partner_facility`.`county_id`     AS `county_id`,
  `tbl_partner_facility`.`sub_county_id` AS `sub_county_id`,
  COUNT(`tbl_appointment`.`id`)          AS `no_of_appointments`,
  DATE_FORMAT(`tbl_appointment`.`appntmnt_date`,'%M %Y') AS `appointment_month`
FROM ((`tbl_appointment`
    JOIN `tbl_client`
      ON ((`tbl_client`.`id` = `tbl_appointment`.`client_id`)))
   JOIN `tbl_partner_facility`
     ON ((`tbl_partner_facility`.`mfl_code` = `tbl_client`.`mfl_code`)))
WHERE ((`tbl_appointment`.`visit_type` = 'Scheduled')
       AND (`tbl_appointment`.`app_status` IN('Missed','Defaulted','LTFU'))
       AND (`tbl_appointment`.`app_type_1` = 'Clinical Review')
       AND (`tbl_appointment`.`appntmnt_date` <= CURDATE()))
GROUP BY `tbl_partner_facility`.`mfl_code`,DATE_FORMAT(`tbl_appointment`.`appntmnt_date`,'%M %Y');

CREATE OR REPLACE VIEW `vw_missed_refill_appointments` AS 
SELECT
  `tbl_appointment`.`app_status`         AS `app_status`,
  `tbl_appointment`.`app_type_1`         AS `app_type_1`,
  `tbl_partner_facility`.`partner_id`    AS `partner_id`,
  `tbl_partner_facility`.`mfl_code`      AS `mfl_code`,
  `tbl_partner_facility`.`county_id`     AS `county_id`,
  `tbl_partner_facility`.`sub_county_id` AS `sub_county_id`,
  COUNT(`tbl_appointment`.`id`)          AS `no_of_appointments`,
  DATE_FORMAT(`tbl_appointment`.`appntmnt_date`,'%M %Y') AS `appointment_month`
FROM ((`tbl_appointment`
    JOIN `tbl_client`
      ON ((`tbl_client`.`id` = `tbl_appointment`.`client_id`)))
   JOIN `tbl_partner_facility`
     ON ((`tbl_partner_facility`.`mfl_code` = `tbl_client`.`mfl_code`)))
WHERE ((`tbl_appointment`.`visit_type` = 'Scheduled')
       AND (`tbl_appointment`.`app_status` IN('Missed','Defaulted','LTFU'))
       AND (`tbl_appointment`.`app_type_1` = 'Re-Fill')
       AND (`tbl_appointment`.`appntmnt_date` <= CURDATE()))
GROUP BY `tbl_partner_facility`.`mfl_code`,DATE_FORMAT(`tbl_appointment`.`appntmnt_date`,'%M %Y') ;


CREATE OR REPLACE VIEW `vw_scheduled_appointments` AS 
SELECT
  `a`.`id`               AS `client_id`,
  CONCAT(`a`.`f_name`,' ',`a`.`m_name`,' ',`a`.`l_name`) AS `client_name`,
  `a`.`clinic_number`    AS `clinic_number`,
  `a`.`status`           AS `status`,
  `d`.`expln_app`        AS `expln_app`,
  `c`.`name`             AS `gender`,
  TIMESTAMPDIFF(YEAR,`a`.`dob`,CURDATE()) AS `age`,
  `a`.`dob`              AS `dob`,
  CURDATE()              AS `today`,
  `b`.`appntmnt_date`    AS `date_attended`,
  `b`.`app_type_1`       AS `appointment_attended`,
  `b`.`app_type_1`       AS `appointment_type`,
  `b`.`active_app`       AS `active_app`,
  `b`.`appointment_kept` AS `appointment_kept`,
  `d`.`appntmnt_date`    AS `next_re_fill_appointment`,
  `d`.`app_type_1`       AS `next_refill_app`,
  `f`.`appntmnt_date`    AS `next_clinical_appointment`,
  `f`.`app_type_1`       AS `next_clinical_review`,
  `a`.`mfl_code`         AS `mfl_code`,
  `e`.`partner_id`       AS `partner_id`,
  `e`.`county_id`        AS `county_id`,
  `e`.`sub_county_id`    AS `sub_county_id`
FROM (((((`tbl_client` `a`
       JOIN `tbl_appointment` `b`
         ON ((`b`.`client_id` = `a`.`id`)))
      JOIN `tbl_gender` `c`
        ON ((`c`.`id` = `a`.`gender`)))
     JOIN `tbl_partner_facility` `e`
       ON ((`e`.`mfl_code` = `a`.`mfl_code`)))
    LEFT JOIN `tbl_appointment` `d`
      ON ((`d`.`client_id` = `a`.`id`)))
   LEFT JOIN `tbl_appointment` `f`
     ON ((`f`.`client_id` = `a`.`id`)))
WHERE ((`b`.`active_app` = '0')
       AND (`d`.`active_app` = '1')
       AND (`d`.`app_type_1` = 'Re-Fill')
       AND (`d`.`appntmnt_date` > CURDATE())
       AND (`f`.`active_app` = '1')
       AND (`f`.`app_type_1` = 'Clinical Review')
       AND (`f`.`appntmnt_date` > CURDATE())
       AND (`d`.`client_id` = `b`.`client_id`)) ;
       
       
       
CREATE OR REPLACE VIEW `vw_scheduled_refill_appointments` AS 
SELECT
  `tbl_appointment`.`app_status`         AS `app_status`,
  `tbl_appointment`.`app_type_1`         AS `app_type_1`,
  `tbl_partner_facility`.`partner_id`    AS `partner_id`,
  `tbl_partner_facility`.`mfl_code`      AS `mfl_code`,
  `tbl_partner_facility`.`county_id`     AS `county_id`,
  `tbl_partner_facility`.`sub_county_id` AS `sub_county_id`,
  COUNT(`tbl_appointment`.`id`)          AS `no_of_appointments`,
  DATE_FORMAT(`tbl_appointment`.`appntmnt_date`,'%M %Y') AS `appointment_month`
FROM ((`tbl_appointment`
    JOIN `tbl_client`
      ON ((`tbl_client`.`id` = `tbl_appointment`.`client_id`)))
   JOIN `tbl_partner_facility`
     ON ((`tbl_partner_facility`.`mfl_code` = `tbl_client`.`mfl_code`)))
WHERE ((`tbl_appointment`.`visit_type` = 'Scheduled')
       AND (`tbl_appointment`.`app_type_1` = 'Re-Fill')
       AND (`tbl_appointment`.`active_app` = 1))
GROUP BY `tbl_partner_facility`.`mfl_code`,DATE_FORMAT(`tbl_appointment`.`appntmnt_date`,'%M %Y') ;


CREATE OR REPLACE VIEW `vw_scheduled_visits_attended` AS 
SELECT
  `tbl_partner_facility`.`partner_id`    AS `partner_id`,
  `tbl_partner_facility`.`mfl_code`      AS `mfl_code`,
  `tbl_partner_facility`.`county_id`     AS `county_id`,
  `tbl_partner_facility`.`sub_county_id` AS `sub_county_id`,
  COUNT(`tbl_appointment`.`id`)          AS `no_of_appointments`,
  DATE_FORMAT(`tbl_appointment`.`appntmnt_date`,'%M %Y') AS `appointment_month`
FROM ((`tbl_appointment`
    JOIN `tbl_client`
      ON ((`tbl_client`.`id` = `tbl_appointment`.`client_id`)))
   JOIN `tbl_partner_facility`
     ON ((`tbl_partner_facility`.`mfl_code` = `tbl_client`.`mfl_code`)))
WHERE ((`tbl_appointment`.`visit_type` = 'Scheduled')
       AND (`tbl_appointment`.`appointment_kept` = 'Yes')
       AND (`tbl_appointment`.`active_app` = '0')
       AND (`tbl_appointment`.`appntmnt_date` <= CURDATE()))
GROUP BY `tbl_partner_facility`.`mfl_code`,DATE_FORMAT(`tbl_appointment`.`appntmnt_date`,'%M %Y') ;


CREATE OR REPLACE VIEW `vw_unscheduled_appointments` AS 
SELECT
  `a`.`id`               AS `client_id`,
  CONCAT(`a`.`f_name`,' ',`a`.`m_name`,' ',`a`.`l_name`) AS `client_name`,
  `a`.`clinic_number`    AS `clinic_number`,
  `a`.`status`           AS `status`,
  `d`.`expln_app`        AS `expln_app`,
  `c`.`name`             AS `gender`,
  TIMESTAMPDIFF(YEAR,`a`.`dob`,CURDATE()) AS `age`,
  `a`.`dob`              AS `dob`,
  CURDATE()              AS `today`,
  `b`.`appntmnt_date`    AS `date_booked`,
  `b`.`app_type_1`       AS `appointment_booked`,
  `b`.`active_app`       AS `booked_app_active_app`,
  `b`.`appointment_kept` AS `appointment_kept`,
  `d`.`appntmnt_date`    AS `next_re_fill_appointment`,
  `d`.`app_type_1`       AS `next_refill_app`,
  `f`.`appntmnt_date`    AS `next_clinical_appointment`,
  `f`.`app_type_1`       AS `next_clinical_review`,
  `a`.`mfl_code`         AS `mfl_code`,
  `e`.`partner_id`       AS `partner_id`,
  `e`.`county_id`        AS `county_id`,
  `e`.`sub_county_id`    AS `sub_county_id`
FROM (((((`tbl_client` `a`
       JOIN `tbl_appointment` `b`
         ON ((`b`.`client_id` = `a`.`id`)))
      JOIN `tbl_gender` `c`
        ON ((`c`.`id` = `a`.`gender`)))
     JOIN `tbl_partner_facility` `e`
       ON ((`e`.`mfl_code` = `a`.`mfl_code`)))
    LEFT JOIN `tbl_appointment` `d`
      ON ((`d`.`client_id` = `a`.`id`)))
   LEFT JOIN `tbl_appointment` `f`
     ON ((`f`.`client_id` = `a`.`id`)))
WHERE ((`b`.`active_app` = '0')
       AND (`d`.`active_app` = '1')
       AND (`d`.`app_type_1` = 'Re-Fill')
       AND (`d`.`appntmnt_date` > CURDATE())
       AND (`f`.`active_app` = '1')
       AND (`f`.`app_type_1` = 'Clinical Review')
       AND (`f`.`appntmnt_date` > CURDATE())
       AND (`d`.`client_id` = `b`.`client_id`)
       AND (`b`.`visit_type` = 'Un-Scheduled')) ;
       
       
       
CREATE OR REPLACE VIEW `vw_unscheduled_refills` AS 
SELECT
  `tbl_partner_facility`.`partner_id`    AS `partner_id`,
  `tbl_partner_facility`.`mfl_code`      AS `mfl_code`,
  `tbl_partner_facility`.`county_id`     AS `county_id`,
  `tbl_partner_facility`.`sub_county_id` AS `sub_county_id`,
  COUNT(`tbl_appointment`.`id`)          AS `no_of_appointments`,
  DATE_FORMAT(`tbl_appointment`.`appntmnt_date`,'%M %Y') AS `appointment_month`
FROM ((`tbl_appointment`
    JOIN `tbl_client`
      ON ((`tbl_client`.`id` = `tbl_appointment`.`client_id`)))
   JOIN `tbl_partner_facility`
     ON ((`tbl_partner_facility`.`mfl_code` = `tbl_client`.`mfl_code`)))
WHERE ((`tbl_appointment`.`visit_type` = 'Un-Scheduled')
       AND (`tbl_appointment`.`app_type_1` = 'Re-Fill'))
GROUP BY `tbl_partner_facility`.`mfl_code`,DATE_FORMAT(`tbl_appointment`.`appntmnt_date`,'%M %Y') ;


CREATE OR REPLACE VIEW `vw_unscheduled_vists` AS 
SELECT
  `tbl_partner_facility`.`partner_id`    AS `partner_id`,
  `tbl_partner_facility`.`mfl_code`      AS `mfl_code`,
  `tbl_partner_facility`.`county_id`     AS `county_id`,
  `tbl_partner_facility`.`sub_county_id` AS `sub_county_id`,
  COUNT(`tbl_appointment`.`id`)          AS `no_of_appointments`,
  DATE_FORMAT(`tbl_appointment`.`appntmnt_date`,'%M %Y') AS `appointment_month`
FROM ((`tbl_appointment`
    JOIN `tbl_client`
      ON ((`tbl_client`.`id` = `tbl_appointment`.`client_id`)))
   JOIN `tbl_partner_facility`
     ON ((`tbl_partner_facility`.`mfl_code` = `tbl_client`.`mfl_code`)))
WHERE ((`tbl_appointment`.`visit_type` = 'Un-Scheduled')
       AND (`tbl_appointment`.`appntmnt_date` <= CURDATE()))
GROUP BY `tbl_partner_facility`.`mfl_code`,DATE_FORMAT(`tbl_appointment`.`appntmnt_date`,'%M %Y');


CREATE OR REPLACE VIEW `vw_user_access_report` AS 
  SELECT 
    CONCAT(
      `tbl_users`.`f_name`,
      ' ',
      `tbl_users`.`m_name`,
      ' ',
      `tbl_users`.`l_name`
    ) AS `user_name`,
    `tbl_users`.`dob` AS `dob`,
    `tbl_users`.`phone_no` AS `phone_no`,
    `tbl_users`.`e_mail` AS `e_mail`,
    `tbl_users`.`access_level` AS `access_level`,
    'Administrator' AS `Administrator` 
  FROM
    (
      `tbl_users` 
      JOIN `tbl_role` 
        ON (
          (
            `tbl_role`.`access_level` = `tbl_users`.`access_level`
          )
        )
    ) 
  WHERE (
      `tbl_users`.`access_level` = 'Admin'
    ) 
  GROUP BY `tbl_users`.`id` 
  UNION
  SELECT 
    CONCAT(
      `tbl_users`.`f_name`,
      ' ',
      `tbl_users`.`m_name`,
      ' ',
      `tbl_users`.`l_name`
    ) AS `user_name`,
    `tbl_users`.`dob` AS `dob`,
    `tbl_users`.`phone_no` AS `phone_no`,
    `tbl_users`.`e_mail` AS `e_mail`,
    `tbl_users`.`access_level` AS `access_level`,
    `tbl_partner`.`name` AS `name` 
  FROM
    (
      (
        `tbl_users` 
        JOIN `tbl_role` 
          ON (
            (
              `tbl_role`.`access_level` = `tbl_users`.`access_level`
            )
          )
      ) 
      JOIN `tbl_partner` 
        ON (
          (
            `tbl_partner`.`id` = `tbl_users`.`partner_id`
          )
        )
    ) 
  WHERE (
      `tbl_users`.`access_level` = 'Partner'
    ) 
  GROUP BY `tbl_users`.`id` 
  UNION
  SELECT 
    CONCAT(
      `tbl_users`.`f_name`,
      ' ',
      `tbl_users`.`m_name`,
      ' ',
      `tbl_users`.`l_name`
    ) AS `user_name`,
    `tbl_users`.`dob` AS `dob`,
    `tbl_users`.`phone_no` AS `phone_no`,
    `tbl_users`.`e_mail` AS `e_mail`,
    `tbl_users`.`access_level` AS `access_level`,
    `tbl_county`.`name` AS `name` 
  FROM
    (
      (
        `tbl_users` 
        JOIN `tbl_role` 
          ON (
            (
              `tbl_role`.`access_level` = `tbl_users`.`access_level`
            )
          )
      ) 
      JOIN `tbl_county` 
        ON (
          (
            `tbl_county`.`id` = `tbl_users`.`county_id`
          )
        )
    ) 
  WHERE (
      `tbl_users`.`access_level` = 'County'
    ) 
  GROUP BY `tbl_users`.`id` 
  UNION
  SELECT 
    CONCAT(
      `tbl_users`.`f_name`,
      ' ',
      `tbl_users`.`m_name`,
      ' ',
      `tbl_users`.`l_name`
    ) AS `user_name`,
    `tbl_users`.`dob` AS `dob`,
    `tbl_users`.`phone_no` AS `phone_no`,
    `tbl_users`.`e_mail` AS `e_mail`,
    `tbl_users`.`access_level` AS `access_level`,
    `tbl_sub_county`.`name` AS `name` 
  FROM
    (
      (
        `tbl_users` 
        JOIN `tbl_role` 
          ON (
            (
              `tbl_role`.`access_level` = `tbl_users`.`access_level`
            )
          )
      ) 
      JOIN `tbl_sub_county` 
        ON (
          (
            `tbl_sub_county`.`id` = `tbl_users`.`subcounty_id`
          )
        )
    ) 
  WHERE (
      `tbl_users`.`access_level` = 'Sub County'
    ) 
  GROUP BY `tbl_users`.`id` 
  UNION
  SELECT 
    CONCAT(
      `tbl_users`.`f_name`,
      ' ',
      `tbl_users`.`m_name`,
      ' ',
      `tbl_users`.`l_name`
    ) AS `user_name`,
    `tbl_users`.`dob` AS `dob`,
    `tbl_users`.`phone_no` AS `phone_no`,
    `tbl_users`.`e_mail` AS `e_mail`,
    `tbl_users`.`access_level` AS `access_level`,
    `tbl_master_facility`.`name` AS `name` 
  FROM
    (
      (
        `tbl_users` 
        JOIN `tbl_master_facility` 
          ON (
            (
              `tbl_master_facility`.`code` = `tbl_users`.`facility_id`
            )
          )
      ) 
      JOIN `tbl_role` 
        ON (
          (
            `tbl_role`.`access_level` = `tbl_users`.`access_level`
          )
        )
    ) 
  WHERE (
      `tbl_users`.`access_level` = 'Facility'
    ) 
  GROUP BY `tbl_users`.`id`  ;
  
CREATE OR REPLACE VIEW `vw_user_msgs` AS 
SELECT 
  `tbl_outgoing`.`id` AS `id`,
  `tbl_outgoing`.`date_added` AS `date_added`,
  `tbl_message_types`.`name` AS `name`,
  `tbl_outgoing`.`status` AS `status`,
  `tbl_outgoing`.`message_type_id` AS `message_type_id`,
  `tbl_outgoing`.`recepient_type` AS `recepient_type`,
  DATE_FORMAT(
    `tbl_outgoing`.`date_added`,
    '%M %Y'
  ) AS `month_year` 
FROM
  (
    `tbl_outgoing` 
    JOIN `tbl_message_types` 
      ON (
        (
          `tbl_message_types`.`id` = `tbl_outgoing`.`message_type_id`
        )
      )
  ) 
WHERE (
    `tbl_outgoing`.`recepient_type` = 'User'
  ) ;



CREATE OR REPLACE VIEW vw_audit_trail AS  SELECT 
  tbl_sys_logs.`user_id`,
  tbl_sys_logs.`description`,
  tbl_sys_logs.`date_added`,tbl_users.`phone_no`,
  CONCAT(
    tbl_users.`f_name`,
    ' ',
    `tbl_users`.`m_name`,
    ' ',
    `tbl_users`.`l_name`
  ) AS user_name 
FROM
  tbl_sys_logs 
  INNER JOIN tbl_users 
    ON tbl_users.`id` = tbl_sys_logs.`user_id` ;
    
    
    
    
    
    CREATE OR REPLACE VIEW vw_client_summary_report AS 
SELECT 
  `tbl_county`.`code` AS `county_code`,
  `tbl_county`.`name` AS `county`,
  `tbl_sub_county`.`name` AS `sub_county`,
  `tbl_master_facility`.`code` AS `mfl_code`,
  `tbl_master_facility`.`name` AS `facility`,
  `tbl_partner`.`name` AS `partner`,
  COUNT(DISTINCT (tbl_client.`id`)) AS no_client,
  COUNT(
    CASE
      WHEN tbl_appointment.`active_app` = "0" 
      AND tbl_appointment.`active_app` != "NULL" 
      THEN 9 
      ELSE NULL 
    END
  ) AS old_appointments,
  COUNT(
    CASE
      WHEN tbl_appointment.`active_app` = "1" 
      AND tbl_appointment.`active_app` != "NULL" 
      THEN 0 
      ELSE NULL 
    END
  ) AS new_appointments,
  COUNT(
    CASE
      WHEN tbl_appointment.`active_app` IS NULL 
      THEN 0 
      ELSE NULL 
    END
  ) AS null_appointments,
  `tbl_sub_county`.`id` AS sub_county_id,
  tbl_sub_county.`name` AS sub_county_name 
FROM
  `tbl_client` 
  JOIN `tbl_partner_facility` 
    ON `tbl_partner_facility`.`mfl_code` = `tbl_client`.`mfl_code` 
  JOIN `tbl_sub_county` 
    ON `tbl_sub_county`.`id` = `tbl_partner_facility`.`sub_county_id` 
  JOIN `tbl_county` 
    ON `tbl_county`.`id` = `tbl_partner_facility`.`county_id` 
  JOIN `tbl_master_facility` 
    ON `tbl_master_facility`.`code` = `tbl_partner_facility`.`mfl_code` 
  JOIN `tbl_partner` 
    ON `tbl_partner`.`id` = `tbl_partner_facility`.`partner_id` 
  LEFT JOIN `tbl_appointment` 
    ON `tbl_appointment`.`client_id` = `tbl_client`.`id` 
GROUP BY tbl_client.`mfl_code` ;



CREATE OR REPLACE VIEW vw_county_performance AS SELECT 
  COUNT(DISTINCT b.county_id) AS actual_counties,
  COUNT(DISTINCT a.`county_id`) AS target_counties 
FROM
  tbl_target_county a,
  tbl_partner_facility b ;
  
  
  
  CREATE OR REPLACE VIEW vw_facility_performance AS SELECT 
  COUNT(DISTINCT b.mfl_code) AS actual_facilities,
  COUNT(DISTINCT a.`mfl_code`) AS target_facilities 
FROM
  tbl_target_facility a,
  tbl_partner_facility b ;
  
  
  
  
CREATE OR REPLACE VIEW vw_client_county_maps AS 
SELECT 
  `tbl_county`.`id` AS `id`,
  `tbl_county`.`name` AS `name`,
  `tbl_county`.`code` AS `code`,
  `tbl_county`.`lat` AS `lat`,
  `tbl_county`.`lng` AS `lng`,
  CONCAT(
    "No of Clients ",
    COUNT(`tbl_client`.`id`)
  ) AS `no_clients`,CONCAT(
    "No of Appointments ",
    COUNT(`tbl_appointment`.`id`)
  ) AS `no_appointments`,
  'Clients' AS `Clients` 
FROM
  tbl_client 
  INNER JOIN tbl_partner_facility 
    ON tbl_partner_facility.`mfl_code` 
  INNER JOIN tbl_county 
    ON tbl_county.`id` = tbl_partner_facility.`county_id` 
  LEFT JOIN tbl_appointment 
    ON tbl_appointment.`client_id` = tbl_client.`id` 
GROUP BY `tbl_county`.`code` ;



CREATE OR REPLACE VIEW vw_client_performance_monitor AS 
SELECT 
  c.`name` AS county,
  d.`name` AS sub_county,
  e.`name` AS facility,
  a.`mfl_code`,
  b.`sub_county_id`,
  b.`county_id`,
  COUNT(DISTINCT a.`id`) AS actual_clients,
  SUM(
    DISTINCT 
    CASE
      WHEN b.`avg_clients` != "NULL" 
      THEN b.`avg_clients` 
      ELSE 0 
    END
  ) AS target_clients 
FROM
  tbl_client a,
  tbl_partner_facility b,
  tbl_county c,
  tbl_sub_county d,
  tbl_master_facility e 
WHERE a.`mfl_code` = b.`mfl_code` 
  AND c.`id` = b.`county_id` 
  AND d.`id` = b.`sub_county_id` 
  AND e.`code` = b.`mfl_code` 
GROUP BY a.`mfl_code`,
  b.`mfl_code` ;

CREATE OR REPLACE VIEW vw_defaulter_bookings_visits AS 
SELECT 
  `a`.`id` AS `client_id`,
  CONCAT(
    `a`.`f_name`,
    ' ',
    `a`.`m_name`,
    ' ',
    `a`.`l_name`
  ) AS `client_name`,
  `a`.`clinic_number` AS `clinic_number`,
  `a`.`status` AS `status`,
  `d`.`expln_app` AS `expln_app`,
  `d`.`app_type_1` AS `purpose_of_visit`,
  `c`.`name` AS `gender`,
  TIMESTAMPDIFF(YEAR, `a`.`dob`, CURDATE()) AS `age`,
  `a`.`dob` AS `dob`,
  CURDATE() AS `today`,
  b.`app_status`,
  `b`.`appntmnt_date` AS `missed_appointment_date`,
  `b`.`app_type_1` AS `missed_appointment_type`,
  `b`.`fnl_outcome_dte` AS `fnl_outcome_dte`,
  `b`.`active_app` AS `active_app`,
  `d`.`appntmnt_date` AS `next_clinical_appointment_date`,
  `d`.`app_type_1` AS `next_clinical_appointment`,
  `a`.`mfl_code` AS `mfl_code`,
  `e`.`partner_id` AS `partner_id`,
  `e`.`county_id` AS `county_id`,
  `e`.`sub_county_id` AS `sub_county_id`,
  `a`.`stable` AS `stable` 
FROM
  `tbl_client` `a` 
  JOIN `tbl_appointment` `b` 
    ON `b`.`client_id` = `a`.`id` 
  JOIN `tbl_gender` `c` 
    ON `c`.`id` = `a`.`gender` 
  JOIN `tbl_partner_facility` `e` 
    ON `e`.`mfl_code` = `a`.`mfl_code` 
  LEFT JOIN `tbl_appointment` `d` 
    ON `d`.`client_id` = `a`.`id` 
WHERE `b`.`active_app` = '0' 
  AND `d`.`active_app` = '1' 
  AND `d`.`app_type_1` = 'Clinical Review' 
  AND `d`.`appntmnt_date` > CURDATE() 
  AND `d`.`client_id` = `b`.`client_id` 
  AND b.`app_status` IN ('Missed', 'Defaulted', 'LTFU') ;
  
  
  
  
  CREATE OR REPLACE VIEW vw_missed_appointments AS SELECT 
  `tbl_partner_facility`.`partner_id` AS `partner_id`,
  `tbl_partner_facility`.`mfl_code` AS `mfl_code`,
  `tbl_partner_facility`.`county_id` AS `county_id`,
  `tbl_partner_facility`.`sub_county_id` AS `sub_county_id`,
  COUNT(`tbl_appointment`.`id`) AS `no_of_appointments`,
  DATE_FORMAT(
    `tbl_appointment`.`appntmnt_date`,
    '%M %Y'
  ) AS `appointment_month` 
FROM
  `tbl_appointment` 
  JOIN `tbl_client` 
    ON `tbl_client`.`id` = `tbl_appointment`.`client_id` 
  JOIN `tbl_partner_facility` 
    ON `tbl_partner_facility`.`mfl_code` = `tbl_client`.`mfl_code` 
WHERE `tbl_appointment`.`visit_type` = 'Scheduled' 
  AND `tbl_appointment`.`app_status` IN ('Missed', 'Defaulted', 'LTFU') 
  AND `tbl_appointment`.`appntmnt_date` <= CURDATE()  
GROUP BY `tbl_partner_facility`.`mfl_code`,
  DATE_FORMAT(
    `tbl_appointment`.`appntmnt_date`,
    '%M %Y'
  ) ;


  
  
CREATE OR REPLACE VIEW vw_defaulter_tracing_register_clients AS 
SELECT 
  tbl_client.`id`,
  tbl_client.`clinic_number`,
  CONCAT(`f_name`, ' ', m_name, ' ', l_name) AS client_name,
  CASE
    WHEN `art_date` = '0000-00-00 00:00:00' 
    THEN 'No' 
    ELSE 'Yes' 
  END AS art,
  `art_date`,
  CASE
    WHEN tbl_gender.`name` = 'Male' 
    THEN 'M' 
    WHEN tbl_gender.`name` = 'Female' 
    THEN 'F' 
  END AS sex,
  tbl_client.`stable`,
  TIMESTAMPDIFF(YEAR, `tbl_client`.`dob`, CURDATE()) AS `age`,
  tbl_client.`enrollment_date`,
  DATE_FORMAT(`art_date`, '%M') AS art_cohort_month,
  tbl_appointment.`appntmnt_date` AS date_of_missed_appointment,
  tbl_client.`phone_no` AS client_phone_no,
  tbl_client.`shared_no_name` AS treatment_supporter_name,
  tbl_client.`alt_phone_no` AS treatment_supporter_no,
  tbl_client.`physical_address` ,tbl_client.`mfl_code`
FROM
  tbl_client 
  INNER JOIN tbl_appointment 
    ON tbl_appointment.`client_id` = tbl_client.`id` 
  INNER JOIN tbl_gender 
    ON tbl_gender.`id` = tbl_client.`gender` 
WHERE tbl_appointment.`appntmnt_date` < CURDATE() 
  AND tbl_appointment.`active_app` = '1' ;

