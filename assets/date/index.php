<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="./cupertino/jquery.ui.all.css" type="text/css">
<script type="text/javascript" src="./jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="./jquery.ui.core.min.js"></script>
<script type="text/javascript" src="./jquery.ui.widget.min.js"></script>
<script type="text/javascript" src="./jquery.ui.datepicker.min.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
   var checkinndateOpts =
   {
      dateFormat: 'mm/dd/yy',
      changeMonth: false,
      changeYear: false,
      showButtonPanel: false,
      showAnim: 'show'
   };
   $("#checkinndate").datepicker(checkinndateOpts);
});
</script>
<style type="text/css">
.ui-datepicker
{
   font-family: Arial;
   font-size: 13px;
   z-index: 1003 !important;
}
</style>
</head>

<body>
<font style="font-size:11px" color="#C0997E" face="Arial">Check-In-Date</font><font style="font-size:11px" color="#FF0000" face="Arial">*</font></div>
<input type="text" id="checkinndate"  name="Check Inn Date" value="" >
</body>
</html>
