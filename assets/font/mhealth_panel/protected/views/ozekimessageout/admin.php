<?php

$this->menu=array(
	array('label'=>'List Ozekimessageout', 'url'=>array('index')),
	array('label'=>'Create Ozekimessageout', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('ozekimessageout-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Message Delivery Status </h1>


<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'ozekimessageout-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'receiver',
		'msg',
		'senttime',
		'receivedtime',
		'status'
	),
)); ?>
