<?php
$this->breadcrumbs=array(
	'Ozekimessageouts',
);

$this->menu=array(
	array('label'=>'Create Ozekimessageout', 'url'=>array('create')),
	array('label'=>'Manage Ozekimessageout', 'url'=>array('admin')),
);
?>

<h1>Ozekimessageouts</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
