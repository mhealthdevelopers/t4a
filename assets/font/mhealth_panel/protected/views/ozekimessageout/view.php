<?php
$this->breadcrumbs=array(
	'Ozekimessageouts'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Ozekimessageout', 'url'=>array('index')),
	array('label'=>'Create Ozekimessageout', 'url'=>array('create')),
	array('label'=>'Update Ozekimessageout', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Ozekimessageout', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Ozekimessageout', 'url'=>array('admin')),
);
?>

<h1>View Ozekimessageout #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'sender',
		'receiver',
		'msg',
		'senttime',
		'receivedtime',
		'reference',
		'status',
		'msgtype',
		'operator',
	),
)); ?>
