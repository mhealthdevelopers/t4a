<?php
$this->breadcrumbs=array(
	'Ozekimessageouts'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Ozekimessageout', 'url'=>array('index')),
	array('label'=>'Create Ozekimessageout', 'url'=>array('create')),
	array('label'=>'View Ozekimessageout', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Ozekimessageout', 'url'=>array('admin')),
);
?>

<h1>Update Ozekimessageout <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>