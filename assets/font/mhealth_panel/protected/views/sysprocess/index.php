<?php
$this->breadcrumbs=array(
	'Sysprocesses',
);

$this->menu=array(
	array('label'=>'Create Sysprocess', 'url'=>array('create')),
	array('label'=>'Manage Sysprocess', 'url'=>array('admin')),
);
?>

<h1>Sysprocesses</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
