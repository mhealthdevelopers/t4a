<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'sysprocess-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'categ'); ?>
		<?php echo $form->textField($model,'categ',array('size'=>60,'maxlength'=>100,'readonly'=>'readonly' )); ?>
		<?php echo $form->error($model,'categ'); ?>
	</div>

	
	<div class="row">
		<?php echo $form->labelEx($model,'in_text'); ?>
		<?php echo $form->textField($model,'in_text',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'in_text'); ?>
	</div>

	
	<div class="row">
		<?php echo $form->labelEx($model,'out_msg'); ?>
		<?php echo $form->textArea($model,'out_msg',array('size'=>60,'maxlength'=>160,'cols'=>38,'rows'=>5)); ?>
		<?php echo $form->error($model,'out_msg'); ?>
	</div>

	

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->