<?php
$this->breadcrumbs=array(
	'Sysprocesses'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Sysprocess', 'url'=>array('index')),
	array('label'=>'Manage Sysprocess', 'url'=>array('admin')),
);
?>

<h1>Create Sysprocess</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>