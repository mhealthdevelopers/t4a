<?php

$this->menu=array(
	array('label'=>'List Sysprocess', 'url'=>array('index')),
	array('label'=>'Create Sysprocess', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('sysprocess-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<p> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </p>

<h1>Manage Autoresponse Messages </h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'sysprocess-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'categ',
		'in_text',
		'out_msg',
		array(
			'class'=>'CButtonColumn',
			'template'=>'{view}{update}',
		),
	),
)); ?>
