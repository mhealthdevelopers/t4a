<?php
$this->breadcrumbs=array(
	'Tbl Patientdetails'=>array('index'),
	$model->p_id,
);

$this->menu=array(
	array('label'=>'List TblPatientdetails', 'url'=>array('index')),
	array('label'=>'Create TblPatientdetails', 'url'=>array('create')),
	array('label'=>'Update TblPatientdetails', 'url'=>array('update', 'id'=>$model->p_id)),
	array('label'=>'Delete TblPatientdetails', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->p_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TblPatientdetails', 'url'=>array('admin')),
);
?>

<h1>View TblPatientdetails #<?php echo $model->p_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'p_id',
		'p_fname',
		'p_lname',
		'p_location',
		'p_gender',
		'p_occupation',
		'p_address',
		'p_mobile',
		'sms_enabled',
		'smsthankyou',
		'smstime',
		'phn',
		'date_admitted',
		'smsday',
		'patient_flag',
	),
)); ?>
