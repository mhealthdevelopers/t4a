<?php
$this->breadcrumbs=array(
	'Tbl Patientdetails'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List TblPatientdetails', 'url'=>array('index')),
	array('label'=>'Manage TblPatientdetails', 'url'=>array('admin')),
);
?>

<h1>Create TblPatientdetails</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>