<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>



	<div class="row">
		<?php echo $form->label($model,'First Name'); ?>
		<?php echo $form->textField($model,'p_fname',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Last Name'); ?>
		<?php echo $form->textField($model,'p_lname',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	

	<div class="row">
		<?php echo $form->label($model,'Cell Phone Number'); ?>
		<?php echo $form->textField($model,'p_mobile',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Provisional Health Number'); ?>
		<?php echo $form->textField($model,'phn',array('size'=>60,'maxlength'=>100)); ?>
	</div>


	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->