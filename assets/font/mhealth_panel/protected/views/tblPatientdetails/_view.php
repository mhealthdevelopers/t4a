<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('p_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->p_id), array('view', 'id'=>$data->p_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('p_fname')); ?>:</b>
	<?php echo CHtml::encode($data->p_fname); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('p_lname')); ?>:</b>
	<?php echo CHtml::encode($data->p_lname); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('p_location')); ?>:</b>
	<?php echo CHtml::encode($data->p_location); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('p_gender')); ?>:</b>
	<?php echo CHtml::encode($data->p_gender); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('p_occupation')); ?>:</b>
	<?php echo CHtml::encode($data->p_occupation); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('p_address')); ?>:</b>
	<?php echo CHtml::encode($data->p_address); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('p_mobile')); ?>:</b>
	<?php echo CHtml::encode($data->p_mobile); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sms_enabled')); ?>:</b>
	<?php echo CHtml::encode($data->sms_enabled); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('smsthankyou')); ?>:</b>
	<?php echo CHtml::encode($data->smsthankyou); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('smstime')); ?>:</b>
	<?php echo CHtml::encode($data->smstime); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('phn')); ?>:</b>
	<?php echo CHtml::encode($data->phn); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date_admitted')); ?>:</b>
	<?php echo CHtml::encode($data->date_admitted); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('smsday')); ?>:</b>
	<?php echo CHtml::encode($data->smsday); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('patient_flag')); ?>:</b>
	<?php echo CHtml::encode($data->patient_flag); ?>
	<br />

	*/ ?>

</div>