<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'tbl-patientdetails-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'p_fname'); ?>
		<?php echo $form->textField($model,'p_fname',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'p_fname'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'p_lname'); ?>
		<?php echo $form->textField($model,'p_lname',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'p_lname'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'p_location'); ?>
		<?php echo $form->textField($model,'p_location',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'p_location'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'p_gender'); ?>
		<?php echo $form->textField($model,'p_gender',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'p_gender'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'p_occupation'); ?>
		<?php echo $form->textField($model,'p_occupation',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'p_occupation'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'p_address'); ?>
		<?php echo $form->textField($model,'p_address',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'p_address'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'p_mobile'); ?>
		<?php echo $form->textField($model,'p_mobile',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'p_mobile'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sms_enabled'); ?>
		<?php echo $form->textField($model,'sms_enabled'); ?>
		<?php echo $form->error($model,'sms_enabled'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'smsthankyou'); ?>
		<?php echo $form->textField($model,'smsthankyou'); ?>
		<?php echo $form->error($model,'smsthankyou'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'smstime'); ?>
		<?php echo $form->textField($model,'smstime',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'smstime'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'phn'); ?>
		<?php echo $form->textField($model,'phn',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'phn'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'date_admitted'); ?>
		<?php echo $form->textField($model,'date_admitted'); ?>
		<?php echo $form->error($model,'date_admitted'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'smsday'); ?>
		<?php echo $form->textField($model,'smsday',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'smsday'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'patient_flag'); ?>
		<?php echo $form->textField($model,'patient_flag',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'patient_flag'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->