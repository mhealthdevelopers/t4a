<?php



$this->menu=array(
	array('label'=>'List TblPatientdetails', 'url'=>array('index')),
	array('label'=>'Create TblPatientdetails', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('tbl-patientdetails-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<p> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </p>

<h1>Active Patients View</h1>


<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->
<br/>
<br/>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'tbl-patientdetails-grid',
	'dataProvider'=>$model->search("active"),
	'filter'=>$model,
   'pager'=>array(
		//'class'=>'CLinkPager',
	),
   
	'columns'=>array(
		array(
		'name'=>'p_fname'
		),
		'p_lname',
		'p_mobile',
		'phn',
		'date_admitted',
		'patient_flag',
	   /**
		array(
			'class'=>'CButtonColumn',
		),
		* */
	),
)); ?>
