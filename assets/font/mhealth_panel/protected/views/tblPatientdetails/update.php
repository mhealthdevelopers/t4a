<?php
$this->breadcrumbs=array(
	'Tbl Patientdetails'=>array('index'),
	$model->p_id=>array('view','id'=>$model->p_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List TblPatientdetails', 'url'=>array('index')),
	array('label'=>'Create TblPatientdetails', 'url'=>array('create')),
	array('label'=>'View TblPatientdetails', 'url'=>array('view', 'id'=>$model->p_id)),
	array('label'=>'Manage TblPatientdetails', 'url'=>array('admin')),
);
?>

<h1>Update TblPatientdetails <?php echo $model->p_id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>