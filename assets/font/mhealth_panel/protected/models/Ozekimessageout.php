<?php

/**
 * This is the model class for table "ozekimessageout".
 *
 * The followings are the available columns in table 'ozekimessageout':
 * @property integer $id
 * @property string $sender
 * @property string $receiver
 * @property string $msg
 * @property string $senttime
 * @property string $receivedtime
 * @property string $reference
 * @property string $status
 * @property string $msgtype
 * @property string $operator
 */
class Ozekimessageout extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Ozekimessageout the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ozekimessageout';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('sender, receiver', 'length', 'max'=>30),
			array('msg, msgtype', 'length', 'max'=>160),
			array('senttime, receivedtime, reference, operator', 'length', 'max'=>100),
			array('status', 'length', 'max'=>20),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, sender, receiver, msg, senttime, receivedtime, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'sender' => 'Sender',
			'receiver' => 'Receiver',
			'msg' => 'Message',
			'senttime' => 'Sent Time',
			'receivedtime' => 'Received Time',
			'reference' => 'Reference',
			'status' => 'Status',
			'msgtype' => 'Msgtype',
			'operator' => 'Operator',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('sender',$this->sender,true);
		$criteria->compare('receiver',$this->receiver,true);
		$criteria->compare('msg',$this->msg,true);
		$criteria->compare('senttime',$this->senttime,true);
		$criteria->compare('receivedtime',$this->receivedtime,true);
		$criteria->compare('reference',$this->reference,false);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('msgtype',$this->msgtype,false);
		$criteria->compare('operator',$this->operator,false);
		$criteria->order = "id DESC";

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}