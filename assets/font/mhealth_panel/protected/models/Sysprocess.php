<?php

/**
 * This is the model class for table "sysprocess".
 *
 * The followings are the available columns in table 'sysprocess':
 * @property integer $id
 * @property string $categ
 * @property string $sub_categ
 * @property string $in_text
 * @property string $out_text
 * @property string $actions
 * @property string $out_msg
 * @property integer $sysdef_id
 * @property string $sms_datetime
 * @property string $action_status
 * @property string $dayofweek
 * @property string $default_time
 */
class Sysprocess extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Sysprocess the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'sysprocess';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('sysdef_id', 'numerical', 'integerOnly'=>true),
			array('categ, sub_categ, action_status, dayofweek, default_time', 'length', 'max'=>100),
			array('in_text, out_text, actions', 'length', 'max'=>50),
			array('out_msg', 'length', 'max'=>160),
			array('sms_datetime', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, categ, sub_categ, in_text, out_text, actions, out_msg, sysdef_id, sms_datetime, action_status, dayofweek, default_time', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'categ' => 'Section',
			'sub_categ' => 'Sub Categ',
			'in_text' => 'InString Text',
			'out_text' => 'Out Text',
			'actions' => 'Actions',
			'out_msg' => 'Outgoing Message',
			'sysdef_id' => 'Sysdef',
			'sms_datetime' => 'Sms Datetime',
			'action_status' => 'Action Status',
			'dayofweek' => 'Dayofweek',
			'default_time' => 'Default Time',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('categ',$this->categ,true);
		$criteria->compare('sub_categ',$this->sub_categ,true);
		$criteria->compare('in_text',$this->in_text,true);
		$criteria->compare('out_text',$this->out_text,true);
		$criteria->compare('actions',$this->actions,true);
		$criteria->compare('out_msg',$this->out_msg,true);
		$criteria->compare('sysdef_id',$this->sysdef_id);
		$criteria->compare('sms_datetime',$this->sms_datetime,true);
		$criteria->compare('action_status',$this->action_status,true);
		$criteria->compare('dayofweek',$this->dayofweek,true);
		$criteria->compare('default_time',$this->default_time,true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}