<?php

/**
 * This is the model class for table "active_patients".
 *
 * The followings are the available columns in table 'active_patients':
 * @property integer $p_id
 * @property string $p_fname
 * @property string $p_lname
 * @property string $p_mobile
 * @property string $date_admitted
 * @property string $patient_flag
 */
class ActivePatients extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return ActivePatients the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	public function primaryKey(){
            return 'p_id';
    }


	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'active_patients';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('p_id', 'numerical', 'integerOnly'=>true),
			array('p_fname, p_lname, patient_flag', 'length', 'max'=>100),
			array('p_mobile', 'length', 'max'=>50),
			array('date_admitted', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('p_id, p_fname, p_lname, p_mobile, date_admitted, patient_flag', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'p_id' => 'P',
			'p_fname' => 'P Fname',
			'p_lname' => 'P Lname',
			'p_mobile' => 'P Mobile',
			'date_admitted' => 'Date Admitted',
			'patient_flag' => 'Patient Flag',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('p_id',$this->p_id);
		$criteria->compare('p_fname',$this->p_fname,true);
		$criteria->compare('p_lname',$this->p_lname,true);
		$criteria->compare('p_mobile',$this->p_mobile,true);
		$criteria->compare('date_admitted',$this->date_admitted,true);
		$criteria->compare('patient_flag',$this->patient_flag,true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}