<?php

/**
 * This is the model class for table "tbl_patientdetails".
 *
 * The followings are the available columns in table 'tbl_patientdetails':
 * @property integer $p_id
 * @property string $p_fname
 * @property string $p_lname
 * @property string $p_location
 * @property string $p_gender
 * @property string $p_occupation
 * @property string $p_address
 * @property string $p_mobile
 * @property integer $sms_enabled
 * @property integer $smsthankyou
 * @property string $smstime
 * @property string $phn
 * @property string $date_admitted
 * @property string $smsday
 * @property string $patient_flag
 */
class TblPatientdetails extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return TblPatientdetails the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_patientdetails';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('sms_enabled, smsthankyou', 'numerical', 'integerOnly'=>true),
			array('p_fname, p_lname, p_location, p_gender, p_occupation, smstime, phn, smsday, patient_flag', 'length', 'max'=>100),
			array('p_address', 'length', 'max'=>200),
			array('p_mobile', 'length', 'max'=>50),
			array('date_admitted', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('p_fname, p_lname, p_mobile, phn,  patient_flag', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'p_id' => 'P',
			'p_fname' => 'First Name',
			'p_lname' => 'Last Name',
			'p_location' => 'P Location',
			'p_gender' => 'P Gender',
			'p_occupation' => 'P Occupation',
			'p_address' => 'P Address',
			'p_mobile' => 'Cell Phone Number',
			'sms_enabled' => 'Sms Enabled',
			'smsthankyou' => 'Smsthankyou',
			'smstime' => 'Smstime',
			'phn' => 'P.H Number',
			'date_admitted' => 'Date Admitted',
			'smsday' => 'Smsday',
			'patient_flag' => 'Patient Flag',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($flag)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		
		$criteria->compare('p_fname',$this->p_fname,true);
		$criteria->compare('p_lname',$this->p_lname,true);
		//$criteria->compare('p_location',$this->p_location,true);
		//$criteria->compare('p_gender',$this->p_gender,true);
		//$criteria->compare('p_occupation',$this->p_occupation,true);
		//$criteria->compare('p_address',$this->p_address,true);
		$criteria->compare('p_mobile',$this->p_mobile,true);
		//$criteria->compare('sms_enabled',$this->sms_enabled);
		//$criteria->compare('smsthankyou',$this->smsthankyou);
		//$criteria->compare('smstime',$this->smstime,true);
		$criteria->compare('phn',$this->phn,true);
		//$criteria->compare('date_admitted',$this->date_admitted,true);
		//$criteria->compare('smsday',$this->smsday,true);
		$criteria->compare('patient_flag',$this->patient_flag,true);
		$criteria->addSearchCondition('patient_flag', $flag);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}